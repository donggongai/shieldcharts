// 扩展处理
ShieldCharts.addExtendFun("bar_line", function(ele, url) {
	var dom = ele[0];
    var myChart = echarts.init(dom, ShieldCharts.theme);
    
    $.getJSON(url, function(result){
    	var datas = result.data.data; //数据
        var titles = result.data.title; //表格标题
        var legends = result.data.axisName||[]; //标注
        
        var data = {
                id: 'multipleBarsLines',
                title: '',
                legendBar: ['正面占比', '中立占比', '负面占比'],
                symbol: '', //数值是否带百分号        --默认为空 ''
                legendLine: ['同期对比'],
                xAxis: legends,
                yAxis: [
                	datas[0]
                ],
                lines: [
                	datas[1]
                ],
                barColor: ['#3FA7DC', '#7091C4', '#5170A2'], //柱子颜色 必填参数
                lineColor: ['#D9523F'], // 折线颜色

            };
            /////////////end/////////

            var myData = (function test() {
                var yAxis = data.yAxis || [];
                var lines = data.lines || [];
                var legendBar = data.legendBar || [];
                var legendLine = data.legendLine || [];
                var symbol = data.symbol || ' ';
                var seriesArr = [];
                var legendArr = [];
                yAxis && yAxis.forEach((item, index) => {
                    legendArr.push({
                        name: legendBar && legendBar.length > 0 && legendBar[index]
                    });
                    seriesArr.push({
                        name: legendBar && legendBar.length > 0 && legendBar[index],
                        type: 'bar',
                        barGap: '0.5px',
                        data: item,
                        barWidth: data.barWidth || 12,
                        label: {
                            normal: {
                                show: true,
                                formatter: '{c}' + symbol,
                                position: 'top',
                                textStyle: {
                                    color: '#fff',
                                    fontStyle: 'normal',
                                    fontFamily: '微软雅黑',
                                    textAlign: 'left',
                                    fontSize: 11,
                                },
                            },
                        },
                        itemStyle: { //图形样式
                            normal: {
                                barBorderRadius: 4,
                                color: data.barColor[index]
                            },
                        }
                    });
                });

                lines && lines.forEach((item, index) => {
                    legendArr.push({
                        name: legendLine && legendLine.length > 0 && legendLine[index]
                    })
                    seriesArr.push({
                        name: legendLine && legendLine.length > 0 && legendLine[index],
                        type: 'line',
                        data: item,
                        itemStyle: {
                            normal: {
                                color: data.lineColor[index],
                                lineStyle: {
                                    width: 3,
                                    type: 'solid',
                                }
                            }
                        },
                        label: {
                            normal: {
                                show: false, //折线上方label控制显示隐藏
                                position: 'top',
                            }
                        },
                        symbol: 'circle',
                        symbolSize: 10
                    });
                });

                return {
                    seriesArr,
                    legendArr
                };
            })();


            var options = {
                title: {
                    show: true,
                    top: '10%',
                    left: '3%',
                    text: data.title,
                    textStyle: {
                        fontSize: 18,
                        color: '#fff'
                    },
                    subtext: data.subTitle,
                    link: ''
                },
                tooltip: {
                    trigger: 'axis',
                    formatter: function (params) {
                        var time = '';
                        var str = '';
                        for (var i of params) {
                            time = i.name.replace(/\n/g, '') + '<br/>';
                            if (i.data == 'null' || i.data == null) {
                                str += i.seriesName + '：无数据' + '<br/>'
                            } else {
                                str += i.seriesName + '：' + i.data + symbol + '%<br/>'
                            }

                        }
                        return time + str;
                    },
                    axisPointer: {
                        type: 'none'
                    },
                },
                legend: {
                    right: data.legendRight || '30%',
                    top: '12%',
                    right: '5%',
                    itemGap: 16,
                    itemWidth: 10,
                    itemHeight: 10,
                    data: myData.legendArr,
                    textStyle: {
                        color: '#fff',
                        fontStyle: 'normal',
                        fontFamily: '微软雅黑',
                        fontSize: 12,
                    }
                },
                grid: {
                    x: 30,
                    y: 80,
                    x2: 30,
                    y2: 60,
                },
                xAxis: {
                    type: 'category',
                    data: data.xAxis,
                    axisTick: {
                        show: false,
                    },

                    axisLine: {
                        show: true,
                        lineStyle: {
                            color: '#1AA1FD',
                        },
                        symbol: ['none', 'arrow']
                    },
                    axisLabel: {
                        show: true,
                        interval: '0',
                        textStyle: {
                            lineHeight: 16,
                            padding: [2, 2, 0, 2],
                            height: 50,
                            fontSize: 12,
                        },
                        rich: {
                            Sunny: {
                                height: 50,
                                // width: 60,
                                padding: [0, 5, 0, 5],
                                align: 'center',
                            },
                        },
                        formatter: function (params, index) {
                            var newParamsName = "";
                            var splitNumber = 5;
                            var paramsNameNumber = params && params.length;
                            if (paramsNameNumber && paramsNameNumber <= 4) {
                                splitNumber = 4;
                            } else if (paramsNameNumber >= 5 && paramsNameNumber <= 7) {
                                splitNumber = 4;
                            } else if (paramsNameNumber >= 8 && paramsNameNumber <= 9) {
                                splitNumber = 5;
                            } else if (paramsNameNumber >= 10 && paramsNameNumber <= 14) {
                                splitNumber = 5;
                            } else {
                                params = params && params.slice(0, 15);
                            }

                            var provideNumber = splitNumber; //一行显示几个字
                            var rowNumber = Math.ceil(paramsNameNumber / provideNumber) || 0;
                            if (paramsNameNumber > provideNumber) {
                                for (var p = 0; p < rowNumber; p++) {
                                    var tempStr = "";
                                    var start = p * provideNumber;
                                    var end = start + provideNumber;
                                    if (p == rowNumber - 1) {
                                        tempStr = params.substring(start, paramsNameNumber);
                                    } else {
                                        tempStr = params.substring(start, end) + "\n";
                                    }
                                    newParamsName += tempStr;
                                }

                            } else {
                                newParamsName = params;
                            }
                            params = newParamsName;
                            return '{Sunny|' + params + '}';
                        },
                        color: '#1AA1FD',
                    },

                },
                yAxis: {
                    axisLine: {
                        show: true,
                        lineStyle: {
                            color: '#1AA1FD',
                        },
                        symbol: ['none', 'arrow']
                    },
                    type: 'value',
                    axisTick: {
                        show: false
                    },
                    axisLabel: {
                        show: false
                    },
                    splitLine: {
                        show: false,
                        lineStyle: {
                            color: '#1AA1FD',
                            type: 'solid'
                        },
                    }
                },
                series: myData.seriesArr
            }
        
            showCharts(ele, myChart, options);
    });

},"柱形和折线图");
ShieldCharts.addExtendFun("map_shandong", function(ele, url) {
	var dom = ele[0];
    var myChart = echarts.init(dom, ShieldCharts.theme);
    
    var geoCoordMap = {};
    
    $.getJSON(url, function(result){
    	var datas = result.data.data; //数据
        var titles = result.data.title; //表格标题
        var legends = result.data.axisName||[]; //标注
        var mapTitle = result.data.maptitle;
        var mapAreaName = "shandong"; //地图区域
    	var showName = result.data.showName||"未定义"; //重点展示区域
    	var adcode = result.data.adcode||"100000"; //区域编码用来获取数据
        
        $.getJSON(chartsjsPath +'/map/geodata/'+adcode+'.geoJson', function(result) {
    		var mapJson = {"type": "FeatureCollection"};
            mapJson.features = result.features;
    		echarts.registerMap(mapAreaName, mapJson ); //注册地区
    		
    		/*var geoCoordMap = {
	            '河池市': [108.085179,24.700488],
	            '柳州市': [109.412578,24.354875],
	            '梧州市': [111.323462,23.478238],
	            '南宁市': [108.359656,22.81328],
	            '北海市': [109.171374,21.477419],
	            '崇左市': [107.347374,22.377503]
	        };*/

	        /*获取地图数据*/
	        myChart.showLoading();
	        var mapFeatures = echarts.getMap(mapAreaName).geoJson.features;
	        myChart.hideLoading();
	        mapFeatures.forEach(function(v) {
	        	// 地区名称
	        	var name = v.properties.name;
	        	// 地区经纬度
	        	geoCoordMap[name] = v.properties.center;
	        	console.log(name+" "+geoCoordMap[name][0]+" "+geoCoordMap[name][1])
	        });
	            
	        var data = datas;
	        var max = 480,
	            min = 9; // todo 
	        var maxSize4Pin = 100,
	            minSize4Pin = 20;
	        var convertData = function (data) {
	            var res = [];
	            for (var i = 0; i < data.length; i++) {
	                var geoCoord = geoCoordMap[data[i].name];
	                if (geoCoord) {
	                    res.push({
	                        name: data[i].name,
	                        value: geoCoord.concat(data[i].value)
	                    });
	                }
	            }
	            return res;
	        };
	
	       var options = {
	            title: {
	                text: mapTitle,
	                subtext: '',
	                x: 'center',
	                textStyle: {
	                    color: '#FFF'
	                },
	                left: '6%',
	                top: '10%'
	            },
	            legend: {
	                orient: 'vertical',
	                y: 'bottom',
	                x: 'right',
	                data: ['pm2.5'],
	                textStyle: {
	                    color: '#fff'
	                }
	            },
	            visualMap: {
	                show: false,
	                min: 0,
	                max: 500,
	                left: 'left',
	                top: 'bottom',
	                text: ['高', '低'], // 文本，默认为数值文本
	                calculable: true,
	                seriesIndex: [1],
	                inRange: {}
	            },
	            geo: {
	                show: true,
	                map: mapAreaName,
	                mapType: mapAreaName,
	                label: {
	                    normal: {},
	                    //鼠标移入后查看效果
	                    emphasis: {
	                        textStyle: {
	                            color: '#fff'
	                        }
	                    }
	                },
	                //鼠标缩放和平移
	                roam: true,
	                itemStyle: {
	                    normal: {
	                        //          	color: '#ddd',
	                        borderColor: 'rgba(147, 235, 248, 1)',
	                        borderWidth: 1,
	                        areaColor: {
	                            type: 'radial',
	                            x: 0.5,
	                            y: 0.5,
	                            r: 0.8,
	                            colorStops: [{
	                                offset: 0,
	                                color: 'rgba(175,238,238, 0)' // 0% 处的颜色
	                            }, {
	                                offset: 1,
	                                color: 'rgba(	47,79,79, .1)' // 100% 处的颜色
	                            }],
	                            globalCoord: false // 缺省为 false
	                        },
	                        shadowColor: 'rgba(128, 217, 248, 1)',
	                        shadowOffsetX: -2,
	                        shadowOffsetY: 2,
	                        shadowBlur: 10
	                    },
	                    emphasis: {
	                        areaColor: '#389BB7',
	                        borderWidth: 0
	                    }
	                }
	            },
	            series: [{
	                    name: 'light',
	                    type: 'map',
	                    coordinateSystem: 'geo',
	                    data: convertData(data),
	                    itemStyle: {
	                        normal: {
	                            color: '#F4E925'
	                        }
	                    }
	                },
	                {
	                    name: '点',
	                    type: 'scatter',
	                    coordinateSystem: 'geo',
	                    symbol: 'pin',
	                    symbolSize: function (val) {
	                        var a = (maxSize4Pin - minSize4Pin) / (max - min);
	                        var b = minSize4Pin - a * min;
	                        b = maxSize4Pin - a * max;
	                        return a * val[2] + b;
	                    },
	                    label: {
	                        normal: {
	                            // show: true,
	                            // textStyle: {
	                            //     color: '#fff',
	                            //     fontSize: 9,
	                            // }
	                        }
	                    },
	                    itemStyle: {
	                        normal: {
	                            color: '#F62157', //标志颜色
	                        }
	                    },
	                    zlevel: 6,
	                    data: convertData(data),
	                },
	                {
	                    name: 'light',
	                    type: 'map',
	                    mapType: 'hunan',
	                    geoIndex: 0,
	                    aspectScale: 0.75, //长宽比
	                    showLegendSymbol: false, // 存在legend时显示
	                    label: {
	                        normal: {
	                            show: false
	                        },
	                        emphasis: {
	                            show: false,
	                            textStyle: {
	                                color: '#fff'
	                            }
	                        }
	                    },
	                    roam: true,
	                    itemStyle: {
	                        normal: {
	                            areaColor: '#031525',
	                            borderColor: '#FFFFFF',
	                        },
	                        emphasis: {
	                            areaColor: '#2B91B7'
	                        }
	                    },
	                    animation: false,
	                    data: data
	                },
	                {
	                    name: ' ',
	                    type: 'effectScatter',
	                    coordinateSystem: 'geo',
	                    data: convertData(data.sort(function (a, b) {
	                        return b.value - a.value;
	                    }).slice(0, 6)),
	                    symbolSize: function (val) {
	                        return val[2] / 10;
	                    },
	                    showEffectOn: 'render',
	                    rippleEffect: {
	                        brushType: 'stroke'
	                    },
	                    hoverAnimation: true,
	                    label: {
	                        normal: {
	                            formatter: '{b}',
	                            position: 'right',
	                            show: true
	                        }
	                    },
	                    itemStyle: {
	                        normal: {
	                            color: '#05C3F9',
	                            shadowBlur: 10,
	                            shadowColor: '#05C3F9'
	                        }
	                    },
	                    zlevel: 1
	                },
	
	            ]
	        };
	        
	        showCharts(ele, myChart, options, result);
        });
        
    });

},"山东地图");
