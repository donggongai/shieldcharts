// 扩展处理
ShieldCharts.addExtendFun("map_shandong", function(ele, url) {
	var dom = ele[0];
    var myChart = echarts.init(dom, ShieldCharts.theme);
    
    var geoCoordMap = {};
    
    $.getJSON(url, function(result){
    	var datas = result.data.data; //数据
        var titles = result.data.title; //表格标题
        var legends = result.data.axisName||[]; //标注
        var mapTitle = result.data.maptitle;
        var mapAreaName = "shandong"; //地图区域
    	var showName = result.data.showName||"未定义"; //重点展示区域
    	var adcode = result.data.adcode||"100000"; //区域编码用来获取数据
        
        $.getJSON(chartsjsPath +'/map/geodata/'+adcode+'.geoJson', function(result) {
    		var mapJson = {"type": "FeatureCollection"};
            mapJson.features = result.features;
    		echarts.registerMap(mapAreaName, mapJson ); //注册地区
    		
    		/*var geoCoordMap = {
	            '河池市': [108.085179,24.700488],
	            '柳州市': [109.412578,24.354875],
	            '梧州市': [111.323462,23.478238],
	            '南宁市': [108.359656,22.81328],
	            '北海市': [109.171374,21.477419],
	            '崇左市': [107.347374,22.377503]
	        };*/

	        /*获取地图数据*/
	        myChart.showLoading();
	        var mapFeatures = echarts.getMap(mapAreaName).geoJson.features;
	        myChart.hideLoading();
	        mapFeatures.forEach(function(v) {
	        	// 地区名称
	        	var name = v.properties.name;
	        	// 地区经纬度
	        	geoCoordMap[name] = v.properties.center;
	        	console.log(name+" "+geoCoordMap[name][0]+" "+geoCoordMap[name][1])
	        });
	            
	        var data = datas;
	        var max = 480,
	            min = 9; // todo 
	        var maxSize4Pin = 100,
	            minSize4Pin = 20;
	        var convertData = function (data) {
	            var res = [];
	            for (var i = 0; i < data.length; i++) {
	                var geoCoord = geoCoordMap[data[i].name];
	                if (geoCoord) {
	                    res.push({
	                        name: data[i].name,
	                        value: geoCoord.concat(data[i].value)
	                    });
	                }
	            }
	            return res;
	        };
	
	       var options = {
	            title: {
	                text: mapTitle,
	                subtext: '',
	                x: 'center',
	                textStyle: {
	                    color: '#FFF'
	                },
	                left: '6%',
	                top: '10%'
	            },
	            legend: {
	                orient: 'vertical',
	                y: 'bottom',
	                x: 'right',
	                data: ['pm2.5'],
	                textStyle: {
	                    color: '#fff'
	                }
	            },
	            visualMap: {
	                show: false,
	                min: 0,
	                max: 500,
	                left: 'left',
	                top: 'bottom',
	                text: ['高', '低'], // 文本，默认为数值文本
	                calculable: true,
	                seriesIndex: [1],
	                inRange: {}
	            },
	            geo: {
	                show: true,
	                map: mapAreaName,
	                mapType: mapAreaName,
	                label: {
	                    normal: {},
	                    //鼠标移入后查看效果
	                    emphasis: {
	                        textStyle: {
	                            color: '#fff'
	                        }
	                    }
	                },
	                //鼠标缩放和平移
	                roam: true,
	                itemStyle: {
	                    normal: {
	                        //          	color: '#ddd',
	                        borderColor: 'rgba(147, 235, 248, 1)',
	                        borderWidth: 1,
	                        areaColor: {
	                            type: 'radial',
	                            x: 0.5,
	                            y: 0.5,
	                            r: 0.8,
	                            colorStops: [{
	                                offset: 0,
	                                color: 'rgba(175,238,238, 0)' // 0% 处的颜色
	                            }, {
	                                offset: 1,
	                                color: 'rgba(	47,79,79, .1)' // 100% 处的颜色
	                            }],
	                            globalCoord: false // 缺省为 false
	                        },
	                        shadowColor: 'rgba(128, 217, 248, 1)',
	                        shadowOffsetX: -2,
	                        shadowOffsetY: 2,
	                        shadowBlur: 10
	                    },
	                    emphasis: {
	                        areaColor: '#389BB7',
	                        borderWidth: 0
	                    }
	                }
	            },
	            series: [{
	                    name: 'light',
	                    type: 'map',
	                    coordinateSystem: 'geo',
	                    data: convertData(data),
	                    itemStyle: {
	                        normal: {
	                            color: '#F4E925'
	                        }
	                    }
	                },
	                {
	                    name: '点',
	                    type: 'scatter',
	                    coordinateSystem: 'geo',
	                    symbol: 'pin',
	                    symbolSize: function (val) {
	                        var a = (maxSize4Pin - minSize4Pin) / (max - min);
	                        var b = minSize4Pin - a * min;
	                        b = maxSize4Pin - a * max;
	                        return a * val[2] + b;
	                    },
	                    label: {
	                        normal: {
	                            // show: true,
	                            // textStyle: {
	                            //     color: '#fff',
	                            //     fontSize: 9,
	                            // }
	                        }
	                    },
	                    itemStyle: {
	                        normal: {
	                            color: '#F62157', //标志颜色
	                        }
	                    },
	                    zlevel: 6,
	                    data: convertData(data),
	                },
	                {
	                    name: 'light',
	                    type: 'map',
	                    mapType: 'hunan',
	                    geoIndex: 0,
	                    aspectScale: 0.75, //长宽比
	                    showLegendSymbol: false, // 存在legend时显示
	                    label: {
	                        normal: {
	                            show: false
	                        },
	                        emphasis: {
	                            show: false,
	                            textStyle: {
	                                color: '#fff'
	                            }
	                        }
	                    },
	                    roam: true,
	                    itemStyle: {
	                        normal: {
	                            areaColor: '#031525',
	                            borderColor: '#FFFFFF',
	                        },
	                        emphasis: {
	                            areaColor: '#2B91B7'
	                        }
	                    },
	                    animation: false,
	                    data: data
	                },
	                {
	                    name: ' ',
	                    type: 'effectScatter',
	                    coordinateSystem: 'geo',
	                    data: convertData(data.sort(function (a, b) {
	                        return b.value - a.value;
	                    }).slice(0, 6)),
	                    symbolSize: function (val) {
	                        return val[2] / 10;
	                    },
	                    showEffectOn: 'render',
	                    rippleEffect: {
	                        brushType: 'stroke'
	                    },
	                    hoverAnimation: true,
	                    label: {
	                        normal: {
	                            formatter: '{b}',
	                            position: 'right',
	                            show: true
	                        }
	                    },
	                    itemStyle: {
	                        normal: {
	                            color: '#05C3F9',
	                            shadowBlur: 10,
	                            shadowColor: '#05C3F9'
	                        }
	                    },
	                    zlevel: 1
	                },
	
	            ]
	        };
	        
	        showCharts(ele, myChart, options);
        });
        
    });

},"山东地图");
