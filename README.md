### 图表可视化模块
![](https://img.shields.io/badge/release-1.0.0-blue.svg)
#### 使用说明
使用了echarts插件开发图表，js进行了插件化封装，可以采用定义html的形式来定义图表，简化操作。其中部分工具js用到了shieldjs中的js。

其中shieldcharts-core为核心js，可以单独使用。shieldcharts-demo为示例项目包含多个示例页面，maven打包引入后直接访问 [项目名][:端口]/charts/index.html即可，通过这个页面可以访问其他模板页面。内置了12种模板，为不断完善更新，模板来自于网上收集，如有侵权请告知删除。

可以使用data-xx扩展参数，如果data-xx不能满足，还可以使用shieldSettings扩展，示例：

使用data-url定义json数据路径【注意数据格式，格式在示例中有】，data-type定义图表类型。

```html
<div id="creditWarning" class="loadItem" data-url="data/companyNum.json" data-type="pie" style="height: 100%; width: 100%;" shieldSettings='{name:"test",show:false}'></div>
```

##### 使用示例

1、引入jar包

```html
<!-- 图表可视化模块 -->
<dependency>
	<groupId>com.ld</groupId>
	<artifactId>shieldcharts-demo</artifactId>
	<version>0.0.1-SNAPSHOT</version>
</dependency>
```

如果有js源码，也可以不引入jar包之间把js文件放到项目中

2、引入js

```html
<script type="text/javascript" src="${ctxPath }/charts/js/jquery-2.2.1.min.js"></script>
<script type="text/javascript" src="${ctxPath }/charts/js/echarts.min.js"></script>
<script type="text/javascript" src="${ctxPath }/charts/js/point_util.js"></script>
<!-- 依赖原数据插件，shieldjs中 -->
<script type="text/javascript" src="${ctxPath }/webjars/shieldjs/depends/jquery.metadata.js"></script>
<script type="text/javascript" src="${ctxPath }/webjars/shieldjs/core/shield.util.js"></script>
<!-- 处理 -->
<script type="text/javascript" src="${ctxPath }/charts/js/charts.deal.pie.js"></script>
<script type="text/javascript" src="${ctxPath }/charts/js/charts.deal.bar.js"></script>
<script type="text/javascript" src="${ctxPath }/charts/js/charts.deal.line.js"></script>
<script type="text/javascript" src="${ctxPath }/charts/js/charts.deal.list.js"></script>
<script type="text/javascript" src="${ctxPath }/charts/js/charts.deal.map.js"></script>
<script type="text/javascript" src="${ctxPath }/charts/js/charts.tool.js"></script>
```
3、定义html

```html
<!-- 饼状图  data-rose南丁格尔玫瑰图 data-label="close"关闭标签   -->
<div id="creditWarning" class="loadItem" data-url="data/companyNum.json" data-type="pie" style="height: 100%; width: 100%;"></div>

<!-- 柱形图,data-vx表示横向显示    -->
<div id="creditUseRanking" class="loadItem" data-url="data/chengliriqi.json" data-type="bar" data-vx="show" style="height: 100%"></div>
```

这样就可以显示了

##### 饼状图【pie】

饼状图包括饼状图、南丁格尔玫瑰图、环状图等。

html配置如下：

```html
<!-- 饼状图  data-rose南丁格尔玫瑰图 data-label="close"关闭标签   -->
<div id="creditWarning" class="loadItem" data-url="data/companyNum.json" data-type="pie" style="height: 100%; width: 100%;"></div>
<!-- 扩展参数：-->
<!-- data-rose，加上此属性则显示南丁格尔玫瑰图，有radius和area2种
//'radius' 面积展现数据的百分比，半径展现数据的大小。
//'area' 所有扇区面积相同，仅通过半径展现数据大小 -->
<!-- data-label="close",设置为cloase则关闭标签-->
<!--  data-legend='{"orient" : "vertical","x":"left","top":20}' 控制lengend的显示 -->
<!--  data-percent="show"，此参数控制是否显示百分百，默认不显示 -->
<!-- data-ring="70%"，加上此参数为环状图，里面的参数表示内环半径 -->
<!-- data-color='["#ff910b","#a2c32e","#fff000","#0988d7"]' 扩展颜色 -->
```
<span id="commondataresult">数据格式：</span>

```javascript
{
"#data注释":"data为数据结果集合，其中data为数据【必填】多行时为数组，一行时直接跟数据，title为每一组的标题，axisName为坐标显示标签【必填】，toolTip用于鼠标提示可不写不写默认使用axisName",
"data":{
	"data":[[10,20,33,58,79,101,211,289,335,700],[18,20,33,48,89,111,221,219,335,512]],
	"title":["企业数量","事业单位数量"],
	"axisName":["10","11","12","13","14","15","16","17","18","19年"],
	"toolTip":["2010年","2011年","2012年","2013年","2014年","2015年","2016年","2017年","2018年","2019年"]
},
"#success注释":"true表示执行成功，false表示执行失败",
"success":true,
"#message注释":"返回信息，一般错误时用",
"message":""
}
```

##### 柱状图【bar】

html配置如下：

```html
<!-- 柱形图,data-vx表示横向显示    -->
<div id="creditUseRanking" class="loadItem" data-url="data/chengliriqi.json" data-type="bar" data-vx="show" style="height: 100%"></div>
<!-- 扩展参数：-->
<!-- data-vx，横图显示，默认为竖图 -->
```
数据格式参考： [数据格式](#commondataresult)

##### 折线图【line】

html配置如下：

```html
<!--折线图,data-color覆盖颜色data-style="area " 模式设置多个模式用空格连接area面积模式 smooth平滑模式-->
<div id="creditProgress" class="loadItem" data-url="data/mulData.json" data-type="line" data-style="area " data-color='["#ffff43", "#17a390"]' style="height: 100%"></div>
<!-- 扩展参数：-->
<!-- data-style，模式，可以写area【面积模式】 smooth【平滑模式】，中间用空格连接 -->
```
数据格式参考： [数据格式](#commondataresult)
##### 列表显示【list】

html配置如下：

```html
<!-- 列表,data-width控制列宽度 -->
<div class="main_table loadItem" data-url="data/list.json" data-type="list" data-width="15%,,,15%"data-limit="5"></div>
<!-- 扩展参数：-->
<!-- data-width，控制每一列的宽度 -->
```
<span id="dataresult_list">数据格式：</span>

```javascript
{
"#data注释":"data里面data为数据【必填】多行时为数组，一行时直接跟数据，title为表头信息",

	"data":{
		"data":[
			{"count":1,"name":"市场监管局","value":10763,"bs":10},
			{"count":2,"name":"公安局","value":8763},
			{"count":3,"name":"大数据局","value":8463,"bs":550},
			{"count":4,"name":"环保局","value":8087},
			{"count":5,"name":"社保局","value":7351},
			{"count":6,"name":"社保局","value":7012},
			{"count":7,"name":"社保局","value":5351},
			{"count":8,"name":"社保局","value":5002},
			{"count":9,"name":"社保局","value":2351,"bs":120},
			{"count":10,"name":"农科局","value":46}],
		"title":["序号","部门名称","数据总量","报送量"]
	},
	"success":true,
	"message":""
}
```
##### 地图【map】
html配置如下：

```html
<!-- 焦作市地图 -->
<div id="jiaozuoMap" class="loadItem" data-url="data/mapdata_boai.json" data-type="map" style="width: 100%; height: 600px;"></div>
```
<span id="dataresult_map">数据格式：</span>

```javascript
{
"#说明":"地图数据文件与其他类型的数据文件不兼容多了name（地图注册区域）showName（突出显示的区域）adcode（地区编码，获取对应的地图geoJson数据，需要在geodata目录下存在对应的数据文件）",
	"data":{
		"data":[			{"archivesCount":0,"blackCount":0,"countType":0,"createBy":-1,"createTime":1557387003000,"departId":1537154097763,"departName":"市场监管局","id":1557387003344,"lastRefreshTime":1557816001000,"licensCount":10666,"modifyBy":-1,"punishCount":10,"redCount":0,"state":0,"sumCount":10676,"type":1,"updateTime":1557816001000},			{"archivesCount":0,"blackCount":0,"countType":0,"createBy":-1,"createTime":1557387005000,"departId":1544412689201,"departName":"政法委","id":1557387004695,"lastRefreshTime":1557816002000,"licensCount":0,"modifyBy":-1,"punishCount":0,"redCount":0,"state":0,"sumCount":0,"type":1,"updateTime":1557816002000},			{"archivesCount":0,"blackCount":0,"countType":0,"createBy":-1,"createTime":1557387005000,"departId":1544412714546,"departName":"组织部","id":1557387004727,"lastRefreshTime":1557816002000,"licensCount":0,"modifyBy":-1,"punishCount":0,"redCount":0,"state":0,"sumCount":0,"type":1,"updateTime":1557816002000},		{"archivesCount":0,"blackCount":0,"countType":0,"createBy":-1,"createTime":1557387005000,"departId":1544412734233,"departName":"宣传部","id":1557387004777,"lastRefreshTime":1557816002000,"licensCount":0,"modifyBy":-1,"punishCount":0,"redCount":0,"state":0,"sumCount":0,"type":1,"updateTime":1557816002000},			{"archivesCount":0,"blackCount":0,"countType":0,"createBy":-1,"createTime":1557387005000,"departId":1554953377123,"departName":"医疗保障局","id":1557387004858,"lastRefreshTime":1557816002000,"licensCount":0,"modifyBy":-1,"punishCount":0,"redCount":0,"state":0,"sumCount":0,"type":1,"updateTime":1557816002000},		{"archivesCount":0,"blackCount":0,"departName":"","licensCount":12417,"punishCount":46,"redCount":0,"sumCount":12463}],
		"name":"焦作",
		"showName":"博爱县",
		"adcode":410800
	},
	"success":true,
	"message":""
}
```

参考项目：
https://www.jianshu.com/p/c293c94d9ab7  echarts地图边界数据的实时获取与应用，省市区县多级联动【附最新geoJson文件下载】
https://gallery.echartsjs.com/editor.html?c=xmCAi_XNuJ 实时获取最新geojson数据



#### 公共参数

图表使用的公共参数：
##### data-autoload 【string】
本插件默认会自动发起ajax请求加载数据，如果不需要刻意设置为data-autoload="close"

##### data-url 【string】

ajax的url，获取远程数据
##### data-refresh 【number】

定时刷新的时间间隔，单位秒

##### data-xxx 【jsonString】

扩展原始参数
##### data-color 【string/Array】

上面data-xxx的一种，原始参数：颜色，可以是单独的一个字符串( data-color=''#48b')，也可以是数组( data-color='["#48b","#48b"]')，扩展的原始参数，具体参考官方文档。
##### data-grid 【string】

上面data-xxx的一种，原始参数：位置，为直角坐标系内绘图网格，可以设置 x y x2 y2 等值。这在控制图表摆放位置上，起了重要的作用。 
	x 为直角坐标系内绘图网格左上角横坐标，数值单位px，支持百分比（字符串），如'50%'(显示区域横向中心)
	y 为左上纵坐标，x2为右下横坐标，y2为右下纵坐标。
	也可以设置top、left、buttom、right，可以是像 20 这样的具体像素值，可以是像 '20%' 这样相对于容器高宽的百分比。
	如：data-grid='{"top":"3%"}' 位置，具体参考官方文档。
##### data-grid-style 【string】

简化data-grid的设置如data-grid-style="right:16%"。可以直接用逗号分隔定义按top,right,bottom,left顺序，如：data-grid-style=",16%,," 
如果规定一个值，比如 div {margin: 50px} - 所有的外边距都是 50 px
如果规定两个值，比如 div {margin: 50px 10px} - 上下外边距是 50px，左右外边距是 10 px。
如果规定三个值，比如 div {margin: 50px 10px 20px}- 上外边距是 50 px，而左右外边距是 10 px，下外边距是 20 px。
如果规定四个值，比如 div {margin: 50px 10px 20px 30px} - 上外边距是 50 px，右外边距是 10 px，下外边距是 20 px，左外边距是 30 px。
##### data-axis-color 【string】

轴线样式，#fff,line:#48b;2;solid,ticky:#333  axis颜色 line为分隔线样式分号(;)分隔颜色、线条粗细，样式 tickx ticky 轴线刻度样式分号(;)分隔颜色、线条粗细，样式
##### data-legend-style 【string】

data-legend-style="LT,vertical,#fff"  hide 隐藏
	可以定义图例的位置，朝向，颜色等，以逗号连接,其中位置需要定义在一个，其他无顺序要求，位置以首字母表示left right top bottom center（可自由组合，不区分大小写）
 	vertical 显示为竖向
 	可以设置icon circle,rect,roundRect,triangle,diamond,pin,arrow,none,也可以使用自定义的icon以icon:开头
 	x:70%可以使用百分比或者像素，前面需要用x:开头，y也是,
 	withItemColor表示跟随item的颜色显示，默认是固定的颜色
##### data-style 【string】

	label:insideRight label:top 只写label表示显示label，带冒号则设置显示位置
	notitle 无标题 
	notooltip 无tooltip
	avoidLabelOverlap防止label覆盖自动调整
	nosplity nosplitx 不显示分隔线
	noAxiStickX noAxiStickY 不显示x/y轴刻度线 nolinex noliney 不显示x/y轴线 nolabelx nolabely 不显示x/y轴标签
	extenddata 扩展数据，调用自定义方法，方法名为extenddata_[ele的id]拼接
	tooltip_Pointer_None
##### data-showtext 【string】

data-showtext="tooltip:,label:" tooltip后面为悬停显示，label为数据标签显示。yprefix:xx,定义y轴前缀，ysuffix:xx定义y轴后缀，xprefix:xx定义x轴前缀，xsuffix:xx定义x轴后缀。
##### shieldSettings 【jsonString】

扩展复杂的原始参数

#### 其他参数

图表使用的其他参数：
饼状图
data-ring 可以为字符串或者数组饼图的半径，数组的第一项是内半径，第二项是外半径。
data-rose radius半径模式 area面积模式默认radius
data-label="close" 关闭标签 center显示在中间
data-percent="show" tooltip显示百分比
data-style doublering双环图 halfring半环图 ringpie环饼图 anticlockwise逆时针 emphasis强调显示【中间加粗】 percent显示百分比 
data-itemborder 条目直接的border大小
data-itemStyle 定义原始的item属性


柱状图
data-mix="line" 多图混合，第二个图 结合返回数据的data.type使用，"type":[0,0,1],表示第3组数据未第二个图
data-mix-axis 第二个图yAxis设置
data-bar-width="20%"
data-item-color="#00c0e9>#3b73cf" 使用>（大于号）表示颜色渐变 "#00c0e9,#3b73cf" 使用,（半角逗号）表示依次取色，需要注意的是如果里面的值为rgba格式（如rgba(147, 235, 248, 1)）因为值里面本身包含,（半角逗号）所以改为;(半角分号) 
使用;（半角分号）分隔不同颜色值
data-radius 每个柱子的圆角
data-symbol 图表及大小用逗号连接
data-style topvalue 柱子顶部显示值  vertical横图 stack堆叠 pictorialBar异象图
data-vx 横图
data-tooltip-show // 显示效果'cross' | 'shadow'
data-delay="show"

折线图
data-area-color 面积渐变色
data-style area面积模式 smooth平滑模式 nospace x轴左右空白为0 vertical横图
data-symbol 图表及大小用逗号连接
data-tooltip-suffix tooltip后缀
data-area-color=#026B6F>#012138:0.8 渐变，冒号后面跟的是透明度，可以不写

数字显示器
data-animate="close"可关闭动画，可定义在父容器上也可单独定义
data-duration可定义动画时长（毫秒），可定义在父容器上也可单独定义
data-classprefix 样式前缀，防止多个数字显示器嵌套时显示冲突
data-startval="1234567" 
data-endval="1234567" 
data-num-prefix=""
data-num-suffix=""

list
data-limit="5" 限定显示列表长度,默认不限制
data-callback="listcallback" 加载完之后的回调
data-tooltip-show="1,2,3,4,5,6" 显示的字段下标
data-links="0[=url]" 连接,下标后可以跟字段名，不跟默认为url，注意此字段是否存在

map_extend 地图扩展
data-pointer="max:30,min:10,circle,rgba(255,255,0,0.8),index:0"显示数据点,max表示点最大大小，min最小大小 index表示下标当存在多层地图时可能需要调整index的值
data-tooltip-suffix="辆" 
data-tooltip-show="" 显示的字段
data-pointer="circle,max:10,min:3,rgba(255,255,0,0.8)" 			    
	控制点显示，circle显示图表默认支持circle|rect|roundRect|triangle|diamond|emptyCircle|emptyRectangle|emptyTriangle|emptyDiamond|pin|arrow|none
	也可以通过icon:xx定义，max最大大小min最小大小，后面控制颜色 index:1 可以控制位置
data-topdata="5"  max:5 color:[red/rgba(255,255,0,0.8)/#75ddff] max控制图表最大大小，默认为10
data-visual-style="1" //visualMap加到第一个数据系列上 color:#75ddff>#0e94eb text:高>低
	"color:#22e5e8>#0035f9>#22e5e8,orient:vertical,max:500"
	max:500 max:{max}/2 标尺的最大值
	bottom:10% 位置可定义left right top bottom
	calculable 标尺刻度显示
data-geo-style="area:#031525;#2B91B7 border:#097bba borderWidth:1" 设置区域和边框原色，用;分隔普通去和重点区，只写一个默认赋值给普通去，
data-style label 显示地区名称

扩展图：
data-stack-set="0,2;1:在库件,3;4:出库件,5,6"

#### 效果展示
##### echarts  

---
**饼状图：**   
```html
<div id="creditWarning" class="loadItem" data-url="data/companyNum.json" data-type="pie" style="height: 100%; width: 100%;"></div>
```
![图表](https://images.gitee.com/uploads/images/2020/0521/172205_5ff59baa_71734.png)  
**环状图：**  
```html
<!-- 饼状图  #61d2f7,data-percent显示百分比，data-ring内环，data-legend控制legend显示，data-label="close"关闭标签-->
<div id="creditEvent" class="loadItem" data-url="data/creditEvent.json" data-type="pie" data-legend='{"orient" : "vertical","x":"left","top":20}' data-label="close" data-percent="show" data-ring="70%" data-color='["#ff910b","#a2c32e","#fff000","#0988d7"]' style="height: 100%"></div>
```
![图表](https://images.gitee.com/uploads/images/2020/0521/172204_5fbe4277_71734.png)  

**柱状图：**  
```html
<!-- 柱形图,data-vx表示横向显示    -->
<div id="creditUseRanking" class="loadItem" data-url="data/chengliriqi.json" data-type="bar" data-vx="show" style="height: 100%"></div>
```
![图表](https://images.gitee.com/uploads/images/2020/0521/172204_ce9e4986_71734.png)   
&emsp;&emsp;
```html
<!-- 柱形图  data-color="#3398DB"可定义颜色，data-randomcolor="true"表示随机data-bar-width="50%"设置每个bar宽度   creditInfo.json data-tooltip-show="cross"悬浮框效果cross|shadow -->
<div id="creditInfo" class="loadItem" data-url="data/mulTypeData.json" data-type="bar" data-delay="10" data-randomcolor="true" data-mix='line' data-mix-axis='{"name": "增长率","min":0,"max": 100,"interval": 20,"axisLabel": {"formatter": "{value} %"}}' data-tooltip-show="cross" style="height: 100%"></div>
```
![图表](https://images.gitee.com/uploads/images/2020/0521/172204_de9f1550_71734.png)  
&emsp;&emsp;
```html
<!-- 柱形图  data-color="#3398DB"可定义颜色，data-randomcolor="true"表示随机data-bar-width="50%"设置每个bar宽度   creditInfo.json data-tooltip-show="cross"悬浮框效果cross|shadow -->
<div id="creditInfo" class="loadItem" data-url="data/creditInfo.json" data-type="bar" data-delay="10" data-randomcolor="true" data-tooltip-show="cross" data-bar-width="50%" style="height: 100%"></div>
```
![图表](https://images.gitee.com/uploads/images/2020/0521/172204_0c2fc31b_71734.png)

**折线图：**  
```html
 <!--折线图,data-color覆盖颜色data-style="area " 模式设置多个模式用空格连接area面积模式 smooth平滑模式-->
<div id="creditProgress" class="loadItem" data-url="data/mulData.json" data-type="line" data-style="area " data-color='["#ffff43", "#17a390"]' style="height: 100%"></div>
```
![图表](https://images.gitee.com/uploads/images/2020/0521/172204_17f2a9d8_71734.png) 
&emsp;&emsp;   

**地图：**  
```html
<!-- 焦作市地图 -->
<div id="jiaozuoMap" class="loadItem" data-url="data/mapdata_boai.json" data-type="map" style="width: 100%; height: 600px;"></div>
```
![图表](https://images.gitee.com/uploads/images/2020/0521/172206_650bfac1_71734.png) 
&emsp;&emsp;  

#### 模板展示  
在demo模块中已经内置了12种模板，后续会继续优化完善。  

[![58PnFP.md.png](https://z3.ax1x.com/2021/10/15/58PnFP.md.png)](https://imgtu.com/i/58PnFP)

[![58iFhV.md.png](https://z3.ax1x.com/2021/10/15/58iFhV.md.png)](https://imgtu.com/i/58iFhV)

列举几个如下：  

[![58F1Vs.md.png](https://z3.ax1x.com/2021/10/15/58F1Vs.md.png)](https://imgtu.com/i/58F1Vs)

[![58F3an.md.png](https://z3.ax1x.com/2021/10/15/58F3an.md.png)](https://imgtu.com/i/58F3an)

[![58FNxU.md.png](https://z3.ax1x.com/2021/10/15/58FNxU.md.png)](https://imgtu.com/i/58FNxU)

[![58FwqJ.md.png](https://z3.ax1x.com/2021/10/15/58FwqJ.md.png)](https://imgtu.com/i/58FwqJ)

[![58FBZ9.md.png](https://z3.ax1x.com/2021/10/15/58FBZ9.md.png)](https://imgtu.com/i/58FBZ9)

[![58FDaR.md.png](https://z3.ax1x.com/2021/10/15/58FDaR.md.png)](https://imgtu.com/i/58FDaR)

---


#### 版本说明
##### v1.0.0（2019-10-17）
- 初版发布;
- ;