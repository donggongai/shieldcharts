// 柱形图处理
var barDeal = function(ele, url) {
	ShieldCharts.getJSON(url, function(result) {
        var dom = ele[0];
        var myChart = echarts.init(dom, dom, ele.data("theme")||ShieldCharts.theme);
        
        var datas = result.data.data; //数据
        var legends = result.data.axisName||[]; //标注
    	var toolTips = result.data.toolTip||legends;//数据过滤掉前面的20 ，最后一个数据添加年
    	var titles = result.data.title||[];//数据过滤掉前面的20 ，最后一个数据添加年
    	var types = result.data.type||[];//多图混合用
        var isArray = $.isArray(datas[0]); //说明有多组数据
        
        var serieData = [];
		// 数据填充
        var optionsFillData = {};
        // 多图处理
        var mix = ele.data("mix"); //多图混合，第二个图
        var mixAxis = ele.data("mixAxis"); //多图混合，第二个图的设置
        if (mix) {
        	optionsFillData.mix = mix;
        	optionsFillData.mixAxis = mixAxis;
        }
    	if(isArray){ //数组循环
        	for (var i = 0; i < datas.length; i++) {
        		var data = datas[i];
        		optionsFillData.type = types[i];
        		fillData(data, serieData, titles[i], optionsFillData);
        	}
    		
    	} else { //非数组直接赋值
    		fillData(datas, serieData, titles[0], optionsFillData);
    	}
        var options = {
			title : {
				text : '', //类型切换
				subtext : '',
				textStyle : {
					color : "#61d2f7"
				}
			},
			tooltip : {
				trigger : 'axis',
				axisPointer : { // 坐标轴指示器，坐标轴触发有效
					type : 'shadow' // 默认为直线，可选为：'cross' | 'shadow'
				}
			},
/* 						legend : {
					data : legendData, //显示提示的标签   与下面series 对比
					textStyle : {
						color : "#61d2f7"
					}
				}, */
			grid : {
				left : '3%',
				right : '4%',
				bottom : '3%',
				containLabel : true
			},
			xAxis : {
				type : 'category',
				axisLine : { //坐标轴的颜色
					lineStyle : {
						//type : 'solid',
						color : '#61d2f7'//左边线和字体的颜色
						//,width : '2'//坐标线的宽度
					}
				},
				data : legends //y轴
			},
			yAxis : {
				type : 'value',
				splitLine : { //网格线
    				show : true,
    				lineStyle:{
                        color:'rgba(160,160,160,0.3)'
                    }
    			},
				axisLine : { //坐标轴的颜色
					lineStyle : {
						//type : 'solid',
						color : '#61d2f7'//左边线和字体的颜色
						//,width : '2'//坐标线的宽度
					}
				}
			},
		    legend: {
    	        data: titles,
    	        textStyle:{color:'#fff'}
    	    },
			//调色 盘颜色 默认 依次从右侧数组 读取
			color : [ '#df05ef', '#0066ff',' #e5e600', 'rgba(240,235,213,1)' ], //对应颜色   
			series :serieData
		};
		var orient = ele.data("vx"); //横图
		var commonStyle = ele.data("style");
		if (commonStyle && commonStyle.indexOf("vertical")>-1) {
			orient = "vertical";
		}
		if (orient) {
			options.yAxis.type="category";
			options.yAxis.data=legends;
			options.xAxis.type="value";
			options.xAxis.data=null;
        }
		var randomcolor = ele.data("randomcolor"); //随机显示柱子颜色
		if (randomcolor) {
			// 定制显示（按顺序）
			options.color = function(params) { 
                var colorList = ['#C33531','#EFE42A','#64BD3D','#EE9201','#29AAE3', '#B74AE5','#0AAF9F','#E89589','#16A085','#4A235A','#C39BD3 ','#F9E79F','#BA4A00','#ECF0F1','#616A6B','#EAF2F8','#4A235A','#3498DB' ]; 
                return colorList[params.dataIndex] 
            };
        }
		if (mix) { //多图混合
			// 定制显示（按顺序）
			var yAxis = [];
			yAxis.push(options.yAxis);
			var copiedObject = $.extend({}, options.yAxis); // 先把就的设置复制出来，直接改会把原来的也改掉
            var newObj = $.extend(true, copiedObject, mixAxis); //覆盖
			yAxis.push(newObj);
			options.yAxis =yAxis;
        }
		if (ele.data("delay")) { //延时动画效果
			options.animationEasing = 'elasticOut',
			options.animationDelayUpdate = function (idx) {
		        return idx * 5;
		    }
        }
		if (ele.data("tooltipShow")) { // 显示效果'cross' | 'shadow'
			options.tooltip.axisPointer.type = ele.data("tooltipShow")
        }
		console.log(options);
		showCharts(ele, myChart, options, result);
    });
	
	// 填充数据
	var fillData = function(datas, serieData, title, options) {
		var serie = {
			name : title,
			type : 'bar',
			data : datas
		};
		var style = ele.data("style")||"";
		if (style.indexOf("pictorialBar")>-1) {
			serie.type = "pictorialBar";
        }
		if (ele.data("symbol")) {
			var symbolArrs = ele.data("symbol").split(",");
			if (symbolArrs[0]) {
				serie.symbol = symbolArrs[0];
            }
			if (symbolArrs[1]) {
				serie.symbolSize = symbolArrs[1];
            }
        }
		
		var barWidth = ele.data("barwidth")||ele.data("barWidth"); //每个柱子的宽度
		if (barWidth) {
			serie.barWidth = barWidth;
        }
		if (ele.data("delay")) { //延时动画效果
			var delay = ele.data("delay");
			serie.animationDelay = function (idx) {
				return idx * 20;
			}
        }
		var mix = options.mix;
		if (mix) { //混合
			var type = options.type;
			if (type) {
				serie.type = mix;
				serie.yAxisIndex = 1;
            }
        }
		
		if (ele.data("itemColor")) { //柱子颜色，>表示渐变 ,表示间隔取色
			var itemColor = ele.data("itemColor");
			var itemColorArrs = itemColor.split(">");
			if (itemColorArrs.length >= 2) { //
				var colors = [];
				for (var i = 0; i < itemColorArrs.length; i++) {
					var areaColorVal = itemColorArrs[i];
					if (areaColorVal) {
						var areaColor = {
                            offset: 1/(itemColorArrs.length-1)*i,
                            color: itemColorArrs[i] //#25f3e6
                        };
						colors.push(areaColor);
                    }
                }
				 var serieExtend = {
					 itemStyle: { //图形样式
						 normal: {show: true,
							 // 线性渐变，前四个参数分别是 x0, y0, x2, y2, 范围从 0 - 1，相当于在图形包围盒中的百分比，如果 globalCoord 为 `true`，则该四个值是绝对的像素位置
							 //数组, 用于配置颜色的渐变过程. 每一项为一个对象, 包含offset和color两个参数. offset的范围是0 ~ 1, 用于表示位置
							 color: new echarts.graphic.LinearGradient(0, 0, 0, 0.8,colors, false)
						 }
					 }
				 };
				 extendOptions(serie, serieExtend);
	        }
			// 间隔色
			itemColorArrs = itemColor.split(",");
			if (itemColor.indexOf("rgba")>-1) { //rgba里面含有，不能这样拆分
				itemColorArrs = itemColor.split(";");
            }
			if (itemColorArrs.length >= 2) { //
				 var serieExtend = {
					 itemStyle: { //图形样式
						 normal: {show: true,
							 color: function(params) {
								 return itemColorArrs[params.dataIndex%itemColorArrs.length];
							 }
						 }
					 }
				 };
				 extendOptions(serie, serieExtend);
	        }
        }
		if (ele.data("radius")) { //圆角
			var serieExtend = {
				 itemStyle: { //图形样式
                    normal: {
                    	barBorderRadius: ele.data("radius")
                    }
                }
			}
			extendOptions(serie, serieExtend);
        }
		if (ele.data("style")) { //圆角
			var style = ele.data("style").toLowerCase();
			if (style.indexOf("topvalue")>-1 ) {
				var serieExtend = {
					itemStyle: { //图形样式
						normal: {
							label: {
	                            show: true,
	                            position: 'top',
	                            formatter: '{c}'
	                        }
						}
					}
				}
				extendOptions(serie, serieExtend);
            }
			if (style.indexOf("stack")>-1 ) {
				serie.stack = "stack"; //多个时需要处理
            }
        }
		
		serieData.push(serie);
	}
};