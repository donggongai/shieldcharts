// 总数处理
var countDeal = function(ele, url) {
	ShieldCharts.getJSON(url, function(result){
        var datas = result.data.data; //数据
        var titles = result.data.title; //表格标题
        
        var durationDefault = ele.data("duration")||2000; //动画持续时间
        var separateDefault = ele.data("separate"); //动画持续时间
        var classprefix = ele.data("classprefix")||"";
        
        var animate = ele.data("animate");
        var useAnimate = true;
        if (animate=="close") {
        	useAnimate = false;
        }
        
    	for (var i = 0; i < datas.length; i++) {
    		var countE = ele.find("." + classprefix + "_count"+i);
    		if (useAnimate) { 
    			countE.each(function() {
    				var $this = $(this);
    				var value = datas[i];
    				$this.data("endval", value);
    				$this.numAnimateShow({duration : durationDefault, separate : separateDefault});
    			})
            } else { //不使用动画
            	countE.text(datas[i]);
            }
    	}
    	var callback = ele.data("callback"); //回调函数
        if (callback) {
        	callback = eval(callback); //字符串转换为方法
        	callback(ele, result);
        }
	});
}
/**
* numAnimateShow
* @fileOverview 数字动态显示
* @author kevin Lv
* @email donggongai@126.com
* @version 3.0.0
* @date 2019-1-25
*/
;(function($){
	/**
	* @author kevin Lv
	* @constructor numAnimateShow
	* @description 数字动态显示
	* @see The <a href="#">kevin</a>
	* @example
	*	$("#form").numAnimateShow();
	*	$("#form").numAnimateShow({duration : durationDefault, separate : separateDefault});
	* @since version 2.0
	*/
	$.fn.numAnimateShow = function(options){
		//debug("选择器["+this.selector+"]调用自定义插件 numAnimateShow，调用元素数 "+this.size()+" 个[jQuery "+this.jquery+"]");
		/**
		* @description {Json} 设置参数选项
		* @field
		*/
		var options = $.extend({}, $.fn.numAnimateShow.defaults, options);
		/**
		* @description 处理函数
		*/
		var eles = $(this); //表单
		eles.each(function(){
			var $this = $(this);
			var startval = $this.data("startval")||0;
			var value = $this.data("endval")||$this.val()||$this.text();
			
			var animateThisE = $this.data("animate"); //当前元素是否关闭动画效果
			var useAnimateThisE = true;
	        if (animateThisE=="close") {
	        	useAnimateThisE = false;
            }
	        if (!useAnimateThisE) {
	        	$this.text(value);
            } else {
            	value = String(value).replace(/,/g,''); //千分位处理
            	var num = Number(value);
            	if (num) { //是数字
            		var duration = $this.data("duration")||options.duration; //当前元素的动画持续时间
            		var separate = $this.data("separate")||options.separate;
            		
            		var prefix = $this.data("numPrefix");
            		var suffix = $this.data("numSuffix");
            		
            		// Animate the element's value from x to y:
            		$({count: startval}).animate({count: num}, {
            			duration: duration, //动画持续时间
            			easing:'swing', // can be anything
            			step: function() { // called on every step
            				if (this.count) {
            					var showValue = Math.round(this.count);
            					showValue = formatNumberShow(showValue, separate, prefix, suffix);
            					$this.html(showValue); //四舍五入取整数
            				}
            				//console.log(this.count);
            			},
            			complete: function(){
            				var showValue = num;
            				showValue = formatNumberShow(num, separate, prefix, suffix);
            				$this.html(showValue);
            			}
            		});
                } else { // 非数字直接显示
                	$this.text(value);
                }
			}
		});
	};
	function formatNumberShow(val, separate, prefix, suffix){
		if (separate && separate!="close") { //不拆分
        	while (/(\d+)(\d{3})/.test(val.toString())){
        		val = val.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1"+separate);
        	}
        }
		if (prefix || suffix) {
			var valArrs = String(val).split("");
			var returnStr = "";
			var isArrayPrefix = $.isArray(prefix); //前缀
			var isArraySuffix = $.isArray(suffix); //后缀
	        for (var i = 0; i < valArrs.length; i++) {
	        	var prefixStr = prefix;
	        	var suffixStr = suffix;
        		if (isArrayPrefix) {
        			if (valArrs[i] == separate) {
        				prefixStr = prefix[1];
        			} else {
        				prefixStr = prefix[0];
					}
                }
        		if (isArraySuffix) {
        			if (valArrs[i] == separate) {
        				suffixStr = suffix[1]||suffix[0];
        			} else {
        				suffixStr = suffix[0];
        			}
                }
                
	        	returnStr += prefixStr+valArrs[i]+suffixStr;
            }
	        return returnStr;
        }
		return val;
	}
	$.fn.numAnimateShow.defaults = {
		/** 动画持续时间 */
		duration : 2000,
		/** 千分位分隔符 */
		separate : ','
	}
	
	//私有方法
	/**
	* @description 输出选中对象的个数到控制台
	* @param {msg} String 输出内容
	*/
	function debug(msg) {
		if (window.console && window.console.log){
			window.console.log('numAnimateShow.js log: ' + msg);
		}
	}
	$(".numAnimateShow").each(function() {
		var $this = $(this);
		var options = $.extend(true, $.fn.numAnimateShow.defaults, $this.data());
		$this.numAnimateShow(options);
    });
})(jQuery);        
