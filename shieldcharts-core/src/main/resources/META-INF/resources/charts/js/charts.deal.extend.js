// 扩展处理
// 全国地图，使用map_extend发现细节有出入，此处先不和并
ShieldCharts.addExtendFun("chinaMap", function(ele, url) {
	var dom = ele[0];
    var myChart = echarts.init(dom, ShieldCharts.theme);
	
    var nameColor = " rgb(55, 75, 113)"
    var name_fontFamily = '宋体'
    var name_fontSize = 35
    var mapName = 'china'
    var data = []
    var geoCoordMap = {};
    var toolTipData = [];
    var tooltipSuffix = ele.data("tooltipSuffix")||""; //显示的字段后缀data-tooltip-suffix
    var tooltipShow = ele.data("tooltipShow"); //显示的字段data-tooltip-show
    
    ShieldCharts.getJSON(url, function(result){
    	var datas = result.data.data; //数据
        var titles = result.data.title; //表格标题
        var data0 = []; //第一个数据集合
        var isArray = $.isArray(datas[0]); //说明有多组数据
        if(isArray){
        	data0 = datas[0];
        } else {
        	data0 = datas;
        }
        data = datas;
        
        
        /*获取地图数据*/
        myChart.showLoading();
        var mapFeatures = echarts.getMap(mapName).geoJson.features;
        myChart.hideLoading();
        mapFeatures.forEach(function(v) {
        	// 地区名称
        	var name = v.properties.name;
        	// 地区经纬度
        	console.log(name)
        	geoCoordMap[name] = v.properties.cp;
        });
        
        var options = mapExtendDeal(ele, mapName, data, geoCoordMap, result);
        showCharts(ele, myChart, options, result);
    });

},"中国地图");

// 扩展地图，非全国地图时适用
ShieldCharts.addExtendFun("map_extend", function(ele, url) {
	var dom = ele[0];
    var myChart = echarts.init(dom, ShieldCharts.theme);
    
    var geoCoordMap = {};
    
    ShieldCharts.getJSON(url, function(result){
    	var datas = result.data.data; //数据
        var titles = result.data.title; //表格标题
        var legends = result.data.axisName||[]; //标注
        var mapTitle = result.data.maptitle||"";
        var mapAreaName = result.data.name||"shandong"; //地图区域
    	var showName = result.data.showName||"未定义"; //重点展示区域
    	var adcode = result.data.adcode||"100000"; //区域编码用来获取数据
    	var tooltipSuffix = ele.data("tooltipSuffix")||""; //显示的字段后缀data-tooltip-suffix
        var tooltipShow = ele.data("tooltipShow"); //显示的字段data-tooltip-show
        
        ShieldCharts.getJSON(chartsjsPath +'/map/geodata/'+adcode+'.geoJson', function(areaResult) {
    		var mapJson = {"type": "FeatureCollection"};
            mapJson.features = areaResult.features;
    		echarts.registerMap(mapAreaName, mapJson ); //注册地区
    		
    		/*var geoCoordMap = {
	            '河池市': [108.085179,24.700488],
	            '柳州市': [109.412578,24.354875],
	            '梧州市': [111.323462,23.478238],
	            '南宁市': [108.359656,22.81328],
	            '北海市': [109.171374,21.477419],
	            '崇左市': [107.347374,22.377503]
	        };*/

	        /*获取地图数据*/
	        myChart.showLoading();
	        var mapFeatures = echarts.getMap(mapAreaName).geoJson.features;
	        myChart.hideLoading();
	        mapFeatures.forEach(function(v) {
	        	// 地区名称
	        	var name = v.properties.name;
	        	// 地区经纬度
	        	geoCoordMap[name] = v.properties.center;
	        	//console.log(name+"="+geoCoordMap[name][0]+"="+geoCoordMap[name][1])
	        });
	        
	        var covertName = function(name) {
	            if (adcode=="100000") {
	                if (name == "香港") {
	                	name = "香港特别行政区";
                    } else if (name == "澳门") {
	                	name = "澳门特别行政区";
                    } else if (name == "新疆") {
	                	name = "新疆维吾尔自治区";
                    } else if (name == "宁夏") {
	                	name = "宁夏回族自治区";
                    } else if (name == "西藏") {
	                	name = "西藏自治区";
                    } else if (name == "广西") {
	                	name = "广西壮族自治区";
                    } else if (name == "内蒙古") {
	                	name = "内蒙古自治区";
                    } else if (name == "北京" || name == "天津" || name == "上海" || name == "重庆") {
	                	name += "市";
                    } else if (!name.endsWith("省")){
                    	name += "省";
                    }
                }
	            return name;
            }
	            
	        var data = datas;
	        for (var i = 0; i < data.length; i++) {
	        	var name = data[i].name;
	        	data[i].name = covertName(name);
            }
	        
	        var options = mapExtendDeal(ele, mapAreaName, data, geoCoordMap, result);
	        
	       showCharts(ele, myChart, options, result);
        });
        
    });

},"地图扩展插件");
// 地图数据处理
var mapExtendDeal = function(ele, mapAreaName, data, geoCoordMap, result, options) {
    var titles = result.data.title; //表格标题
    var legends = result.data.axisName||[]; //标注
    var mapTitle = result.data.maptitle||"";
	var showName = result.data.showName||"未定义"; //重点展示区域
	var adcode = result.data.adcode||"100000"; //区域编码用来获取数据
	var tooltipSuffix = ele.data("tooltipSuffix")||""; //显示的字段后缀data-tooltip-suffix
    var tooltipShow = ele.data("tooltipShow"); //显示的字段data-tooltip-show
    
	
	var maxData = data.sort(function(a, b) {
    	return b.value - a.value; //倒序
    })[0].value||600;
    var minData = data.sort(function(a, b) {
    	return a.value - b.value; // 正序
    })[0].value||1;
    
    var max = maxData, //最大数据，maxData*5,
        min = minData; //最小数据
    var maxSize4Pin = 20,
        minSize4Pin = 5;
    
    var convertData = function (data, ele) {
        var res = [];
        var key = "name";
        if (ele && ele.data("dataname")) {
        	key = ele.data("dataname");
        }
        for (var i = 0; i < data.length; i++) {
        	var nameKey = data[i].name;
        	var showName = data[i][key];
            var geoCoord = geoCoordMap[nameKey];
            if (geoCoord) {
                res.push({
                    name: showName, //可能不带省市
                    value: geoCoord.concat(data[i].value)
                });
            }
        }
        return res;
    };
    

   var options = {
	   tooltip: {
           trigger: 'item',
           formatter: function(params) {
				var toolTiphtml = '';
				for(var i = 0;i<data.length;i++){
					if(params.name==data[i].name){
						var thisData = data[i];
						toolTiphtml += thisData.name+':<br>';
						var keys = Object.keys(thisData);
		        		for (var j = 0; j < titles.length; j++) {
		        			var key = keys[j];
		        			var showTooltip = true;
		        			if (tooltipShow && tooltipShow.indexOf(key)==-1) {
		        				showTooltip = false;
                           }
		        			if (key!="name" && showTooltip) {
		        				toolTiphtml += titles[j]+':'+(thisData[key]||"")+tooltipSuffix+"<br>";
                           }
	                    }
					}
				}
				// console.log(convertData(data))
				return toolTiphtml;
           }
       },
       title: {
            text: mapTitle,
            subtext: '',
            x: 'center',
            textStyle: {
                color: '#FFF'
            }
       },
       geo: {
            show: true,
            map: mapAreaName,
            mapType: mapAreaName,
            label: {
                normal: {},
                //鼠标移入后查看效果
                emphasis: {
                    textStyle: {
                        color: '#fff'
                    }
                }
            },
            roam: true,  //鼠标缩放和平移
            itemStyle: {
                normal: {
                    //color: '#ddd',
                    borderColor: 'rgba(147, 235, 248, 1)',
//                    borderWidth: 1,
                    areaColor: {
                        type: 'radial',
                        x: 0.5,
                        y: 0.5,
                        r: 0.8,
                        colorStops: [{
                            offset: 0,
                            color: 'rgba(175,238,238, 0)' // 0% 处的颜色
                        }, {
                            offset: 1,
                            color: 'rgba(47,79,79, .1)' // 100% 处的颜色
                        }],
                        globalCoord: false // 缺省为 false
                    },
                   // shadowColor: 'rgba(128, 217, 248, 1)',
                    //shadowOffsetX: -2,
                    //shadowOffsetY: 2,
                    //shadowBlur: 10
                },
                emphasis: {
                    areaColor: '#389BB7',
                    borderWidth: 0
                }
            }
       },
       series: [
    	   /*{
                name: 'light',
                type: 'map',
                coordinateSystem: 'geo',
                data: convertData(data),
                itemStyle: {
                    normal: {
                        color: '#F4E925'
                    }
                }
            },
            {
                name: '点',
                type: 'scatter',
                coordinateSystem: 'geo',
                symbol: 'pin', // 气球，默认圆
                symbolSize: function (val) {
                    var a = (maxSize4Pin - minSize4Pin) / (max - min);
                    var b = minSize4Pin - a * min;
                    b = maxSize4Pin - a * max;
                    return a * val[2] + b;
                },
                label: {
                    normal: {
                        // show: true,
                        // textStyle: {
                        //     color: '#fff',
                        //     fontSize: 9,
                        // }
                    }
                },
                itemStyle: {
                    normal: {
                        color: '#F62157', //标志颜色
                    }
                },
                zlevel: 6,
                data: convertData(data),
            },*/
            {
                name: 'light', // 区域悬停显示
                type: 'map',
//                mapType: mapAreaName,
                geoIndex: 0,
                selectedMode: 'multiple',
                aspectScale: 0.75, //长宽比
//                zoom: 2.2,   //这里是关键，一定要放在 series中
                showLegendSymbol: false, // 存在legend时显示
                label: {
                    normal: {
//                        show: false
                    },
                    emphasis: {
//                        show: false,
                        textStyle: {
                            color: '#fff'
                        }
                    }
                },
                roam: true, //鼠标缩放和平移
                /*itemStyle: {
                    normal: {
                        areaColor: '#031525',
                        borderColor: '#FFFFFF',
                    },
                    emphasis: {
                        areaColor: '#2B91B7'
                    }
                },*/
//                animation: false,
                data: data
            }
            /*,{
                name: 'Top 5', //前5
                type: 'effectScatter',
                coordinateSystem: 'geo',
                data: convertData(data.sort(function (a, b) {
                    return b.value - a.value;
                }).slice(0, 6)),
                symbolSize: function (val) {
                    return val[2] / (maxData/10);
                },
                showEffectOn: 'render',
                rippleEffect: {
                    brushType: 'stroke'
                },
                hoverAnimation: true,
                label: {
                    normal: {
                        formatter: '{b}',
                        position: 'right',
                        show: true
                    }
                },
                itemStyle: {
                    normal: {
                        color: '#fff',
                        shadowBlur: 10,
                        shadowColor: '#fff'
                    }
                },
                zlevel: 1
            }*/

            ]
   };
   var commonStyle = ele.data("style");
   if (commonStyle) {
	   if (commonStyle.indexOf(" label:") > -1 || commonStyle.indexOf(" label")>-1) {  //label显示位置,只写label表示显示带冒号则设置显示位置
		   options.geo.label.normal.show = true; //显示标签
	   } 
   }
   var geoStyle = ele.data("geoStyle");
   if (geoStyle ) {
	   geoStyle += "";
	   var borderWidthNormal,borderWidthEmphasis;
	   if (geoStyle.indexOf("borderWidth:")>-1) {
			var matchRegex = /borderWidth:(.+?)( |,|$)/; //,或者结束
			var matchResult = geoStyle.match(matchRegex);
			var borderWidth = matchResult?matchResult[1]:null;
			if (borderWidth.indexOf(";")==-1) {
				borderWidthNormal = borderWidth;
            } else {
            	var borderWidthArrs = borderWidth.split(";");
            	borderWidthNormal = borderWidthArrs[0];
            	borderWidthEmphasis = borderWidthArrs[1];
            }
	   } 
	   var areaColorNormal,areaColorEmphasis;
	   if (geoStyle.indexOf("area:")>-1) {
			var matchRegex = /area:(.+?)( |$)/; //空格或者结束
			var matchResult = geoStyle.match(matchRegex);
			var borderWidth = matchResult?matchResult[1]:null;
			if (borderWidth.indexOf(";")==-1) {
				areaColorNormal = borderWidth;
           } else {
           	var borderWidthArrs = borderWidth.split(";");
           	areaColorNormal = borderWidthArrs[0];
           	areaColorEmphasis = borderWidthArrs[1];
           }
	   }
	   var borderColorNormal,borderColorEmphasis;
	   if (geoStyle.indexOf("border:")>-1) {
			var matchRegex = /border:(.+?)( |$)/; //空格或者结束
			var matchResult = geoStyle.match(matchRegex);
			var borderWidth = matchResult?matchResult[1]:null;
			if (borderWidth.indexOf(";")==-1) {
				borderColorNormal = borderWidth;
           } else {
           		var borderWidthArrs = borderWidth.split(";");
           		borderColorNormal = borderWidthArrs[0];
           		borderColorEmphasis = borderWidthArrs[1];
           }
	   }
	   if (borderWidthNormal) {
		   var geoStyle = { geo : { itemStyle : { normal: {  borderWidth: borderWidthNormal } } } };
		   options = $.extend(true, options, geoStyle);
       }
	   if (areaColorEmphasis) {
		   var geoStyle = { geo : { itemStyle : { emphasis: {  borderWidth: borderWidthNormal } } } };
		   options = $.extend(true, options, geoStyle);
       }
	   if (areaColorNormal) {
		   var geoStyle = { geo : { itemStyle : { normal: {  areaColor: areaColorNormal } } } };
		   options = $.extend(true, options, geoStyle);
       }
	   if (areaColorEmphasis) {
		   var geoStyle = { geo : { itemStyle : { emphasis: {  areaColor: areaColorEmphasis } } } };
		   options = $.extend(true, options, geoStyle);
       }
	   if (borderColorNormal) {
		   var geoStyle = { geo : { itemStyle : { normal: {  borderColor: borderColorNormal } } } };
		   options = $.extend(true, options, geoStyle);
       }
	   if (borderColorEmphasis) {
		   var geoStyle = { geo : { itemStyle : { emphasis: {  borderColor: borderColorEmphasis } } } };
		   options = $.extend(true, options, geoStyle);
       }
	   options = $.extend(true, options, geoStyle);
   }
   var visualStyle = ele.data("visualStyle"); //默认不显示，所以允许值为空，为空即打开该配置
   if (visualStyle || visualStyle=="") {
	   if (!visualStyle) { //默认
		   visualStyle == "0";
	   }
	   visualStyle += "";
	   var seriesIndex = 0;
	   if (visualStyle.indexOf("index:")>-1) { 
				var matchRegex = /index:(.+?)( |,|$)/; //,或者结束
				var matchResult = visualStyle.match(matchRegex);
				seriesIndex = matchResult?matchResult[1]:null;
	   } else {
		   var matchRegex = /^(\d+?)( |,|$)/; //,或者结束
		   var matchResult = visualStyle.match(matchRegex);
		   seriesIndex = matchResult?Number(matchResult[1]):seriesIndex;
	   }
	   
	   var visualMapColor = null;
	   if (visualStyle.indexOf("color:")>-1) {
				var matchRegex = /color:(.+?)( |,|$)/; //,或者结束
				var matchResult = visualStyle.match(matchRegex);
				visualMapColor = matchResult?matchResult[1]:null;
				visualMapColor = visualMapColor.split(">");
	   }
	   var visualMapText = ['高', '低'];
	   if (visualStyle.indexOf("text:")>-1) {
				var matchRegex = /text:(.+?)( |,|$)/; //,或者结束
				var matchResult = visualStyle.match(matchRegex);
				visualMapText = matchResult?matchResult[1]:null;
				visualMapText = visualMapColor.split(">");
	   }
	   var visualMapOrient = "horizontal"; //朝向
	   if (visualStyle.indexOf("orient:")>-1) {
		   var matchRegex = /orient:(.+?)( |,|$)/; //,或者结束
		   var matchResult = visualStyle.match(matchRegex);
		   visualMapOrient = matchResult?matchResult[1]:null;
	   } else if (visualStyle.indexOf("vertical")>-1) {
		   visualMapOrient = "vertical";
	   }
	   var visualMapShow = true; //朝向
	   if (visualStyle.indexOf("hide")>-1) {
		   visualMapShow = false;
	   }
	   var minValue = 0;
	   var maxValue = maxData;
	   if (visualStyle.indexOf("min:")>-1) {
			var matchRegex = /min:(.+?)( |,|$)/; //,或者结束
			var matchResult = visualStyle.match(matchRegex);
			minValue = matchResult?matchResult[1]:null;
			minValue = eval(minValue.replaceAll ("{max}",maxData));
	   }
	   if (visualStyle.indexOf("max:")>-1) {
			var matchRegex = /max:(.+?)( |,|$)/; //,或者结束
			var matchResult = visualStyle.match(matchRegex);
			maxValue = matchResult?matchResult[1]:null;
			maxValue = eval(maxValue.replaceAll ("{max}",maxData));
	   }
	   var left;
	   if (visualStyle.indexOf("left:")>-1) {
			var matchRegex = /left:(.+?)( |,|$)/; //,或者结束
			var matchResult = visualStyle.match(matchRegex);
			left = matchResult?matchResult[1]:null;
	   }
	   var right;
       if (visualStyle.indexOf("right:")>-1) {
       		var matchRegex = /right:(.+?)( |,|$)/; //,或者结束
       		var matchResult = visualStyle.match(matchRegex);
       		right = matchResult?matchResult[1]:null;
       }
       var top;
       if (visualStyle.indexOf("top:")>-1) {
       		var matchRegex = /top:(.+?)( |,|$)/; //,或者结束
       		var matchResult = visualStyle.match(matchRegex);
       		top = matchResult?matchResult[1]:null;
       }
       var bottom;
       if (visualStyle.indexOf("bottom:")>-1) {
       		var matchRegex = /bottom:(.+?)( |,|$)/; //,或者结束
       		var matchResult = visualStyle.match(matchRegex);
       		bottom = matchResult?matchResult[1]:null;
       }
       var calculable = false; //朝向
	   if (visualStyle.indexOf("calculable")>-1) {
		   calculable = true;
	   }
	   var visualMapStyle = {
		   visualMap : {
			   show : visualMapShow,
			   min: minValue,
			   max: maxValue,
			   /*left: 'left',
            	top: 'bottom',*/
			   orient: visualMapOrient, //'horizontal',
			   text: visualMapText, //['高', '低'], // 文本，默认为数值文本
			   calculable: calculable, //标尺刻度显示
			   seriesIndex: [seriesIndex],
			   itemWidth: 15,
			   itemHeight: 200,
			   right: 0,
			   bottom: 10,
			   inRange: {
				   //color: ['#75ddff', '#0e94eb']
			   },
			   textStyle: {
				   color: 'white'
			   }
		   }
	   };
	   if (left) {visualMapStyle.visualMap.left = left;}
	   if (right) {visualMapStyle.visualMap.right = right;}
	   if (top) {visualMapStyle.visualMap.top = top;}
	   if (bottom) {visualMapStyle.visualMap.bottom = bottom;}
	   if (visualMapColor) {
		   visualMapStyle.visualMap.inRange.color = visualMapColor;
	   }
	   options = $.extend(true, options, visualMapStyle);
   }
   
   // 需要控制是否显示点，是否重点显示前几名data-pointer="pin,#F62157[rgba(255,255,0,0.8)]"
   var pointerStr = ele.data("pointer");
   var colorRegex1 = /(#([0-9a-fA-F]{6}|[0-9a-fA-F]{3}))/; //匹配#000-#fff表示法的颜色
   var colorRegex2 = /([rR][gG][Bb][Aa]?[\(]([\s]*(2[0-4][0-9]|25[0-5]|[01]?[0-9][0-9]?),){2}[\s]*(2[0-4][0-9]|25[0-5]|[01]?[0-9][0-9]?),?[\s]*(0\.\d{1,2}|1|0)?[\)]{1})/g; //匹配rgba(122,122,122) rgba(122,122,122,0.89)
   if (pointerStr || pointerStr=="") {
	   var pointerColor = "#F62157";
	   if (colorRegex1.test(pointerStr)) {
		   pointerColor = RegExp.$1;
	   } else if(colorRegex2.test(pointerStr)) {
		   pointerColor = RegExp.$1;
	   } else if (pointerStr.indexOf("color:")>-1) {
		   var matchRegex = /color:(.+?)( |$)/; //,或者结束
		   var matchResult = pointerStr.match(matchRegex);
		   pointerColor = matchResult?matchResult[1]:null;
	   }
	   var maxSize4Pin = maxSize4Pin;
	   if (pointerStr.indexOf("max:")>-1) {
				var matchRegex = /max:(.+?)( |,|$)/; //,或者结束
				var matchResult = pointerStr.match(matchRegex);
				maxSize4Pin = matchResult?matchResult[1]:null;
	   }
	   var minSize4Pin = minSize4Pin;
       if (pointerStr.indexOf("min:")>-1) {
       		var matchRegex = /min:(.+?)( |,|$)/; //,或者结束
       		var matchResult = pointerStr.match(matchRegex);
       		minSize4Pin = Number(matchResult?matchResult[1]:null);
       }
       var pointerIndex = -1
       if (pointerStr.indexOf("index:")>-1) { 
				var matchRegex = /index:(.+?)( |,|$)/; //,或者结束
				var matchResult = pointerStr.match(matchRegex);
				pointerIndex = matchResult?matchResult[1]:null;
	   }
	   var pointerStyle = {
			   name: '点', 
			   type: 'scatter', 
			   coordinateSystem: 'geo',
			   symbol: 'pin', // 气球，默认圆
			   symbolSize: function (val) {
				   // 最小数据和最大数据有可能相等,相等时直接取最大值
				   var a = 1;
				   var b = maxSize4Pin;
				   if (max != min) {
					   a = (maxSize4Pin - minSize4Pin) / (max - min); //平均每份间隔
					   b = minSize4Pin;
				   }
				   var value = (val[2]-min)*a+ b;
				   //console.log(value+" maxSize4Pin:"+maxSize4Pin+" minSize4Pin:"+minSize4Pin+" max:"+max+" min:"+min+" val:"+val[2]+" a="+a+" b="+b);
				   return value;
			   },
			   label: {
				   normal: {
					   // show: true,
					   // textStyle: {
						   //     color: '#fff',
					   //     fontSize: 9,
					   // }
				   }
			   },
			   zlevel: 6,
			   data: convertData(data, ele)
	   }
	   if (pointerColor) { //颜色
		   var pointerStyleExtend = {
			   itemStyle: {
				   normal: {
					   color: pointerColor //标志颜色
				   }
			   }
		   }
		   pointerStyle = $.extend(true, pointerStyle, pointerStyleExtend);
	   }
	   var pointerIcon = null;
	   var iconRegex1 = /(circle|rect|roundRect|triangle|diamond|emptyCircle|emptyRectangle|emptyTriangle|emptyDiamond|pin|arrow|none)/; //icon
	   if (pointerStr.indexOf("icon:")>-1) { //自定义icon
		   var iconRegex = /icon:(\w+?)(,|$)/; //需要以,结束
		   var matchResult = pointerStr.match(iconRegex);
		   pointerIcon = matchResult?matchResult[1]:null;
	   } else if (iconRegex1.test(pointerStr)) {
		   pointerIcon = RegExp.$1;
	   } else if (pointerStr.indexOf("car")>-1) {
		   pointerIcon = "path://M874.666667 469.333333c17.28 42.666667 42.666667 85.333333 42.666666 128v213.333334c0 27.946667-23.893333 42.666667-64 42.666666s-64-17.28-64-42.666666v-42.666667H234.666667v42.666667c0 25.386667-23.893333 42.666667-64 42.666666s-64-14.72-64-42.666666V597.333333c0-42.666667 25.386667-85.333333 42.666666-128s-64-37.333333-64-64 5.333333-42.666667 42.666667-42.666666h64s30.293333-87.04 42.666667-128 64-64 106.666666-64h341.333334c42.666667 0 94.293333 23.04 106.666666 64s42.666667 128 42.666667 128h64c37.333333 0 42.666667 16 42.666667 42.666666s-81.28 21.333333-64 64z m-469.333334 149.333334a21.333333 21.333333 0 0 0 21.333334 21.333333h170.666666a21.333333 21.333333 0 0 0 21.333334-21.333333v-21.333334a21.333333 21.333333 0 0 0-21.333334-21.333333h-170.666666a21.333333 21.333333 0 0 0-21.333334 21.333333v21.333334zM192 618.666667a64 64 0 1 0 64-64 64 64 0 0 0-64 64z m597.333333-256c0-21.333333-42.666667-106.666667-42.666666-106.666667H277.333333s-42.666667 85.333333-42.666666 106.666667v21.333333a42.666667 42.666667 0 0 0 42.666666 42.666667h469.333334a42.666667 42.666667 0 0 0 42.666666-42.666667v-21.333333z m-21.333333 192a64 64 0 1 0 64 64 64 64 0 0 0-64-64z";
	   } else if (pointerStr.indexOf("stop")>-1) {
		   pointerIcon = "path://M30.9,53.2C16.8,53.2,5.3,41.7,5.3,27.6S16.8,2,30.9,2C45,2,56.4,13.5,56.4,27.6S45,53.2,30.9,53.2z M30.9,3.5C17.6,3.5,6.8,14.4,6.8,27.6c0,13.3,10.8,24.1,24.101,24.1C44.2,51.7,55,40.9,55,27.6C54.9,14.4,44.1,3.5,30.9,3.5z M36.9,35.8c0,0.601-0.4,1-0.9,1h-1.3c-0.5,0-0.9-0.399-0.9-1V19.5c0-0.6,0.4-1,0.9-1H36c0.5,0,0.9,0.4,0.9,1V35.8z M27.8,35.8 c0,0.601-0.4,1-0.9,1h-1.3c-0.5,0-0.9-0.399-0.9-1V19.5c0-0.6,0.4-1,0.9-1H27c0.5,0,0.9,0.4,0.9,1L27.8,35.8L27.8,35.8z";
	   } else if (pointerStr.indexOf("diewu")>-1){ 
		   pointerIcon = "path://M10,50 Q30,100 50,50 T80,70";
	   }
	   if (pointerIcon) { //icon
		   var pointerStyleExtend = {symbol: pointerIcon }
		   pointerStyle = $.extend(true, pointerStyle, pointerStyleExtend);
	   }
	   if (pointerIndex > -1) { 
		   options.series.splice(pointerIndex, 0, pointerStyle);
	   } else {
		   options.series.push(pointerStyle);
	   }
   }
   
   // 需要控制是否显示前几名，是否重点显示前几名data-top="pin;#F62157[rgba(255,255,0,0.8)]"
   var topDataStr = ele.data("topdata");
   if (topDataStr) {
	   topDataStr +=""; //转字符串
	   var topNum = 5;
	   var matchRegex = /(\d+?)( |,|$)/; //,或者结束
	   var matchResult = topDataStr.match(matchRegex);
	   topNum = matchResult?Number(matchResult[1]):null;
	   var topColor = '#61d2f7';
	   colorRegex2.lastIndex = 0; //清除全局正则的缓存
	   if (colorRegex1.test(topDataStr)) {
		   	topColor = RegExp.$1;
	   } else if(colorRegex2.test(topDataStr)) {
		   	topColor = RegExp.$1;
	   } else if (topDataStr.indexOf("color:")>-1) {
       		var matchRegex = /color:(.+?)( |$)/; //,或者结束
       		var matchResult = topDataStr.match(matchRegex);
       		topColor = matchResult?matchResult[1]:null;
	   }
	   var maxSymbolSize = 10; //最大10
	   if (topDataStr.indexOf("max:")>-1) {
		   var matchRegex = /max:(.+?)( |,|$)/; //,或者结束
		   var matchResult = topDataStr.match(matchRegex);
		   maxSymbolSize = matchResult?matchResult[1]:null;
	   }
	   var topStyle = { 
		   name: 'Top '+topNum, //前5
		   type: 'effectScatter',
		   coordinateSystem: 'geo',
		   data: convertData(data.sort(function (a, b) {
			   return b.value - a.value;
		   }).slice(0, topNum), ele),
		   symbolSize: function (val) {
			   return val[2] * maxSymbolSize/maxData;
		   },
		   showEffectOn: 'render',
		   rippleEffect: {
			   brushType: 'stroke'
		   },
		   hoverAnimation: true,
		   label: {
			   normal: {
				   formatter: '{b}',
				   position: 'right',
				   show: true
			   }
		   },
		   itemStyle: {
			   normal: {
				   color: topColor, //字体颜色
				   shadowBlur: 10,
				   shadowColor: '#fff'
			   }
		   },
		   zlevel: 1
	   }
	   options.series.push(topStyle);
   }
   return options;
}
// 横向柱状图，占比图
ShieldCharts.addExtendFun("barPercent", function(ele, url) {
	var dom = ele[0];
    var myChart = echarts.init(dom, ShieldCharts.theme);
    
    ShieldCharts.getJSON(url, function(result){
    	var datas = result.data.data; //数据
        var titles = result.data.title; //表格标题
        var legends = result.data.axisName||[]; //标注
        
        var data = datas[0];
        var valdata = datas[1];
        var myColor = ['#1089E7', '#F57474', '#56D0E3', '#F8B448', '#8B78F6'];
        var containerData = [];
        for (var i = 0; i < data.length; i++) {
        	containerData.push(100);
        }
        
        var options = {
            title: {
                text: titles[0],
                x: 'center',
                textStyle: {
                    color: '#FFF'
                },
                left: '6%',
                top: '10%'
            },
            //图标位置
            grid: {
                top: '20%',
                left: '100'
            },
            xAxis: {
                show: false
            },
            yAxis: [{
                show: true,
                data: legends,
                inverse: true,
                axisLine: {
                    show: false
                },
                splitLine: {
                    show: false
                },
                axisTick: {
                    show: false
                },
                axisLabel: {
                    color: '#fff',
                    formatter: function(value, index){
                        return [
                            `{lg|${index+1}}  ` + '{title|' + value + '} '
                        ].join('\n')
                    },
                    rich: {
                        lg: {
                            backgroundColor: '#339911',
                            color: '#fff',
                            borderRadius: 15,
                            // padding: 5,
                            align: 'center',
                            width: 15,
                            height: 15
                        }
                    }
                },


            }, {
                show: true,
                inverse: true,
                data: valdata,
                axisLabel: {
                    textStyle: {
                        fontSize: 12,
                        color: '#fff',
                    },
                },
                axisLine: {
                    show: false
                },
                splitLine: {
                    show: false
                },
                axisTick: {
                    show: false
                },

            }],
            series: [{
                name: '条',
                type: 'bar',
                yAxisIndex: 0,
                data: data,
                barWidth: 10,
                itemStyle: {
                    normal: {
                        barBorderRadius: 20,
                        color: function(params) {
                            var num = myColor.length;
                            return myColor[params.dataIndex % num]
                        },
                    }
                },
                label: {
                    normal: {
                        show: true,
                        position: 'inside',
                        formatter: '{c}%'
                    }
                },
            }, {
                name: '框',
                type: 'bar',
                yAxisIndex: 1,
                barGap: '-100%',
                data: containerData,
                barWidth: 15,
                itemStyle: {
                    normal: {
                        color: 'none',
                        borderColor: '#00c1de',
                        borderWidth: 3,
                        barBorderRadius: 15,
                    }
                }
            }, ]
        };
        
        showCharts(ele, myChart, options, result);
    });

},"横向占比柱状图");
// 柱状阴影图
ShieldCharts.addExtendFun("barShadow", function(ele, url) {
	var dom = ele[0];
    var myChart = echarts.init(dom, ShieldCharts.theme);
    
    ShieldCharts.getJSON(url, function(result){
    	var datas = result.data.data; //数据
        var titles = result.data.title; //表格标题
        var legends = result.data.axisName||[]; //标注
        
        var valdata = datas[1];
        var myColor = ["#0e94eb", "rgba(239,176,19,.9)", "rgba(239,176,19,0.3)", "rgba(196,64,239,0.8)", "rgba(196,64,239,0.4)", "rgba(219,44,44,0.8)", "rgba(239,176,19,0.4)"];
        if (ele.data("stackColor")) {
        	myColor = ele.data("stackColor");
        }
        
        var series_data; //绘制图表的数据
        //绘制图表
        var yMax = 0;
        for (var j = 0; j < datas.length; j++) {
            if (yMax < datas[j]) {
                yMax = datas[j];
            }
        }
        
        var getColor = function(index) {
        	if (index >= myColor.length) {
        		index = index%myColor.length; //取余
            }
	        return myColor[index];
        }
        
        var dataShadow = [];
        series_data = [
            { // For shadow
                type: 'bar',
                barWidth: 20,
                xAxisIndex: 2,
                tooltip: {
                    show: false
                },
                itemStyle: {
                    normal: {
                        color: 'rgba(14, 148, 235, 0.102)'
                    }
                },
                data: dataShadow,
                animation: false
            }];
        var stack = ele.data("stackSet");
        var xAxisNames = [];
        var longIndex = 0; //真实下标
        if (stack) {
	        var stackArrs = stack.split(","); //按逗号分隔 0,1:title,2;3:title,最后一个为标题
	        for (var i = 0; i < stackArrs.length*2; i++) {
	        	dataShadow.push(yMax * 2);
	        }
	        console.log(dataShadow);
	        
	        for (var i = 0; i < stackArrs.length; i++) {
	            var stackArrStr = stackArrs[i];
	        	var dataIndex = stackArrStr; //数据下标
	        	var title = legends[dataIndex];
	        	var stack = false;
	        	
	        	
	        	var dataExtend = {
        			name: title,
        			type: 'bar',
        			//barGap: '-100%',
        			//barWidth: '40%',
        			xAxisIndex: 1,
        			itemStyle: {
        				normal: {
        					color: '#0e94eb'
        				},
        				emphasis: {
        					opacity: 1
        				}
        			}
	        		//,data: [data_[0], 0, 0, 0, 0],
	        	};
	        	if (i==0) {
	                dataExtend.barGap = '-100%';
	                dataExtend.barWidth ='40%';
                }
	        	
	        	if (stackArrStr.indexOf(":")>-1) { //包含标题
	        		var stackArrStrLong = stackArrStr.split(":");
	        		var stackName = stackArrStrLong[1];
	        		xAxisNames.push(stackName);
	        		var stackArrStrLong0 = stackArrStrLong[0];
	        		if (stackArrStrLong0.indexOf(";")) { //包含多个下标，叠加
	        			stack = true;
	        			var stackArrStrLongIndexs = stackArrStrLong0.split(";");
	        			for (var i1 = 0; i1 < stackArrStrLongIndexs.length; i1++) { //循环所有下标
	        				var dataExtendNew = JSON.parse(JSON.stringify(dataExtend)); //深层拷贝
	        				
	        				dataIndex = stackArrStrLongIndexs[i1];
	        				title = legends[dataIndex];
	        				var dataArrs = [];
	        				for (var j = 0; j < stackArrs.length; j++) {
	        					if (j == i) {
	        						dataArrs[j] = {name:title,value:datas[dataIndex]};;
	        					} else {
	        						dataArrs[j] = 0;
	        					}
	        				}
	        				dataExtendNew.itemStyle.normal.color = getColor(longIndex); //获取颜色
	        				dataExtendNew.name = title;
	        				dataExtendNew.stack = stackName;
	        				dataExtendNew.data = dataArrs;
	        				
	        				series_data.push(dataExtendNew);
	        				longIndex ++;
                        }
	        			
                    } else { //单个元素，不叠加
                    	dataIndex = stackArrStrLong0;
                    	var dataArrs = [];
                    	for (var j = 0; j < stackArrs.length; j++) {
	                        if (j == i) {
	                        	dataArrs[j] = {name:title,value:datas[dataIndex]};;
                            } else {
                            	dataArrs[j] = 0;
                            }
                        }
//                    	dataExtend.name = title;
                    	dataExtend.itemStyle.normal.color = getColor(longIndex); //获取颜色
                    	dataExtend.data = dataArrs;
                    	series_data.push(dataExtend);
                    	longIndex ++;
                    }
                } else { //不包含标题
                	xAxisNames.push(title);
                	var dataArrs = [];
                	for (var j = 0; j < stackArrs.length; j++) {
                        if (j == i) {
                        	dataArrs[j] = {name:title,value:datas[dataIndex]};
                        } else {
                        	dataArrs[j] = 0;
                        }
                    }
                	dataExtend.itemStyle.normal.color = getColor(longIndex); //获取颜色
                	dataExtend.data = dataArrs;
                	series_data.push(dataExtend);
                	longIndex ++;
                }
            }
        }
        console.log(series_data)
        
        var options = {
	        title: '',
	        grid: {
	            top: '5%',
	            bottom: '3%',
	            containLabel: true
	        },
	        tooltip: {
	            show: true
	        },
	        xAxis: [{
	                type: 'category',
	                show: false,
	                data: xAxisNames,
	                axisLabel: {
	                    textStyle: {
	                        color: '#fff'
	                    }
	                }
	            },
	            {
	                type: 'category',
	                position: "bottom",
	                data: xAxisNames,
	                boundaryGap: true,
	                // offset: 40,
	                axisTick: {
	                    show: false
	                },
	                axisLine: {
	                    show: false
	                },
	                axisLabel: {
	                    textStyle: {
	                        color: '#fff'
	                    }
	                }
	            },
	            {
	                show: false,
	                data: dataShadow,
	                axisLabel: {
	                    inside: true,
	                    textStyle: {
	                        color: '#fff'
	                    }
	                },
	                axisTick: {
	                    show: false
	                },
	                axisLine: {
	                    show: false
	                },
	                z: 10
	        },
	        ],
	        yAxis: [{
	                show: true,
	                splitLine: {
	                    show: false,
	                    lineStyle: {
	                        color: "#0e94eb"
	                    }
	                },
	                axisTick: {
	                    show: false
	                },
	                axisLine: {
	                    show: false
	                },
	                axisLabel: {
	                    show: true,
	                    color: '#0e94eb'
	                }
	        }, {
	                show: false,
	                type: "value",
	                nameTextStyle: {
	                    color: '#0e94eb'
	                },
	                axisLabel: {
	                    color: '#0e94eb'
	                },
	                splitLine: {
	                    show: false
	                },
	                axisLine: {
	                    show: false
	                },
	                axisTick: {
	                    show: false
	                }
	        },
	            {
	                axisLine: {
	                    show: false
	                },
	                axisTick: {
	                    show: false
	                },
	                axisLabel: {
	                    textStyle: {
	                        color: '#999'
	                    }
	                }
	                }],
	        //        color: ['#e54035'],
	        series: series_data
	    }
        
        showCharts(ele, myChart, options, result);
    });

}, "柱状阴影图");