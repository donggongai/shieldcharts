// 列表页处理，可能需要自定义字段，此处未处理
var listDeal = function(ele, url) {
    var tooltipShowStr = ele.data("tooltipShow"); //显示的字段data-tooltip-show，此处为下标从0开始
    var links = ele.data("links"); //显示的字段data-tooltip-show
    if (links=="") {
    	links = "0";
    }
    var tooltipShow = null;
    if (tooltipShowStr) {
    	tooltipShow = tooltipShowStr.split(",");
    }
    
    var linkIndexs = []; //增加超链接的下标
    var linkNames = []; //超链接字段
    if (links) {
	    var linksArrs =  (links+"").split(",");
	    for (var i = 0; i < linksArrs.length; i++) {
	    	var linksStr = linksArrs[i]; 
	    	if (linksStr.indexOf("=")>-1) { //=链接下标和对应的属性，如果没有下标则使用默认的url
	    		 var linksStrArrs =  linksStr.split("=");
	    		 linkIndexs[i] = linksStrArrs[0];
	    		 linkNames[i] = linksStrArrs[1];
            } else {
            	linkIndexs[i] = linksStr;
	    		 linkNames[i] = "url";
			}
	        
        }
    }
	
    ShieldCharts.getJSON(url, function(result){
        var datas = result.data.data; //数据
        var titles = result.data.title; //表格标题
        
        //前提是titles与datas里面每个对象的参数个数一样
        var data0 = []; //第一个数据集合
        var isArray = $.isArray(datas[0]); //说明有多组数据
        if(isArray){
        	data0 = datas[0];
        } else {
        	data0 = datas;
        }
        
        var widthStr = ele.data("width");
        
        
        var tableHtml = '<table class="loadDataTable">';
        if (widthStr) {
        	var widthArrs = widthStr.split(",");
        	for (var i = 0; i < widthArrs.length; i++) {
        		tableHtml += '<col width="'+widthArrs[i]+'" />';
            }
        }
        if (titles && titles.length>0) {
        	tableHtml += '<thead><tr>';
        	for (var i=0;i<titles.length;i++) {
        		var showTooltip = true;
        		if (tooltipShow && tooltipShow.indexOf(i+"")==-1) {
        			showTooltip = false;
        		}
        		if (showTooltip) {
        			tableHtml += ' <th>'+titles[i]+'</th>';
        		}
        	}
        	tableHtml += '</tr></thead>';
        }
        if (datas && datas.length>0) {
        	tableHtml += '<tbody>';
        	var limit = ele.data("limit")||datas.length; //限定长度
        	for (var i=0;i<limit;i++) {
        		var data = datas[i];
        		
        		tableHtml += '<tr ';
        		if (data.id) {
        			tableHtml += ' id="td_'+data.id+'" data-id="'+data.id+'"'; 
                }
        		tableHtml += '>';
//	        		tableHtml += ' <td>'+(data.count||i)+'</td>'
//		        	+' <td>'+data.name+'</td>'
//		        	+' <td>'+data.value+'</td>';
        		var dataIndex=0;
        		var keys = Object.keys(data);
        		for (var j = 0; j < titles.length; j++) {
        			var showContent = data[keys[j]]||"";
        			var showTooltip = true;
        			if (tooltipShow && tooltipShow.indexOf(j+"")==-1) {
        				showTooltip = false;
        			}
        			if (showTooltip) {
        				tableHtml += ' <td>';
        				var ahref = null;
        				if (links && linkIndexs.length > 0) {
	                        for (var ii = 0; ii < linkIndexs.length; ii++) {
	                        	var linkIndex = linkIndexs[ii];
	                        	if (links == j && data[linkNames[ii]]) {
	                        		ahref = data[linkNames[ii]];
	                        		break;
                                }
                            }
                        }
        				if (ahref) {
        					tableHtml += ' <a target="blank" href="'+ahref+'">'+showContent+'</a>';
        				} else {
        					tableHtml += showContent;
						}
        				tableHtml += '</td>';
        			}
//        			<a target="blank" href="http://www.gxitps.org/zhanhui/detail/id/20.html">
                }
        			
        		
	        	tableHtml += '</tr>';
        	}
        	tableHtml += '</tbody>';
        }
        tableHtml += '</table>';
        ele.html(tableHtml);
        var callback = ele.data("callback"); //回调函数
        if (callback) {
        	callback = eval(callback); //字符串转换为方法
        	callback(ele, result);
        }
    
	});
}
        
