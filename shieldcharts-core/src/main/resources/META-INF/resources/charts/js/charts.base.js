// 当前js的目录
chartsjsPath = getJSDir();

ShieldCharts = {
	_set:{
        /** 调试模式，部署时关闭 */
        debug : false 
    },
	debug : function(msg, trace){
        if (this._set.debug) {
            if (typeof(console) != "undefined"){
                console.log(new Date());
                console.log(msg);
            } 
        }
    },
	extendFuns : {},
	addExtendFun : function (HTMLEleName, fn, description) {
		if(this.extendFuns[HTMLEleName]){
			this.debug(HTMLEleName + "动作处理方法被覆盖！");
		}
		// 复制对象
		var copiedObject = $.extend({}, this.extendFuns[HTMLEleName]);
		var fnV = fn;
		var descriptionV = description;
		this.extendFuns[HTMLEleName] = {
			fn:fnV //处理函数
			,description : descriptionV || copiedObject.description //方法描述
		};
	},
	theme:null,
	getJSON : function(url, params, callback) {
		if (jQuery.isFunction(params)) { //没有params直接写callback时
			callback = params;
			params = {};
        }
        if (!params) {
            params = {};
        }
        if (typeof(params) === "string") { //字符串
        	params += "&r="+new Date().getTime(); //增加时间戳
        } else { //json
        	params.r = new Date().getTime(); //增加时间戳
        }
        $.getJSON(url, params, callback);
	}
}
var extendOptions = function(serie, serieExtend) {
	serie = $.extend(true, serie, serieExtend);
	return serie;
}
var showCharts = function(ele, myChart, options, result) {
	setAxisColor(ele, options, myChart); //设置axis颜色
	setLegendStyle(ele, options, myChart); //设置legend图例样式
	setGridStyle(ele, options, myChart); //设置grid位置信息
	setFormatterStyle(ele, options, myChart); //设置格式化显示
	loadCommonStyle(ele, options, myChart); //加载通用样式
	options = getExtendSettings(ele, options); //data-xx可能会覆盖取到的值
	myChart.setOption(options, true);
	myChart.resize();
	var callback = ele.data("callback"); //回调函数
	if (callback) {
    	callback = eval(callback); //字符串转换为方法
    	callback(ele, result, myChart, options);
    }
	ShieldCharts.debug(options)
	window.addEventListener("resize",function(){
		myChart.resize();
	});
}
function fnW(str) {
    var num;
    str >= 10 ? num = str : num = "0" + str;
    return num;
}
//获取当前时间
var timer = function() {
	setInterval(function () {
	    var date = new Date();
	    var year = date.getFullYear(); //当前年份
	    var month = date.getMonth(); //当前月份
	    var data = date.getDate(); //天
	    var hours = date.getHours(); //小时
	    var minute = date.getMinutes(); //分
	    var second = date.getSeconds(); //秒
	    var day = date.getDay(); //获取当前星期几 
	    var ampm = hours < 12 ? 'am' : 'pm';
	    $('#time').html(fnW(hours) + ":" + fnW(minute) + ":" + fnW(second));
	    $('#date').html('<span>' + year + '/' + (month + 1) + '/' + data + '</span><span>' + ampm + '</span><span>周' + day + '</span>')
	}, 1000);
}
var setAxisColor = function(ele, options, myChart) { //设置axis轴显示颜色
	var axisColorStr = ele.data("axisColor");
	if (axisColorStr) {
		var axisColor;
		var lineColor;
		var lineWidth;
		var lineType; //加粗等
		var tickXColor; //x轴线上的刻度
		var tickXWidth; //x轴线上的刻度
		var tickXType; //x轴线上的刻度
		var tickYColor; //y轴线上的刻度
		var tickYWidth; //y轴线上的刻度
		var tickYType; //y轴线上的刻度
		if (axisColorStr.indexOf("line:")>-1) {
			var matchRegex = /line:(.+?)( |,|$)/; //,或者结束
			var matchResult = axisColorStr.match(matchRegex);
			lineColor = matchResult?matchResult[1]:null;
			if(lineColor.indexOf(";")>-1){
				var lineColorArrs = lineColor.split(";"); //line:#fff;2;solid
				lineColor = lineColorArrs[0];
				if (lineColorArrs.length>1) {
					lineWidth = lineColorArrs[1]; //宽度
                }
				if (lineColorArrs.length>2) {
					lineType = lineColorArrs[2]; //type
                }
			}
			axisColor = axisColorStr.substring(0, axisColorStr.indexOf("line:"+lineColor));
			if (axisColor.endsWith(",")) {
				axisColor = axisColor.slice(0, -1); //从后往前截取一位
            }
		} else {
			axisColor = axisColorStr;
		}
		//x轴线上的刻度
		if (axisColorStr.indexOf("tickx:")>-1) {
			var matchRegex = /tickx:(.+?)( |,|$)/; //,或者结束
			var matchResult = axisColorStr.match(matchRegex);
			tickXColor = matchResult?matchResult[1]:null;
			if(tickXColor.indexOf(";")>-1){
				var lineColorArrs = lineColor.split(";"); //line:#fff;2;solid
				tickXColor = lineColorArrs[0];
				if (lineColorArrs.length>1) {
					tickXWidth = lineColorArrs[1]; //宽度
                }
				if (lineColorArrs.length>2) {
					tickXType = lineColorArrs[2]; //type
                }
			}
		}
		//y轴线上的刻度
		if (axisColorStr.indexOf("ticky:")>-1) {
			var matchRegex = /ticky:(.+?)( |,|$)/; //,或者结束
			var matchResult = axisColorStr.match(matchRegex);
			tickYColor = matchResult?matchResult[1]:null;
			if(tickYColor.indexOf(";")>-1){
				var lineColorArrs = lineColor.split(";"); //line:#fff;2;solid
				tickYColor = lineColorArrs[0];
				if (lineColorArrs.length>1) {
					tickYWidth = lineColorArrs[1]; //宽度
                }
				if (lineColorArrs.length>2) {
					tickYType = lineColorArrs[2]; //type
                }
			}
		}
		
		var serieExtend = {
				//color: ['#FADB71'],
				xAxis:{axisLabel: {color: axisColor,textStyle:{color:axisColor}}},
				yAxis:{axisLabel: {color: axisColor,textStyle:{color:axisColor}}}
		}
		if (lineColor) {
			var serieExtend2 = {
					//color: ['#FADB71'],
					xAxis:{axisLine:{lineStyle:{color : lineColor}}},
					yAxis:{axisLine:{lineStyle:{color : lineColor}}}
			}
			serieExtend = $.extend(true, serieExtend, serieExtend2);
		}
		if (lineWidth) {
			var serieExtend2 = {
					//color: ['#FADB71'],
					xAxis:{axisLine:{lineStyle:{width : lineWidth}}},
					yAxis:{axisLine:{lineStyle:{width : lineWidth}}}
			}
			serieExtend = $.extend(true, serieExtend, serieExtend2);
		}
		if (lineType) {
			var serieExtend2 = {
					//color: ['#FADB71'],
					xAxis:{axisLine:{lineStyle:{type : lineType}}},
					yAxis:{axisLine:{lineStyle:{type : lineType}}}
			}
			serieExtend = $.extend(true, serieExtend, serieExtend2);
		}
		//x轴线上的刻度
		if (tickXColor) {
			var serieExtend2 = {
					xAxis:{axisTick:{lineStyle:{color : tickXColor}}}
			}
			serieExtend = $.extend(true, serieExtend, serieExtend2);
		}
		if (tickXWidth) {
			var serieExtend2 = {
					xAxis:{axisTick:{lineStyle:{width : tickXWidth}}}
			}
			serieExtend = $.extend(true, serieExtend, serieExtend2);
		}
		if (tickXType) {
			var serieExtend2 = {
					xAxis:{axisLine:{lineStyle:{type : tickXType}}}
			}
			serieExtend = $.extend(true, serieExtend, serieExtend2);
		}
		//y轴线上的刻度
		if (tickYColor) {
			var serieExtend2 = {
					yAxis:{axisTick:{lineStyle:{color : tickYColor}}}
			}
			serieExtend = $.extend(true, serieExtend, serieExtend2);
		}
		if (tickYWidth) {
			var serieExtend2 = {
					yAxis:{axisTick:{lineStyle:{width : tickYWidth}}}
			}
			serieExtend = $.extend(true, serieExtend, serieExtend2);
		}
		if (tickYType) {
			var serieExtend2 = {
					yAxis:{axisTick:{lineStyle:{type : tickYType}}}
			}
			serieExtend = $.extend(true, serieExtend, serieExtend2);
		}
		options = $.extend(true, options, serieExtend);
    }
}
var setLegendStyle = function(ele, options, myChart) { //设置legend位置
	var legendPosition = ele.data("legendStyle");
	
	if (legendPosition) {
		var x = null;
		var y = null;
		var orient = null; //朝向
		if (legendPosition.indexOf("l")==0 || legendPosition.indexOf("L")==0) {
			x = "left";
        } else if (legendPosition.indexOf("r")==0 || legendPosition.indexOf("R")==0) {
			x = "right";
        } else if (legendPosition.indexOf("t")==0 || legendPosition.indexOf("T")==0) {
			x = "top";
        } else if (legendPosition.indexOf("b")==0 || legendPosition.indexOf("B")==0) {
			x = "bottom";
        } else if (legendPosition.indexOf("c")==0 || legendPosition.indexOf("C")==0) {
			x = "center";
        } else if (legendPosition.indexOf("x:")>-1) {
        	var matchRegex = /x:(.+?)( |,|$)/; //,或者结束
        	var matchResult = legendPosition.match(matchRegex);
        	x = matchResult?matchResult[1]:null;
        }
		
		if (legendPosition.indexOf("l")==1 || legendPosition.indexOf("L")==1) {
			y = "left";
        } else if (legendPosition.indexOf("r")==1 || legendPosition.indexOf("R")==1) {
			y = "right";
        } else if (legendPosition.indexOf("t")==1 || legendPosition.indexOf("T")==1) {
			y = "top";
        } else if (legendPosition.indexOf("b")==1 || legendPosition.indexOf("B")==1) {
			y = "bottom";
        } else if (legendPosition.indexOf("c")==1 || legendPosition.indexOf("C")==1) {
			y = "center";
        }  else if (legendPosition.indexOf("y:")>-1) {
        	var matchRegex = /y:(.+?)( |,|$)/; //,或者结束
        	var matchResult = legendPosition.match(matchRegex);
        	y = matchResult?matchResult[1]:null;
        }
		var left;
		if (legendPosition.indexOf("left:")>-1) {
			var matchRegex = /left:(.+?)( |,|$)/; //,或者结束
			var matchResult = legendPosition.match(matchRegex);
			left = matchResult?matchResult[1]:null;
		}
		var right;
        if (legendPosition.indexOf("right:")>-1) {
        	var matchRegex = /right:(.+?)( |,|$)/; //,或者结束
        	var matchResult = legendPosition.match(matchRegex);
        	right = matchResult?matchResult[1]:null;
        }
        var top;
        if (legendPosition.indexOf("top:")>-1) {
        	var matchRegex = /top:(.+?)( |,|$)/; //,或者结束
        	var matchResult = legendPosition.match(matchRegex);
        	top = matchResult?matchResult[1]:null;
        }
        var bottom;
        if (legendPosition.indexOf("bottom:")>-1) {
        	var matchRegex = /bottom:(.+?)( |,|$)/; //,或者结束
        	var matchResult = legendPosition.match(matchRegex);
        	bottom = matchResult?matchResult[1]:null;
        }
		
		if (legendPosition.indexOf("vertical")>-1) {
			orient = "vertical";
        }
		var color = null;
		var colorRegex1 = /(#([0-9a-fA-F]{6}|[0-9a-fA-F]{3}))/; //匹配#000-#fff表示法的颜色
		var colorRegex2 = /([rR][gG][Bb][Aa]?[\(]([\s]*(2[0-4][0-9]|25[0-5]|[01]?[0-9][0-9]?),){2}[\s]*(2[0-4][0-9]|25[0-5]|[01]?[0-9][0-9]?),?[\s]*(0\.\d{1,2}|1|0)?[\)]{1})/g; //匹配rgba(122,122,122) rgba(122,122,122,0.89)
		if (colorRegex1.test(legendPosition)) {
			color = RegExp.$1;
		} else if(colorRegex2.test(legendPosition)) {
			color = RegExp.$1;
		} 
		var serieExtend = {legend:{}}
		if (x) {serieExtend.legend.x = x;}
		if (y) {serieExtend.legend.y = y;}
		if (left) {serieExtend.legend.left = left;}
		if (right) {serieExtend.legend.right = right;}
		if (top) {serieExtend.legend.top = top;}
		if (bottom) {serieExtend.legend.bottom = bottom;}
		if (orient) {serieExtend.legend.orient = orient;}
		if (color) {
			var textStyle = {
				legend:{ textStyle:{	color:color	} }
			}
			serieExtend = $.extend(true, serieExtend, textStyle);
        }
		var icon = null;
		var iconRegex1 = /(circle|rect|roundRect|triangle|diamond|emptyCircle|emptyRectangle|emptyTriangle|emptyDiamond|pin|arrow|none)/; //icon
		if (legendPosition.indexOf("icon:")>-1) { //自定义icon
        	var iconRegex = /icon:(\w+?)( |,|$)/; //需要以,结束
        	var matchResult = legendPosition.match(iconRegex);
        	icon = matchResult?matchResult[1]:null;
        } else if (iconRegex1.test(legendPosition)) {
			icon = RegExp.$1;
		} else if (legendPosition.indexOf("car")>-1) { //汽车
	        icon = "path://M874.666667 469.333333c17.28 42.666667 42.666667 85.333333 42.666666 128v213.333334c0 27.946667-23.893333 42.666667-64 42.666666s-64-17.28-64-42.666666v-42.666667H234.666667v42.666667c0 25.386667-23.893333 42.666667-64 42.666666s-64-14.72-64-42.666666V597.333333c0-42.666667 25.386667-85.333333 42.666666-128s-64-37.333333-64-64 5.333333-42.666667 42.666667-42.666666h64s30.293333-87.04 42.666667-128 64-64 106.666666-64h341.333334c42.666667 0 94.293333 23.04 106.666666 64s42.666667 128 42.666667 128h64c37.333333 0 42.666667 16 42.666667 42.666666s-81.28 21.333333-64 64z m-469.333334 149.333334a21.333333 21.333333 0 0 0 21.333334 21.333333h170.666666a21.333333 21.333333 0 0 0 21.333334-21.333333v-21.333334a21.333333 21.333333 0 0 0-21.333334-21.333333h-170.666666a21.333333 21.333333 0 0 0-21.333334 21.333333v21.333334zM192 618.666667a64 64 0 1 0 64-64 64 64 0 0 0-64 64z m597.333333-256c0-21.333333-42.666667-106.666667-42.666666-106.666667H277.333333s-42.666667 85.333333-42.666666 106.666667v21.333333a42.666667 42.666667 0 0 0 42.666666 42.666667h469.333334a42.666667 42.666667 0 0 0 42.666666-42.666667v-21.333333z m-21.333333 192a64 64 0 1 0 64 64 64 64 0 0 0-64-64z";
        } else if (legendPosition.indexOf("stop")>-1) {
	        icon = "path://M30.9,53.2C16.8,53.2,5.3,41.7,5.3,27.6S16.8,2,30.9,2C45,2,56.4,13.5,56.4,27.6S45,53.2,30.9,53.2z M30.9,3.5C17.6,3.5,6.8,14.4,6.8,27.6c0,13.3,10.8,24.1,24.101,24.1C44.2,51.7,55,40.9,55,27.6C54.9,14.4,44.1,3.5,30.9,3.5z M36.9,35.8c0,0.601-0.4,1-0.9,1h-1.3c-0.5,0-0.9-0.399-0.9-1V19.5c0-0.6,0.4-1,0.9-1H36c0.5,0,0.9,0.4,0.9,1V35.8z M27.8,35.8 c0,0.601-0.4,1-0.9,1h-1.3c-0.5,0-0.9-0.399-0.9-1V19.5c0-0.6,0.4-1,0.9-1H27c0.5,0,0.9,0.4,0.9,1L27.8,35.8L27.8,35.8z";
        } else if (legendPosition.indexOf("diewu")>-1){ 
        	icon = "path://M10,50 Q30,100 50,50 T80,70";
        } else if (legendPosition.indexOf("plane")>-1) { //飞机
        	icon = "path://M1705.06,1318.313v-89.254l-319.9-221.799l0.073-208.063c0.521-84.662-26.629-121.796-63.961-121.491c-37.332-0.305-64.482,36.829-63.961,121.491l0.073,208.063l-319.9,221.799v89.254l330.343-157.288l12.238,241.308l-134.449,92.931l0.531,42.034l175.125-42.917l175.125,42.917l0.531-42.034l-134.449-92.931l12.238-241.308L1705.06,1318.313z";
        }
		if (icon) {
			var textStyle = { legend:{	icon:icon } }
			serieExtend = $.extend(true, serieExtend, textStyle);
        }
		if (legendPosition.indexOf("withItemColor")>-1) { //与item显示相同的颜色，而不是固定的颜色
			var textStyle = { legend:{ textStyle:{color:[]} } }
			serieExtend = $.extend(true, serieExtend, textStyle);
        }
		if (legendPosition.indexOf("color:")>-1) {
        	var matchRegex = /color:(.+?)( |,|$)/; //,或者结束
        	var matchResult = legendPosition.match(matchRegex);
        	var legendTextColor = matchResult?matchResult[1]:null;
        	var textStyle = { legend:{ textStyle:{color:legendTextColor} } }
			serieExtend = $.extend(true, serieExtend, textStyle);
        }
		if (legendPosition.indexOf("itemwidth:")>-1) { //每个条目宽度
        	var matchRegex = /itemwidth:(.+?)( |,|$)/; //,或者结束
        	var matchResult = legendPosition.match(matchRegex);
        	var itemwidth = matchResult?matchResult[1]:null;
        	var textStyle = { legend:{ itemWidth:itemwidth } }
			serieExtend = $.extend(true, serieExtend, textStyle);
        }
		if (legendPosition.indexOf("itemheight:")>-1) { //每个条目高度
        	var matchRegex = /itemheight:(.+?)( |,|$)/; //,或者结束
        	var matchResult = legendPosition.match(matchRegex);
        	var itemheight = matchResult?matchResult[1]:null;
        	var textStyle = { legend:{ itemHeight:itemheight } }
			serieExtend = $.extend(true, serieExtend, textStyle);
        }
		if (legendPosition.indexOf("item:")>-1) { //每个条目高度,item的设置不生效，待查明原因
        	var matchRegex = /item:(.+?)( |,|$)/; //,或者结束
        	var matchResult = legendPosition.match(matchRegex);
        	var itemheight = matchResult?matchResult[1]:null;
        	alert(itemheight)
        	var textStyle = { legend:{ itemWidth:itemheight, itemHeight:itemheight } }
			serieExtend = $.extend(true, serieExtend, textStyle);
        }
		if (legendPosition.indexOf("hide")>-1) { //隐藏
        	var textStyle = { legend:{ show:false } }
			serieExtend = $.extend(true, serieExtend, textStyle);
        }
		options = $.extend(true, options, serieExtend);
    }
}
// 设置位置
var setGridStyle = function(ele, options, myChart) { //设置legend位置
	var gridStyle = ele.data("gridStyle");
	
	var left,right,top,bottom;
	if (gridStyle) { //top right bottom left
		if (gridStyle.indexOf(":") == -1) {
			var gridStyleArrs = gridStyle.split(",");
			if (gridStyleArrs.length >= 4) {  //按top right bottom left的顺序
				top = gridStyleArrs[0];
				right = gridStyleArrs[1];
				bottom = gridStyleArrs[2];
				left = gridStyleArrs[3];
			} else if (gridStyleArrs.length >= 3) {  //上、左右、下
				top = gridStyleArrs[0];
				right = gridStyleArrs[1];
				left = gridStyleArrs[1];
				bottom = gridStyleArrs[2];
			} else if (gridStyleArrs.length >= 2) {  //上下、左右
				top = gridStyleArrs[0];
				bottom = gridStyleArrs[0];
				right = gridStyleArrs[1];
				left = gridStyleArrs[1];
			} else if (gridStyleArrs.length >= 1) {  //全都相等
				top = gridStyleArrs[0];
				right = gridStyleArrs[0];
				bottom = gridStyleArrs[0];
				left = gridStyleArrs[0];
			}
        }
		if (gridStyle.indexOf("left:")>-1) {
			var matchRegex = /left:(.+?)( |,|$)/; //,或者结束
			var matchResult = gridStyle.match(matchRegex);
			left = matchResult?matchResult[1]:null;
		}
        if (gridStyle.indexOf("right:")>-1) {
        	var matchRegex = /right:(.+?)( |,|$)/; //,或者结束
        	var matchResult = gridStyle.match(matchRegex);
        	right = matchResult?matchResult[1]:null;
        }
        if (gridStyle.indexOf("top:")>-1) {
        	var matchRegex = /top:(.+?)( |,|$)/; //,或者结束
        	var matchResult = gridStyle.match(matchRegex);
        	top = matchResult?matchResult[1]:null;
        }
        if (gridStyle.indexOf("bottom:")>-1) {
        	var matchRegex = /bottom:(.+?)( |,|$)/; //,或者结束
        	var matchResult = gridStyle.match(matchRegex);
        	bottom = matchResult?matchResult[1]:null;
        }
        var serieExtend = {grid:{}}
        if (left) {serieExtend.grid.left = left;}
        if (right) {serieExtend.grid.right = right;}
        if (top) {serieExtend.grid.top = top;}
        if (bottom) {serieExtend.grid.bottom = bottom;}
        //console.log(serieExtend)
        options = $.extend(true, options, serieExtend);
	}
}
//设置格式化显示
var setFormatterStyle = function(ele, options, myChart) { //设置legend位置
	var gridStyle = ele.data("showtext");
	
	if (gridStyle) { //top right bottom left
		var tooltip,label;
		if (gridStyle.indexOf("tooltip:")>-1) {
			var matchRegex = /tooltip:(.+?)(,|$)/; //,或者结束
			var matchResult = gridStyle.match(matchRegex);
			tooltip = matchResult?matchResult[1]:null;
		}
        if (gridStyle.indexOf("label:")>-1) {
        	var matchRegex = /label:(.+?)(,|$)/; //,或者结束
        	var matchResult = gridStyle.match(matchRegex);
        	label = matchResult?matchResult[1]:null;
        }
        var yprefix,ysuffix,xprefix,xsuffix; //y轴前轴后缀，x轴前缀后缀
        if (gridStyle.indexOf("yprefix:")>-1) {
        	var matchRegex = /yprefix:(.+?)(,|$)/; //,或者结束
        	var matchResult = gridStyle.match(matchRegex);
        	yprefix = matchResult?matchResult[1]:null;
        }
        if (gridStyle.indexOf("ysuffix:")>-1) {
        	var matchRegex = /ysuffix:(.+?)(,|$)/; //,或者结束
        	var matchResult = gridStyle.match(matchRegex);
        	ysuffix = matchResult?matchResult[1]:null;
        }
        if (gridStyle.indexOf("xprefix:")>-1) {
        	var matchRegex = /xprefix:(.+?)(,|$)/; //,或者结束
        	var matchResult = gridStyle.match(matchRegex);
        	xprefix = matchResult?matchResult[1]:null;
        }
        if (gridStyle.indexOf("xsuffix:")>-1) {
        	var matchRegex = /xsuffix:(.+?)(,|$)/; //,或者结束
        	var matchResult = gridStyle.match(matchRegex);
        	xsuffix = matchResult?matchResult[1]:null;
        }
        
        var serieExtend = {}
        if (tooltip) {
        	tooltip = tooltip.replace("\\n", "\n")
        	var serieExtend2 = {
    			tooltip: {
    				formatter : tooltip
		        }
        	}
        	serieExtend = $.extend(true, serieExtend, serieExtend2);
        }
        if (label) {
        	label = label.replace("\\n", "\n")
        	var seriesLength = options.series.length;
        	var serieExtend2 = {
        		series: []
        	}
        	var seriesLabel = {
        		label: {
        			normal: {
        				show: true,
        				formatter: label
        			}
        		}
            };
        	for (var i = 0; i < seriesLength; i++) {
        		serieExtend2.series.push(seriesLabel);
            }
        	serieExtend = $.extend(true, serieExtend, serieExtend2);
        }
        if (yprefix) { //y轴前缀
        	yprefix = yprefix.replace("\\n", "\n")
        	var serieExtend2 = {
    			yAxis: { axisLabel: { formatter: function (value) { return yprefix+value; } } }
        	}
        	var isArray = $.isArray(options.yAxis); //说明有多组数据
        	if (isArray) {
        		serieExtend2 = {
        			yAxis: [{ axisLabel: { formatter: function (value) { return yprefix+value; } } }]
            	}
            } 
        	serieExtend = $.extend(true, serieExtend, serieExtend2);
        }
        if (ysuffix) { //y轴后缀
        	ysuffix = ysuffix.replace("\\n", "\n")
        	var serieExtend2 = {
    			yAxis: { axisLabel: { formatter: function (value) { return value+ysuffix; } } }
        	}
        	var isArray = $.isArray(options.yAxis); //说明有多组数据
        	if (isArray) {
        		serieExtend2 = {
        			yAxis: [{ axisLabel: { formatter: function (value) { return value+ysuffix; } } }]
            	}
            } 
        	serieExtend = $.extend(true, serieExtend, serieExtend2);
        }
        if (xprefix) { //x轴前缀
        	xprefix = xprefix.replace("\\n", "\n")
        	var serieExtend2 = {
    			xAxis: { axisLabel: { formatter: function (value) { return xprefix+value; } } }
        	}
        	var isArray = $.isArray(options.xAxis); //说明有多组数据
        	if (isArray) {
        		serieExtend2 = {
        			xAxis: [{ axisLabel: { formatter: function (value) { return xprefix+value; } } }]
            	}
            } 
        	serieExtend = $.extend(true, serieExtend, serieExtend2);
        }
        if (xsuffix) { //x轴后缀
        	xsuffix = xsuffix.replace("\\n", "\n")
        	var serieExtend2 = {
    			xAxis: { axisLabel: { formatter: function (value) { return value+xsuffix; } } }
        	}
        	var isArray = $.isArray(options.yAxis); //说明有多组数据
        	if (isArray) {
        		serieExtend2 = {
        			xAxis: [{ axisLabel: { formatter: function (value) { return value+xsuffix; } } }]
            	}
            } 
        	serieExtend = $.extend(true, serieExtend, serieExtend2);
        }
        options = $.extend(true, options, serieExtend);
	}
}
//加载通用样式
var loadCommonStyle = function(ele, options, myChart) { //设置legend位置
	var commonStyle = ele.data("style");
	
	if (commonStyle) { //top right bottom left
		var nosplitx,nosplity;
		if (commonStyle.indexOf("nosplitx") > -1) {  //不显示x轴
			nosplitx=true;
        } 
		if (commonStyle.indexOf("nosplity") > -1) {  //不显示y轴
			nosplity=true;
        } 
		
        var serieExtend = {}
        if (nosplitx) {
        	var serieExtend2 = { xAxis: { splitLine: { show: false } } }
        	serieExtend = $.extend(true, serieExtend, serieExtend2);
        }
        if (nosplity) {
        	var serieExtend2 = { yAxis: { splitLine: { show: false } } }
        	serieExtend = $.extend(true, serieExtend, serieExtend2);
        }
        var showlabel,labelPosition;
        if (commonStyle.indexOf(" label:") > -1 || commonStyle.indexOf(" label ")>-1) {  //label显示位置,只写label表示显示带冒号则设置显示位置
        	showlabel=true;
        	if (commonStyle.indexOf(" label:")>-1) {
            	var matchRegex = / label:(.+?)(,|$)/; //,或者结束
            	var matchResult = commonStyle.match(matchRegex);
            	labelPosition = matchResult?matchResult[1]:null;
            }

        	var seriesLength = options.series.length;
        	var serieExtend2 = { series: []}
        	var seriesLabel = { itemStyle: { normal: { label: { show : true } } } };
        	if (labelPosition) {
        		seriesLabel = { itemStyle: { normal: { label: { show : true, position: labelPosition  } } } };
            }
        	for (var i = 0; i < seriesLength; i++) {
        		serieExtend2.series.push(seriesLabel);
            }
        	serieExtend = $.extend(true, serieExtend, serieExtend2);
        } 
        commonStyle = commonStyle.toLowerCase(); //注意转为小写，则下面的忽略大小写，如果是大小写敏感的请在上面进行处理
        
        if (commonStyle.indexOf("avoidlabeloverlap") > -1) {  //防止label覆盖自动调整
        	var seriesLength = options.series.length;
        	var serieExtend2 = { series: []	}
        	var seriesLabel = { avoidLabelOverlap : true };
        	for (var i = 0; i < seriesLength; i++) {
        		serieExtend2.series.push(seriesLabel);
            }
        	serieExtend = $.extend(true, serieExtend, serieExtend2);
        }
        if (commonStyle.indexOf("notitle") > -1) {  //不显示title
        	var serieExtend2 = { title: { show:false} }
        	serieExtend = $.extend(true, serieExtend, serieExtend2);
        }
        if (commonStyle.indexOf("notooltip") > -1) {  //不显示tooltip
        	var serieExtend2 = { tooltip: { show:false } }
        	serieExtend = $.extend(true, serieExtend, serieExtend2);
        }
        if (commonStyle.indexOf("noaxistickx") > -1) {  //不显示x轴分隔线
        	var serieExtend2 = { xAxis: { axisTick:{ show:false } } }
        	serieExtend = $.extend(true, serieExtend, serieExtend2);
        }
        if (commonStyle.indexOf("noaxisticky") > -1) {  //不显示y轴分隔线
        	var serieExtend2 = { yAxis: { axisTick:{ show:false } } }
        	serieExtend = $.extend(true, serieExtend, serieExtend2);
        }
        if (commonStyle.indexOf("nolinex") > -1) {  //不显示x轴线
        	var serieExtend2 = { xAxis: { axisLine:{ show:false } } }
        	serieExtend = $.extend(true, serieExtend, serieExtend2);
        }
        if (commonStyle.indexOf("noliney") > -1) {  //不显示y轴线
        	var serieExtend2 = { yAxis: { axisLine:{ show:false } } }
        	serieExtend = $.extend(true, serieExtend, serieExtend2);
        }
        if (commonStyle.indexOf("nolabelx") > -1) {  //不显示x轴线
        	var serieExtend2 = { xAxis: { axisLabel:{ show:false } } }
        	serieExtend = $.extend(true, serieExtend, serieExtend2);
        }
        if (commonStyle.indexOf("nolabely") > -1) {  //不显示y轴线
        	var serieExtend2 = { yAxis: { axisLabel:{ show:false } } }
        	serieExtend = $.extend(true, serieExtend, serieExtend2);
        }
        if (commonStyle.indexOf("tooltip_pointer_none") > -1) {  //鼠标移动到图标上时不显示背景焦点
        	var serieExtend2 = { tooltip:{axisPointer: {type: 'none'}} }
        	serieExtend = $.extend(true, serieExtend, serieExtend2);
        }
        
        if (commonStyle.indexOf("extenddata") > -1) {  //扩展数据
        	var extendCount=1;
        	if (commonStyle.indexOf("extenddata:")>-1) {
            	var matchRegex = /extenddata:(.+?)(,| |$)/; //,或者结束
            	var matchResult = commonStyle.match(matchRegex);
            	extendCount = matchResult?matchResult[1]:null;
            }
        	var eleId = ele.attr("id");
        	var fnName = "extenddata_"+eleId;
        	fnName = eval(fnName); //字符串转换为方法
        	fnName(ele, options, myChart);
        }		
		
		commonStyle = commonStyle.toLowerCase();
		if (commonStyle.indexOf("topvalue")>-1 ) {
			var seriesLength = options.series.length;
			var serieExtend2 = { series: []	}
			var seriesOptions = {
				itemStyle: { //图形样式
					normal: {
						label: {
							show: true,
							position: 'top',
							formatter: '{c}'
						}
					}
				}
			}
			if (commonStyle.indexOf("stack")>-1 ) {
				seriesOptions.stack = "stack"; //多个时需要处理
			}	
			for (var i = 0; i < seriesLength; i++) {
				serieExtend2.series.push(seriesOptions);
			}
			extendOptions(serieExtend, serieExtend2);
		}
		
		
        options = $.extend(true, options, serieExtend);
	}
}