var lineDeal = function(ele, url) {
	ShieldCharts.getJSON(url, function(result){
    	var dom = ele[0];
        var myChart = echarts.init(dom, ele.data("theme")||ShieldCharts.theme);
        
        var datas = result.data.data; //数据
        var legends = result.data.axisName||[]; //标注
    	var toolTips = result.data.toolTip||legends;//数据过滤掉前面的20 ，最后一个数据添加年
    	var titles = result.data.title||[];//数据过滤掉前面的20 ，最后一个数据添加年
        var isArray = $.isArray(datas[0]); //说明有多组数据
        
    	var serieData = [];
		// 数据填充
    	if(isArray){ //
        	for (var i = 0; i < datas.length; i++) {
        		fillData(datas[i], serieData, titles[i]);
        	}
    	} else {
    		fillData(datas, serieData, titles[0]);
    	}
    	//名称过滤
    	var title = result.data.chartTitle||""; //result.title;
    	var colors = [ "#ffff43",'#c23531','#2f4554', '#61a0a8', '#d48265', '#91c7ae','#749f83',  '#ca8622', '#bda29a','#6e7074', '#546570', '#c4ccd3'];
    	var options = {
    		backgroundColor : 'transparent',
    		title : {
    			text : title,
    			textAlign : 'left',
    			textStyle : {
    				color : "#333",
    				fontSize : "16",
    				fontWeight : "bold"
    			}
    		},
    	    tooltip : { //提示框组件。
    	        trigger: 'axis' ,//触发类型
    /* 			axisPointer : {
    				type : 'shadow'
    			}, */
    	       	//formatter: '{b0}:\n {c0}',//{a}（系列名称），{b}（区域名称），{c}（合并数值）, {d}（无）
     	        formatter: function(params) {
  					if(params.value=="undefined"){} //如果没定义就什么不做
    	            else {
    	            	var dataIndex = params[0].dataIndex;
    	                var toolTipName = toolTips[dataIndex];
    	                var toolTiphtml = toolTipName; //dataIndex seriesIndex
    	                for (var i=0;i<params.length;i++) {
    	                	var seriesIndex = params[i].seriesIndex;
    	                	var title = titles[seriesIndex]||i;
	                		title ="<br>"+title;
        	                toolTiphtml += title+':'+params[i].value;
    	                }
    	       			return toolTiphtml;
    	            }
    	        } 
    	    }, 
    	    legend: {
    	        data: titles,
    	        textStyle:{color:'#61d2f7'}
    	    },
    		color : colors,
    		grid : {
    			left : '4%',
    			top : "5%",
    			bottom : "5%",
    			right : "4%",
    			containLabel : true
    		},
    		xAxis : {
    			type : 'category',
    			axisLine : {
    				show : true
    			},
    			axisLabel : {
    				// interval : 0, //此行不建议使用,会影响x轴文字的超过时自动隐藏
    				textStyle : {
    					color : '#61d2f7',
    					fontSize : 12
    				}
    			},
    			axisTick : {
    				show : true
    			},
    			data : legends,
    		},
    		yAxis : {
    			axisTick : { //y轴刻度线
    				show : true
    			},
    			splitLine : { //网格线
    				show : true,
    				lineStyle:{
                        color:'rgba(160,160,160,0.3)',
                    }
    			},
    			axisLabel : {
    				textStyle : {
    					color : '#61d2f7',
    					fontSize : 12
    				}
    			},
    			axisLine : { //y轴
    				show : true
    			},
    		},
    		series : serieData
    	};
    	if (ele.data("style")) {
			var style = ele.data("style");
			if (style.indexOf("nospace")>-1) { //x轴左右没有空白
				options.xAxis.boundaryGap = false;
	        }
        }
    	var orient = ele.data("vx"); //横图
		var commonStyle = ele.data("style");
		if (commonStyle && commonStyle.indexOf("vertical")>-1) {
			orient = "vertical";
			if (style.indexOf("nospace")>-1) { //y轴左右没有空白
				options.yAxis.boundaryGap = false;
			}
		}
		if (orient) {
			options.yAxis.type="category";
			options.yAxis.data=legends;
			options.xAxis.type="value";
			options.xAxis.data=null;
        }
    	// 给对应的进行变化赋值
     	// 使用刚指定的配置项和数据显示图表。
    	showCharts(ele, myChart, options, result);
	});
    
    // 填充数据
	var fillData = function(datas, serieData, title, options) {
		var serie = {
			name : title,
			type : 'line',
//			symbol : "circle",
//			symbolSize : 10,
			data : datas
		};
		if (ele.data("symbol")) {
			var symbolArrs = ele.data("symbol").split(",");
			if (symbolArrs[0]) {
				serie.symbol = symbolArrs[0];
            }
			if (symbolArrs[1]) {
				serie.symbolSize = symbolArrs[1];
            }
        }
		if (ele.data("area")||ele.data("area")=="") {
			serie.areaStyle = {};
        }
		if (ele.data("smooth")||ele.data("smooth")=="") {
			serie.smooth = true
        }
		if (ele.data("style")) {
			var style = ele.data("style");
			if (style.indexOf("area")>-1) { //面积模式
				 serie.areaStyle = {};
	        }
			if (style.indexOf("smooth")>-1) { //平滑模式
				serie.smooth = true
	        }
        }
		if (ele.data("areaColor")) {
			var areaColorStr = ele.data("areaColor");
			var opacityArrs = areaColorStr.split(":"); //后面跟透明度
			var opacity = null;
			if (opacityArrs.length == 2) {
				areaColorStr = opacityArrs[0];
				opacity = opacityArrs[1];
			}
			var areaColorArrs = areaColorStr.split(">");
			if (areaColorArrs.length >= 2) { //面积模式
				var colors = [];
				for (var i = 0; i < areaColorArrs.length; i++) {
					var areaColorVal = areaColorArrs[i];
					if (areaColorVal) {
						var areaColor = {
                            offset: 1/(areaColorArrs.length-1)*i,
                            color: areaColorArrs[i] //#25f3e6
                        };
						colors.push(areaColor);
                    }
                }
				 var serieExtend = {
                    normal: {type: 'default',
                    	// 线性渐变，前四个参数分别是 x0, y0, x2, y2, 范围从 0 - 1，相当于在图形包围盒中的百分比，如果 globalCoord 为 `true`，则该四个值是绝对的像素位置
                    	//数组, 用于配置颜色的渐变过程. 每一项为一个对象, 包含offset和color两个参数. offset的范围是0 ~ 1, 用于表示位置
                    	color: new echarts.graphic.LinearGradient(0, 0, 0, 0.8,colors, false)
                    }
				 };
				if (opacity) {
					serieExtend.normal.opacity = opacity;
				}
				serie.areaStyle = serieExtend;
	        }
        }
		serieData.push(serie);
	}
}