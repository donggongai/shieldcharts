// 扩展处理
// 全国地图，使用map_extend发现细节有出入，此处先不和并
ShieldCharts.addExtendFun("chinaMap", function(ele, url) {
	var dom = ele[0];
    var myChart = echarts.init(dom, ShieldCharts.theme);
	
    var nameColor = " rgb(55, 75, 113)"
    var name_fontFamily = '宋体'
    var name_fontSize = 35
    var mapName = 'china'
    var data = []
    var geoCoordMap = {};
    var toolTipData = [];
    var tooltipSuffix = ele.data("tooltipSuffix")||""; //显示的字段后缀data-tooltip-suffix
    var tooltipShow = ele.data("tooltipShow"); //显示的字段data-tooltip-show
    
    $.getJSON(url, function(result){
    	var datas = result.data.data; //数据
        var titles = result.data.title; //表格标题
        var data0 = []; //第一个数据集合
        var isArray = $.isArray(datas[0]); //说明有多组数据
        if(isArray){
        	data0 = datas[0];
        } else {
        	data0 = datas;
        }
        data = datas;
        
        
        /*获取地图数据*/
        myChart.showLoading();
        var mapFeatures = echarts.getMap(mapName).geoJson.features;
        myChart.hideLoading();
        mapFeatures.forEach(function(v) {
        	// 地区名称
        	var name = v.properties.name;
        	// 地区经纬度
        	console.log(name)
        	geoCoordMap[name] = v.properties.cp;
        });
        
       /* var maxData = data.sort(function(a, b) {
        	return b.value - a.value; //倒序
        })[0].value||600;
        var minData = data.sort(function(a, b) {
        	return a.value - b.value; // 正序
        })[0].value||1;
        
        var max = maxData, //最大数据，maxData*5,
        min = minData; // 最小数据
        var maxSize4Pin = 50,
        minSize4Pin = 20;
        
        var convertData = function(data) {
        	var res = [];
        	for (var i = 0; i < data.length; i++) {
        		var geoCoord = geoCoordMap[data[i].name];
        		if (geoCoord) {
        			res.push({
        				name: data[i].name,
        				value: geoCoord.concat(data[i].value),
        			});
        		}
        	}
        	return res;
        };
        
        var options = {
        		
    		tooltip: {
    			trigger: 'item',
    			formatter: function(params) {
					var toolTiphtml = '';
					for(var i = 0;i<data.length;i++){
						if(params.name==data[i].name){
							var thisData = data[i];
							toolTiphtml += thisData.name+':<br>';
							var keys = Object.keys(thisData);
			        		for (var j = 0; j < titles.length; j++) {
			        			var key = keys[j];
			        			var showTooltip = true;
			        			if (tooltipShow && tooltipShow.indexOf(key)==-1) {
			        				showTooltip = false;
                                }
			        			if (key!="name" && showTooltip) {
			        				toolTiphtml += titles[j]+':'+(thisData[key]||"")+tooltipSuffix+"<br>";
                                }
		                    }
						}
					}
					// console.log(convertData(data))
					return toolTiphtml;
    			}
        },
        visualMap: {
        	show: false,
        	min: 0,
        	max: maxData/2,
        	left: 'left',
        	top: 'bottom',
        	calculable: true,
        	seriesIndex: [1],
        	inRange: {
        		// color: ['#3B5077', '#031525'] // 蓝黑
        		// color: ['#ffc0cb', '#800080'] // 红紫
        		// color: ['#3C3B3F', '#605C3C'] // 黑绿
        		//  color: ['#0f0c29', '#302b63', '#24243e'] // 黑紫黑
        		// color: ['#23074d', '#cc5333'] // 紫红
        		//   color: ['#00467F', '#A5CC82'] // 蓝绿
        		// color: ['#1488CC', '#2B32B2'] // 浅蓝
        		// color: ['#00467F', '#A5CC82','#ffc0cb'] // 蓝绿红
        		// color: ['#00467F', '#A5CC82'] // 蓝绿
        		// color: ['#00467F', '#A5CC82'] // 蓝绿
        		// color: ['#00467F', '#A5CC82'] // 蓝绿
        		color: ['#22e5e8', '#0035f9','#22e5e8'] // 蓝绿
        
        	}
        },
        geo: {
        	show: true,
        	map: mapName,
        	label: {
        		normal: {
        			show: false
        		},
        		emphasis: {
        			show: false
        		}
        	},
        	roam: true,
        	itemStyle: {
        		normal: {
        			areaColor: '#031525',
        			borderColor: '#097bba'
        		},
        		emphasis: {
        			areaColor: '#2B91B7'
        		}
        	}
        },
        series: [{
        	name: '散点',
        	type: 'scatter',
        	coordinateSystem: 'geo',
        	symbolSize: function(val) { //0 1是坐标2是值
        		return val[2] / 100;
        	},
        	label: {
        		normal: {
        			formatter: '{b}',
        			position: 'right',
        			show: false
        		},
        		emphasis: {
        			show: false
        		}
        	},
        	itemStyle: {
        		normal: {
        			color: 'rgba(255,255,0,0.8)'
        		}
        	},
        	data: convertData(data)
        },
        {
        	type: 'map',
        	map: mapName,
        	geoIndex: 0,
        	aspectScale: 0.75, //长宽比
        	showLegendSymbol: false, // 存在legend时显示
        	label: {
        		normal: {
        			show: true
        		},
        		emphasis: {
        			show: false,
        			textStyle: {
        				color: '#fff'
        			}
        		}
        	},
        	roam: true,
        	itemStyle: {
        		normal: {
        			areaColor: '#031525',
        			borderColor: '#3B5077',
        		},
        		emphasis: {
        			areaColor: '#2B91B7'
        		}
        	},
        	animation: false,
        	data: data
        },
        {
        	name: '点', //干嘛的，不清楚，貌似去了也不影响
        	type: 'scatter',
        	coordinateSystem: 'geo',
        	symbol: 'pin', //气泡
        	symbolSize: function(val) {
        		var a = (maxSize4Pin - minSize4Pin) / (max - min);
        		var b = minSize4Pin - a * min;
        		b = maxSize4Pin - a * max;
        		return a * val[2] + b;
        	},
        	label: {
        		
        		normal: {
        			show: false,
        			formatter:function (params) { return params.data.value[2] },
        			textStyle: {
        				color: '#fff',
        				fontSize: 9,
        			}
        		}
        	},
        	itemStyle: {
        		
        		normal: {
        			color: 'rgba(255,255,0,0)', //标志颜色
        		}
        	},
        	zlevel: 6,
        	data: convertData(data),
        },
        {
        	name: 'Top 5', //value最大的前5个
        	type: 'effectScatter',
        	coordinateSystem: 'geo',
        	data: convertData(data.sort(function(a, b) {
        		return b.value - a.value;
        	}).slice(0, 5)),
        	symbolSize: function(val) {
        		return val[2] / 100; //15已经很大了
        	},
        	showEffectOn: 'render',
        	rippleEffect: {
        		brushType: 'stroke'
        	},
        	hoverAnimation: true,
        	label: {
        		normal: {
        			formatter: '{b}', //显示名称
        			position: 'right',
        			show: true
        		}
        	},
        	itemStyle: {
        		normal: {
        			color: 'rgba(255,255,0,0.8)',
        			shadowBlur: 10,
        			shadowColor: '#05C3F9'
        		}
        	},
        	zlevel: 1
        },
        
        ]
        };*/
        
        var options = mapExtendDeal(ele, mapName, data, geoCoordMap, result);
        
        showCharts(ele, myChart, options, result);
    });

},"中国地图");

ShieldCharts.addExtendFun("chinaMap2", function(ele, url) {
	var dom = ele[0];
    var myChart = echarts.init(dom, ShieldCharts.theme);
	
    var nameColor = " rgb(55, 75, 113)"
    var name_fontFamily = '宋体'
    var name_fontSize = 35
    var mapName = 'china'
    var data = []
    var geoCoordMap = {};
    var toolTipData = [];
    var tooltipSuffix = ele.data("tooltipSuffix")||""; //显示的字段后缀data-tooltip-suffix
    var tooltipShow = ele.data("tooltipShow"); //显示的字段data-tooltip-show
    
    $.getJSON(url, function(result){
    	var datas = result.data.data; //数据
        var titles = result.data.title; //表格标题
        var mapTitle = result.data.maptitle||ele.data("mapTitle");
        var data0 = []; //第一个数据集合
        var isArray = $.isArray(datas[0]); //说明有多组数据
        if(isArray){
        	data0 = datas[0];
        } else {
        	data0 = datas;
        }
        data = datas;
        
        var maxData = data.sort(function(a, b) {
    		return b.value - a.value;
    	})[0].value||600;
        maxData = maxData/2;
        
        
        /*获取地图数据*/
        myChart.showLoading();
        var mapFeatures = echarts.getMap(mapName).geoJson.features;
        myChart.hideLoading();
        
        var options = {
        		title: {
        			text: mapTitle,
        			textStyle:{color:'#fff'},
        			//subtext: '纯属虚构',
        			x:'center'
        		},
        		tooltip : {
        			trigger: 'item'
        		},
        		visualMap: {
        			show : false,
        			x: 'left',
        			y: 'bottom',
        			//layoutCenter:['30%','30%'],
        			/*splitList: [ 
        				{start: 500, end:1600},{start: 400, end: 500},
        				{start: 300, end: 400},{start: 200, end: 300},
        				{start: 100, end: 200},{start: 0, end: 100},
        			],
        			color: ['#ff0', '#ffff00', '#0E94EB','#6FBCF0', '#F0F06F', '#00CC00']*/
	            	min: 0,
	            	max: maxData,
	            	inRange: {
	            		// color: ['#3B5077', '#031525'] // 蓝黑
	            		// color: ['#ffc0cb', '#800080'] // 红紫
	            		// color: ['#3C3B3F', '#605C3C'] // 黑绿
	            		//  color: ['#0f0c29', '#302b63', '#24243e'] // 黑紫黑
	            		// color: ['#23074d', '#cc5333'] // 紫红
	            		//   color: ['#00467F', '#A5CC82'] // 蓝绿
	            		// color: ['#1488CC', '#2B32B2'] // 浅蓝
	            		// color: ['#00467F', '#A5CC82','#ffc0cb'] // 蓝绿红
	            		// color: ['#00467F', '#A5CC82'] // 蓝绿
	            		// color: ['#00467F', '#A5CC82'] // 蓝绿
	            		// color: ['#00467F', '#A5CC82'] // 蓝绿
	            		color: ['#ff0', '#ffff00', '#0E94EB'] // 蓝绿
	            
	            	}
        		},
        		series: [{
        			name: mapTitle,
        			type: 'map',
        			mapType: 'china', 
        			roam: true,
        			label: {
        				normal: {
        					show: false
        				},
        				emphasis: {
        					show: false
        				}
        			},
        			data:datas
        		}]
        };
        showCharts(ele, myChart, options, result);
    });

},"中国地图2");
// 扩展地图，非全国地图时适用
ShieldCharts.addExtendFun("map_extend", function(ele, url) {
	var dom = ele[0];
    var myChart = echarts.init(dom, ShieldCharts.theme);
    
    var geoCoordMap = {};
    
    $.getJSON(url, function(result){
    	var datas = result.data.data; //数据
        var titles = result.data.title; //表格标题
        var legends = result.data.axisName||[]; //标注
        var mapTitle = result.data.maptitle||"";
        var mapAreaName = result.data.name||"shandong"; //地图区域
    	var showName = result.data.showName||"未定义"; //重点展示区域
    	var adcode = result.data.adcode||"100000"; //区域编码用来获取数据
    	var tooltipSuffix = ele.data("tooltipSuffix")||""; //显示的字段后缀data-tooltip-suffix
        var tooltipShow = ele.data("tooltipShow"); //显示的字段data-tooltip-show
        
        $.getJSON(chartsjsPath +'/map/geodata/'+adcode+'.geoJson', function(areaResult) {
    		var mapJson = {"type": "FeatureCollection"};
            mapJson.features = areaResult.features;
    		echarts.registerMap(mapAreaName, mapJson ); //注册地区
    		
    		/*var geoCoordMap = {
	            '河池市': [108.085179,24.700488],
	            '柳州市': [109.412578,24.354875],
	            '梧州市': [111.323462,23.478238],
	            '南宁市': [108.359656,22.81328],
	            '北海市': [109.171374,21.477419],
	            '崇左市': [107.347374,22.377503]
	        };*/

	        /*获取地图数据*/
	        myChart.showLoading();
	        var mapFeatures = echarts.getMap(mapAreaName).geoJson.features;
	        myChart.hideLoading();
	        mapFeatures.forEach(function(v) {
	        	// 地区名称
	        	var name = v.properties.name;
	        	// 地区经纬度
	        	geoCoordMap[name] = v.properties.center;
	        	//console.log(name+"="+geoCoordMap[name][0]+"="+geoCoordMap[name][1])
	        });
	        
	        var covertName = function(name) {
	            if (adcode=="100000") {
	                if (name == "香港") {
	                	name = "香港特别行政区";
                    } else if (name == "澳门") {
	                	name = "澳门特别行政区";
                    } else if (name == "新疆") {
	                	name = "新疆维吾尔自治区";
                    } else if (name == "宁夏") {
	                	name = "宁夏回族自治区";
                    } else if (name == "西藏") {
	                	name = "西藏自治区";
                    } else if (name == "广西") {
	                	name = "广西壮族自治区";
                    } else if (name == "内蒙古") {
	                	name = "内蒙古自治区";
                    } else if (name == "北京" || name == "天津" || name == "上海" || name == "重庆") {
	                	name += "市";
                    } else if (!name.endsWith("省")){
                    	name += "省";
                    }
                }
	            return name;
            }
	            
	        var data = datas;
	        for (var i = 0; i < data.length; i++) {
	        	var name = data[i].name;
	        	data[i].name = covertName(name);
            }
	        
	        var options = mapExtendDeal(ele, mapAreaName, data, geoCoordMap, result);
	        
	       showCharts(ele, myChart, options, result);
        });
        
    });

},"地图扩展插件");
// 地图数据处理
var mapExtendDeal = function(ele, mapAreaName, data, geoCoordMap, result, options) {
    var titles = result.data.title; //表格标题
    var legends = result.data.axisName||[]; //标注
    var mapTitle = result.data.maptitle||"";
	var showName = result.data.showName||"未定义"; //重点展示区域
	var adcode = result.data.adcode||"100000"; //区域编码用来获取数据
	var tooltipSuffix = ele.data("tooltipSuffix")||""; //显示的字段后缀data-tooltip-suffix
    var tooltipShow = ele.data("tooltipShow"); //显示的字段data-tooltip-show
    
	
	var maxData = data.sort(function(a, b) {
    	return b.value - a.value; //倒序
    })[0].value||600;
    var minData = data.sort(function(a, b) {
    	return a.value - b.value; // 正序
    })[0].value||1;
    
    var max = maxData, //最大数据，maxData*5,
        min = minData; // 最小数据
    var maxSize4Pin = 20,
        minSize4Pin = 5;
    var convertData = function (data) {
        var res = [];
        for (var i = 0; i < data.length; i++) {
            var geoCoord = geoCoordMap[data[i].name];
            if (geoCoord) {
                res.push({
                    name: data[i].name, //可能不带省市
                    value: geoCoord.concat(data[i].value)
                });
            }
        }
        return res;
    };
    

   var options = {
	   tooltip: {
           trigger: 'item',
           formatter: function(params) {
					var toolTiphtml = '';
					for(var i = 0;i<data.length;i++){
						if(params.name==data[i].name){
							var thisData = data[i];
							toolTiphtml += thisData.name+':<br>';
							var keys = Object.keys(thisData);
			        		for (var j = 0; j < titles.length; j++) {
			        			var key = keys[j];
			        			var showTooltip = true;
			        			if (tooltipShow && tooltipShow.indexOf(key)==-1) {
			        				showTooltip = false;
                               }
			        			if (key!="name" && showTooltip) {
			        				toolTiphtml += titles[j]+':'+(thisData[key]||"")+tooltipSuffix+"<br>";
                               }
		                    }
						}
					}
					// console.log(convertData(data))
					return toolTiphtml;
           }
       },
       title: {
            text: mapTitle,
            subtext: '',
            x: 'center',
            textStyle: {
                color: '#FFF'
            }
       },
       geo: {
            show: true,
            map: mapAreaName,
            mapType: mapAreaName,
            label: {
                normal: {},
                //鼠标移入后查看效果
                emphasis: {
                    textStyle: {
                        color: '#fff'
                    }
                }
            },
            roam: true,  //鼠标缩放和平移
            itemStyle: {
                normal: {
                    //          	color: '#ddd',
                    borderColor: 'rgba(147, 235, 248, 1)',
//                    borderWidth: 1,
                    areaColor: {
                        type: 'radial',
                        x: 0.5,
                        y: 0.5,
                        r: 0.8,
                        colorStops: [{
                            offset: 0,
                            color: 'rgba(175,238,238, 0)' // 0% 处的颜色
                        }, {
                            offset: 1,
                            color: 'rgba(47,79,79, .1)' // 100% 处的颜色
                        }],
                        globalCoord: false // 缺省为 false
                    },
                   // shadowColor: 'rgba(128, 217, 248, 1)',
                    //shadowOffsetX: -2,
                    //shadowOffsetY: 2,
                    //shadowBlur: 10
                },
                emphasis: {
                    areaColor: '#389BB7',
                    borderWidth: 0
                }
            }
       },
       series: [
    	   /*{
                name: 'light',
                type: 'map',
                coordinateSystem: 'geo',
                data: convertData(data),
                itemStyle: {
                    normal: {
                        color: '#F4E925'
                    }
                }
            },
            {
                name: '点',
                type: 'scatter',
                coordinateSystem: 'geo',
                symbol: 'pin', // 气球，默认圆
                symbolSize: function (val) {
                    var a = (maxSize4Pin - minSize4Pin) / (max - min);
                    var b = minSize4Pin - a * min;
                    b = maxSize4Pin - a * max;
                    return a * val[2] + b;
                },
                label: {
                    normal: {
                        // show: true,
                        // textStyle: {
                        //     color: '#fff',
                        //     fontSize: 9,
                        // }
                    }
                },
                itemStyle: {
                    normal: {
                        color: '#F62157', //标志颜色
                    }
                },
                zlevel: 6,
                data: convertData(data),
            },*/
            {
                name: 'light', // 区域悬停显示
                type: 'map',
//                mapType: mapAreaName,
                geoIndex: 0,
                selectedMode: 'multiple',
                aspectScale: 0.75, //长宽比
//                zoom: 2.2,   //这里是关键，一定要放在 series中
                showLegendSymbol: false, // 存在legend时显示
                label: {
                    normal: {
//                        show: false
                    },
                    emphasis: {
//                        show: false,
                        textStyle: {
                            color: '#fff'
                        }
                    }
                },
                roam: true, //鼠标缩放和平移
                /*itemStyle: {
                    normal: {
                        areaColor: '#031525',
                        borderColor: '#FFFFFF',
                    },
                    emphasis: {
                        areaColor: '#2B91B7'
                    }
                },*/
//                animation: false,
                data: data
            }
            /*,{
                name: 'Top 5', //前5
                type: 'effectScatter',
                coordinateSystem: 'geo',
                data: convertData(data.sort(function (a, b) {
                    return b.value - a.value;
                }).slice(0, 6)),
                symbolSize: function (val) {
                    return val[2] / (maxData/10);
                },
                showEffectOn: 'render',
                rippleEffect: {
                    brushType: 'stroke'
                },
                hoverAnimation: true,
                label: {
                    normal: {
                        formatter: '{b}',
                        position: 'right',
                        show: true
                    }
                },
                itemStyle: {
                    normal: {
                        color: '#fff',
                        shadowBlur: 10,
                        shadowColor: '#fff'
                    }
                },
                zlevel: 1
            }*/

            ]
   };
   var geoStyle = ele.data("geoStyle");
   if (geoStyle ) {
	   geoStyle += "";
	   var borderWidthNormal,borderWidthEmphasis;
	   if (geoStyle.indexOf("borderWidth:")>-1) {
			var matchRegex = /borderWidth:(.+?)( |,|$)/; //,或者结束
			var matchResult = geoStyle.match(matchRegex);
			var borderWidth = matchResult?matchResult[1]:null;
			if (borderWidth.indexOf(";")==-1) {
				borderWidthNormal = borderWidth;
            } else {
            	var borderWidthArrs = borderWidth.split(";");
            	borderWidthNormal = borderWidthArrs[0];
            	borderWidthEmphasis = borderWidthArrs[1];
            }
	   } 
	   var areaColorNormal,areaColorEmphasis;
	   if (geoStyle.indexOf("area:")>-1) {
			var matchRegex = /area:(.+?)( |$)/; //空格或者结束
			var matchResult = geoStyle.match(matchRegex);
			var borderWidth = matchResult?matchResult[1]:null;
			if (borderWidth.indexOf(";")==-1) {
				areaColorNormal = borderWidth;
           } else {
           	var borderWidthArrs = borderWidth.split(";");
           	areaColorNormal = borderWidthArrs[0];
           	areaColorEmphasis = borderWidthArrs[1];
           }
	   }
	   var borderColorNormal,borderColorEmphasis;
	   if (geoStyle.indexOf("border:")>-1) {
			var matchRegex = /border:(.+?)( |$)/; //空格或者结束
			var matchResult = geoStyle.match(matchRegex);
			var borderWidth = matchResult?matchResult[1]:null;
			if (borderWidth.indexOf(";")==-1) {
				borderColorNormal = borderWidth;
           } else {
           		var borderWidthArrs = borderWidth.split(";");
           		borderColorNormal = borderWidthArrs[0];
           		borderColorEmphasis = borderWidthArrs[1];
           }
	   }
	   if (borderWidthNormal) {
		   var geoStyle = { geo : { itemStyle : { normal: {  borderWidth: borderWidthNormal } } } };
		   options = $.extend(true, options, geoStyle);
       }
	   if (areaColorEmphasis) {
		   var geoStyle = { geo : { itemStyle : { emphasis: {  borderWidth: borderWidthNormal } } } };
		   options = $.extend(true, options, geoStyle);
       }
	   if (areaColorNormal) {
		   var geoStyle = { geo : { itemStyle : { normal: {  areaColor: areaColorNormal } } } };
		   options = $.extend(true, options, geoStyle);
       }
	   if (areaColorEmphasis) {
		   var geoStyle = { geo : { itemStyle : { emphasis: {  areaColor: areaColorEmphasis } } } };
		   options = $.extend(true, options, geoStyle);
       }
	   if (borderColorNormal) {
		   var geoStyle = { geo : { itemStyle : { normal: {  borderColor: borderColorNormal } } } };
		   options = $.extend(true, options, geoStyle);
       }
	   if (borderColorEmphasis) {
		   var geoStyle = { geo : { itemStyle : { emphasis: {  borderColor: borderColorEmphasis } } } };
		   options = $.extend(true, options, geoStyle);
       }
	   options = $.extend(true, options, geoStyle);
   }
   var visualStyle = ele.data("visualStyle"); //默认不显示，所以允许值为空，为空即打开该配置
   if (visualStyle || visualStyle=="") {
	   if (!visualStyle) { //默认
		   visualStyle == "0";
	   }
	   visualStyle += "";
	   var seriesIndex = 0;
	   if (visualStyle.indexOf("index:")>-1) { 
				var matchRegex = /index:(.+?)( |,|$)/; //,或者结束
				var matchResult = visualStyle.match(matchRegex);
				seriesIndex = matchResult?matchResult[1]:null;
	   } else {
		   var matchRegex = /^(\d+?)( |,|$)/; //,或者结束
		   var matchResult = visualStyle.match(matchRegex);
		   seriesIndex = matchResult?Number(matchResult[1]):seriesIndex;
	   }
	   
	   var visualMapColor = null;
	   if (visualStyle.indexOf("color:")>-1) {
				var matchRegex = /color:(.+?)( |,|$)/; //,或者结束
				var matchResult = visualStyle.match(matchRegex);
				visualMapColor = matchResult?matchResult[1]:null;
				visualMapColor = visualMapColor.split(">");
	   }
	   var visualMapText = ['高', '低'];
	   if (visualStyle.indexOf("text:")>-1) {
				var matchRegex = /text:(.+?)( |,|$)/; //,或者结束
				var matchResult = visualStyle.match(matchRegex);
				visualMapText = matchResult?matchResult[1]:null;
				visualMapText = visualMapColor.split(">");
	   }
	   var visualMapOrient = "horizontal"; //朝向
	   if (visualStyle.indexOf("orient:")>-1) {
		   var matchRegex = /orient:(.+?)( |,|$)/; //,或者结束
		   var matchResult = visualStyle.match(matchRegex);
		   visualMapOrient = matchResult?matchResult[1]:null;
	   }
	   var visualMapShow = true; //朝向
	   if (visualStyle.indexOf("hide")>-1) {
		   visualMapShow = false;
	   }
	   var minValue = 0;
	   var maxValue = maxData;
	   if (visualStyle.indexOf("min:")>-1) {
			var matchRegex = /min:(.+?)( |,|$)/; //,或者结束
			var matchResult = visualStyle.match(matchRegex);
			minValue = matchResult?matchResult[1]:null;
			minValue = eval(minValue.replaceAll ("{max}",maxData));
	   }
	   if (visualStyle.indexOf("max:")>-1) {
			var matchRegex = /max:(.+?)( |,|$)/; //,或者结束
			var matchResult = visualStyle.match(matchRegex);
			maxValue = matchResult?matchResult[1]:null;
			maxValue = eval(maxValue.replaceAll ("{max}",maxData));
	   }
	   var left;
	   if (visualStyle.indexOf("left:")>-1) {
			var matchRegex = /left:(.+?)( |,|$)/; //,或者结束
			var matchResult = visualStyle.match(matchRegex);
			left = matchResult?matchResult[1]:null;
	   }
	   var right;
       if (visualStyle.indexOf("right:")>-1) {
       		var matchRegex = /right:(.+?)( |,|$)/; //,或者结束
       		var matchResult = visualStyle.match(matchRegex);
       		right = matchResult?matchResult[1]:null;
       }
       var top;
       if (visualStyle.indexOf("top:")>-1) {
       		var matchRegex = /top:(.+?)( |,|$)/; //,或者结束
       		var matchResult = visualStyle.match(matchRegex);
       		top = matchResult?matchResult[1]:null;
       }
       var bottom;
       if (visualStyle.indexOf("bottom:")>-1) {
       		var matchRegex = /bottom:(.+?)( |,|$)/; //,或者结束
       		var matchResult = visualStyle.match(matchRegex);
       		bottom = matchResult?matchResult[1]:null;
       }
	   var visualMapStyle = {
		   visualMap : {
			   show : visualMapShow,
			   min: minValue,
			   max: maxValue,
			   /*left: 'left',
            	top: 'bottom',*/
			   orient: visualMapOrient, //'horizontal',
			   text: visualMapText, //['高', '低'], // 文本，默认为数值文本
			   calculable: true, //标尺显示
			   seriesIndex: [seriesIndex],
			   itemWidth: 15,
			   itemHeight: 200,
			   right: 0,
			   bottom: 10,
			   inRange: {
				   //color: ['#75ddff', '#0e94eb']
			   },
			   textStyle: {
				   color: 'white'
			   }
		   }
	   };
	   if (left) {visualMapStyle.visualMap.left = left;}
	   if (right) {visualMapStyle.visualMap.right = right;}
	   if (top) {visualMapStyle.visualMap.top = top;}
	   if (bottom) {visualMapStyle.visualMap.bottom = bottom;}
	   if (visualMapColor) {
		   visualMapStyle.visualMap.inRange.color = visualMapColor;
	   }
	   options = $.extend(true, options, visualMapStyle);
   }
   
   // 需要控制是否显示点，是否重点显示前几名data-pointer="pin,#F62157[rgba(255,255,0,0.8)]"
   var pointerStr = ele.data("pointer");
   var colorRegex1 = /(#([0-9a-fA-F]{6}|[0-9a-fA-F]{3}))/; //匹配#000-#fff表示法的颜色
   var colorRegex2 = /([rR][gG][Bb][Aa]?[\(]([\s]*(2[0-4][0-9]|25[0-5]|[01]?[0-9][0-9]?),){2}[\s]*(2[0-4][0-9]|25[0-5]|[01]?[0-9][0-9]?),?[\s]*(0\.\d{1,2}|1|0)?[\)]{1})/g; //匹配rgba(122,122,122) rgba(122,122,122,0.89)
   if (pointerStr) {
	   var pointerColor = null;
	   if (colorRegex1.test(pointerStr)) {
		   pointerColor = RegExp.$1;
	   } else if(colorRegex2.test(pointerStr)) {
		   pointerColor = RegExp.$1;
	   } else if (pointerStr.indexOf("color:")>-1) {
		   var matchRegex = /color:(.+?)( |$)/; //,或者结束
		   var matchResult = pointerStr.match(matchRegex);
		   pointerColor = matchResult?matchResult[1]:null;
	   }
	   var maxSize4Pin = maxSize4Pin;
	   if (pointerStr.indexOf("max:")>-1) {
				var matchRegex = /max:(.+?)( |,|$)/; //,或者结束
				var matchResult = pointerStr.match(matchRegex);
				maxSize4Pin = matchResult?matchResult[1]:null;
	   }
	   var minSize4Pin = minSize4Pin;
       if (pointerStr.indexOf("min:")>-1) {
       		var matchRegex = /min:(.+?)( |,|$)/; //,或者结束
       		var matchResult = pointerStr.match(matchRegex);
       		minSize4Pin = Number(matchResult?matchResult[1]:null);
       }
       var pointerIndex = -1
       if (pointerStr.indexOf("index:")>-1) { 
				var matchRegex = /index:(.+?)( |,|$)/; //,或者结束
				var matchResult = pointerStr.match(matchRegex);
				pointerIndex = matchResult?matchResult[1]:null;
	   }
	   var pointerStyle = {
			   name: '点', 
			   type: 'scatter', 
			   coordinateSystem: 'geo',
			   symbol: 'pin', // 气球，默认圆
			   symbolSize: function (val) {
				   // 最小数据和最大数据有可能相等,相等时直接取最大值
				   var a = 1;
				   var b = maxSize4Pin;
				   if (max != min) {
					   a = (maxSize4Pin - minSize4Pin) / (max - min); //平均每份间隔
					   b = minSize4Pin;
				   }
				   var value = (val[2]-min)*a+ b;
				   //console.log(value+" maxSize4Pin:"+maxSize4Pin+" minSize4Pin:"+minSize4Pin+" max:"+max+" min:"+min+" val:"+val[2]+" a="+a+" b="+b);
				   return value;
			   },
			   label: {
				   normal: {
					   // show: true,
					   // textStyle: {
						   //     color: '#fff',
					   //     fontSize: 9,
					   // }
				   }
			   },
			   zlevel: 6,
			   data: convertData(data)
	   }
	   if (pointerColor) { //颜色
		   var pointerStyleExtend = {
				   itemStyle: {
					   normal: {
						   color: pointerColor, //标志颜色
					   }
				   }
		   }
		   pointerStyle = $.extend(true, pointerStyle, pointerStyleExtend);
	   }
	   var pointerIcon = null;
	   var iconRegex1 = /(circle|rect|roundRect|triangle|diamond|emptyCircle|emptyRectangle|emptyTriangle|emptyDiamond|pin|arrow|none)/; //icon
	   if (pointerStr.indexOf("icon:")>-1) { //自定义icon
		   var iconRegex = /icon:(\w+?)(,|$)/; //需要以,结束
		   var matchResult = pointerStr.match(iconRegex);
		   pointerIcon = matchResult?matchResult[1]:null;
	   } else if (iconRegex1.test(pointerStr)) {
		   pointerIcon = RegExp.$1;
	   } else if (pointerStr.indexOf("car")>-1) {
		   pointerIcon = "path://M874.666667 469.333333c17.28 42.666667 42.666667 85.333333 42.666666 128v213.333334c0 27.946667-23.893333 42.666667-64 42.666666s-64-17.28-64-42.666666v-42.666667H234.666667v42.666667c0 25.386667-23.893333 42.666667-64 42.666666s-64-14.72-64-42.666666V597.333333c0-42.666667 25.386667-85.333333 42.666666-128s-64-37.333333-64-64 5.333333-42.666667 42.666667-42.666666h64s30.293333-87.04 42.666667-128 64-64 106.666666-64h341.333334c42.666667 0 94.293333 23.04 106.666666 64s42.666667 128 42.666667 128h64c37.333333 0 42.666667 16 42.666667 42.666666s-81.28 21.333333-64 64z m-469.333334 149.333334a21.333333 21.333333 0 0 0 21.333334 21.333333h170.666666a21.333333 21.333333 0 0 0 21.333334-21.333333v-21.333334a21.333333 21.333333 0 0 0-21.333334-21.333333h-170.666666a21.333333 21.333333 0 0 0-21.333334 21.333333v21.333334zM192 618.666667a64 64 0 1 0 64-64 64 64 0 0 0-64 64z m597.333333-256c0-21.333333-42.666667-106.666667-42.666666-106.666667H277.333333s-42.666667 85.333333-42.666666 106.666667v21.333333a42.666667 42.666667 0 0 0 42.666666 42.666667h469.333334a42.666667 42.666667 0 0 0 42.666666-42.666667v-21.333333z m-21.333333 192a64 64 0 1 0 64 64 64 64 0 0 0-64-64z";
	   } else if (pointerStr.indexOf("stop")>-1) {
		   pointerIcon = "path://M30.9,53.2C16.8,53.2,5.3,41.7,5.3,27.6S16.8,2,30.9,2C45,2,56.4,13.5,56.4,27.6S45,53.2,30.9,53.2z M30.9,3.5C17.6,3.5,6.8,14.4,6.8,27.6c0,13.3,10.8,24.1,24.101,24.1C44.2,51.7,55,40.9,55,27.6C54.9,14.4,44.1,3.5,30.9,3.5z M36.9,35.8c0,0.601-0.4,1-0.9,1h-1.3c-0.5,0-0.9-0.399-0.9-1V19.5c0-0.6,0.4-1,0.9-1H36c0.5,0,0.9,0.4,0.9,1V35.8z M27.8,35.8 c0,0.601-0.4,1-0.9,1h-1.3c-0.5,0-0.9-0.399-0.9-1V19.5c0-0.6,0.4-1,0.9-1H27c0.5,0,0.9,0.4,0.9,1L27.8,35.8L27.8,35.8z";
	   } else if (pointerStr.indexOf("diewu")>-1){ 
		   pointerIcon = "path://M10,50 Q30,100 50,50 T80,70";
	   }
	   if (pointerIcon) { //icon
		   var pointerStyleExtend = {symbol: pointerIcon }
		   pointerStyle = $.extend(true, pointerStyle, pointerStyleExtend);
	   }
	   if (pointerIndex > -1) { 
		   options.series.splice(pointerIndex, 0, pointerStyle);
	   } else {
		   options.series.push(pointerStyle);
	   }
   }
   
   // 需要控制是否显示前几名，是否重点显示前几名data-top="pin;#F62157[rgba(255,255,0,0.8)]"
   var topDataStr = ele.data("topdata");
   if (topDataStr) {
	   topDataStr +=""; //转字符串
	   var topNum = 5;
	   var matchRegex = /(\d+?)( |,|$)/; //,或者结束
	   var matchResult = topDataStr.match(matchRegex);
	   topNum = matchResult?Number(matchResult[1]):null;
	   var topColor = '#61d2f7';
	   colorRegex2.lastIndex = 0; //清除全局正则的缓存
	   if (colorRegex1.test(topDataStr)) {
		   	topColor = RegExp.$1;
	   } else if(colorRegex2.test(topDataStr)) {
		   	topColor = RegExp.$1;
	   } else if (topDataStr.indexOf("color:")>-1) {
       		var matchRegex = /color:(.+?)( |$)/; //,或者结束
       		var matchResult = topDataStr.match(matchRegex);
       		topColor = matchResult?matchResult[1]:null;
	   }
	   var maxSymbolSize = 10; //最大10
	   if (pointerStr.indexOf("max:")>-1) {
		   var matchRegex = /max:(.+?)( |,|$)/; //,或者结束
		   var matchResult = pointerStr.match(matchRegex);
		   maxSymbolSize = matchResult?matchResult[1]:null;
	   }
	   var topStyle = { 
		   name: 'Top '+topNum, //前5
		   type: 'effectScatter',
		   coordinateSystem: 'geo',
		   data: convertData(data.sort(function (a, b) {
			   return b.value - a.value;
		   }).slice(0, topNum)),
		   symbolSize: function (val) {
			   return val[2] * maxSymbolSize/maxData;
		   },
		   showEffectOn: 'render',
		   rippleEffect: {
			   brushType: 'stroke'
		   },
		   hoverAnimation: true,
		   label: {
			   normal: {
				   formatter: '{b}',
				   position: 'right',
				   show: true
			   }
		   },
		   itemStyle: {
			   normal: {
				   color: topColor, //字体颜色
				   shadowBlur: 10,
				   shadowColor: '#fff'
			   }
		   },
		   zlevel: 1
	   }
	   options.series.push(topStyle);
   }
   return options;
}

ShieldCharts.addExtendFun("barPercent", function(ele, url) {
	var dom = ele[0];
    var myChart = echarts.init(dom, ShieldCharts.theme);
    
    $.getJSON(url, function(result){
    	var datas = result.data.data; //数据
        var titles = result.data.title; //表格标题
        var legends = result.data.axisName||[]; //标注
        
        var data = datas[0];
        var valdata = datas[1];
        var myColor = ['#1089E7', '#F57474', '#56D0E3', '#F8B448', '#8B78F6'];
        var containerData = [];
        for (var i = 0; i < data.length; i++) {
        	containerData.push(100);
        }
        
        var options = {
            title: {
                text: titles[0],
                x: 'center',
                textStyle: {
                    color: '#FFF'
                },
                left: '6%',
                top: '10%'
            },
            //图标位置
            grid: {
                top: '20%',
                left: '100'
            },
            xAxis: {
                show: false
            },
            yAxis: [{
                show: true,
                data: legends,
                inverse: true,
                axisLine: {
                    show: false
                },
                splitLine: {
                    show: false
                },
                axisTick: {
                    show: false
                },
                axisLabel: {
                    color: '#fff',
                    formatter: (value, index) => {
                        return [

                            `{lg|${index+1}}  ` + '{title|' + value + '} '
                        ].join('\n')
                    },
                    rich: {
                        lg: {
                            backgroundColor: '#339911',
                            color: '#fff',
                            borderRadius: 15,
                            // padding: 5,
                            align: 'center',
                            width: 15,
                            height: 15
                        },
                    }
                },


            }, {
                show: true,
                inverse: true,
                data: valdata,
                axisLabel: {
                    textStyle: {
                        fontSize: 12,
                        color: '#fff',
                    },
                },
                axisLine: {
                    show: false
                },
                splitLine: {
                    show: false
                },
                axisTick: {
                    show: false
                },

            }],
            series: [{
                name: '条',
                type: 'bar',
                yAxisIndex: 0,
                data: data,
                barWidth: 10,
                itemStyle: {
                    normal: {
                        barBorderRadius: 20,
                        color: function(params) {
                            var num = myColor.length;
                            return myColor[params.dataIndex % num]
                        },
                    }
                },
                label: {
                    normal: {
                        show: true,
                        position: 'inside',
                        formatter: '{c}%'
                    }
                },
            }, {
                name: '框',
                type: 'bar',
                yAxisIndex: 1,
                barGap: '-100%',
                data: containerData,
                barWidth: 15,
                itemStyle: {
                    normal: {
                        color: 'none',
                        borderColor: '#00c1de',
                        borderWidth: 3,
                        barBorderRadius: 15,
                    }
                }
            }, ]
        };
        
        showCharts(ele, myChart, options, result);
    });

},"横向占比柱状图");