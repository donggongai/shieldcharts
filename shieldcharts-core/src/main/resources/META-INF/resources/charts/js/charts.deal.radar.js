var radarDeal = function(ele, url) {
	ShieldCharts.getJSON(url, function(result) {
		var dom = ele[0];
		var myChart = echarts.init(dom, ele.data("theme") || ShieldCharts.theme);

		var datas = result.data.data; /**数据 */
		var legends = result.data.axisName || []; /**标注 */
		var toolTips = result.data.toolTip || legends;
		var titles = result.data.title || [];
		//var isArrayData = $.isArray(datas[0]); /**说明有多组数据 */
		var indicators = result.data.indicators || []; //雷达图的指示器，用来指定雷达图中的多个变量（维度）
		//var isArrayRadar = $.isArray(indicators[0]); /**说明有多个雷达图 */

		var radarData = [];
		var serieData = [];
		/**绘制图形 */
		drawRadar(indicators, radarData, titles[0]);
		/**检查溢出 */
		indicators = isMax(indicators,datas);
		fillData(datas, serieData, titles[0]);
		//名称过滤
		var title = result.data.title || [];  //result.title;
		var colors = ["#32aa66","#ffff43", '#c23531', '#2f4554', '#61a0a8', '#d48265', '#91c7ae', '#749f83', '#ca8622', '#bda29a', '#6e7074', '#546570', '#c4ccd3'];
		var options = {
			title: {
				text: title,
				textStyle: {
					color: "#000",
					fontSize: "16",
					fontWeight: "bolder"
				}
			},
			tooltip: { //提示框组件。
				trigger: 'axis'//触发类型
			},
			/*legend: {
				data: titles
			},*/
			color: colors,
			radar: radarData,
			series: serieData
		};
		showCharts(ele, myChart, options, result);
	});
	/**绘制雷达坐标图 */
	var drawRadar = function(indicator, radarData, title) {
		var radar = {
			indicator: indicator,
			center: ['50%', '50%'], /**相对div中心的位置 */
			radius: 80 /**半径 */
		}
		radarData.push(radar);
	}
	/**填充数据 */
	var fillData = function(datas, serieData, title, options) {
		/**serie各配置项 */
		var serie = {
			type: 'radar',/**组件类型 */
			radarIndex: 0, /**雷达图所使用的 radar 组件的 序号（从0开始）。适用于多个雷达图时 */
			tooltip: {},
			areaStyle: {},
			data: datas
		};
		serie.tooltip={trigger: 'item'};/**提示框浮层的显示 */
		serieData.push(serie);
	}
	/**如果数值溢出，则设置为最大值 */
	function isMax(indicators,datas){
		var data = datas[0].value;
		for(var i=0;i<data.length;i++){
			if(indicators[i].max<data[i]){
				indicators[i].max=data[i];
			}
		}
		return indicators;
	}
}