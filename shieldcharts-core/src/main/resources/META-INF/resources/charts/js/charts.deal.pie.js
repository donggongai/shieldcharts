// 饼状图，南丁格尔玫瑰图，环状图
var pieDeal = function(ele, url) {
	ShieldCharts.getJSON(url, function(result) {
        var dom = ele[0];
        var myChart = echarts.init(dom, ele.data("theme")||ShieldCharts.theme);
        
        var datas = result.data.data; //数据
        var legends = result.data.axisName||[]; //标注
    	var toolTips = result.data.toolTip||legends;//数据过滤掉前面的20 ，最后一个数据添加年
    	var titles = result.data.title||[];//数据过滤掉前面的20 ，最后一个数据添加年
    	var isArray = $.isArray(datas[0]); //说明有多组数据
    	
    	var radius = "50%";
    	if(ele.data("ring")){ //显示环,双环和环饼图用
    		var ringStr = ele.data("ring");
    		if ( $.isArray(ringStr)) {
    			radius = ringStr;
            }else {
            	radius = [ '50%', ele.data("ring") ] //饼图的半径，数组的第一项是内半径，第二项是外半径。
			}
    	}
    	var radius0 = "";
    	if(ele.data("ring0")){ //内环半径，双环和环饼图用
    		var ringStr = ele.data("ring0");
    		if ( $.isArray(ringStr)) {
    			radius0 = ringStr;
            }
    	}
    	ShieldCharts.debug("radius")
    	ShieldCharts.debug(radius)
    	var roseType; //南丁格尔玫瑰图radius半径模式 area面积模式
    	if(ele.data("rose") || ele.data("rose")==""){ //显示环
    		roseType = ele.data("rose")||"radius"; //radius半径模式 area面积模式
    	}
    	
    	var showLobel = true;
    	if(ele.data("label")=="close"){ //关闭标签
    		showLobel = false;
    	}
    	 
    	var serieData = [];
    	var optionsFillData = {radius :radius,showLobel :showLobel,roseType:roseType, title:titles[0]};
    	if (radius0) {
    		optionsFillData.radius0 = radius0;
        }
    	// 数据填充
    	if(isArray){ //
    		optionsFillData.length = datas.length;
        	for (var i = 0; i < datas.length; i++) {
        		var pieDatas = []; //数据
        		var data = datas[i];
        		optionsFillData.index = i;
        		optionsFillData.title = titles[i];
        		fillData(data, serieData, legends, optionsFillData); //填充数据
        	}
    		
    	} else {
    		fillData(datas, serieData, legends, optionsFillData); //填充数据
    	}
        
        var options={};
    	var formatterFn = function(params) { 
    		if(params.value!="undefined"){//如果没定义就什么不做
            	var dataIndex = params.dataIndex;
            	var seriesIndex = params.seriesIndex;
                var toolTipName = toolTips[dataIndex];
                var toolTiphtml = titles[seriesIndex]||""; //dataIndex seriesIndex
            	var title = toolTipName;
        		title ="<br>"+title;
                toolTiphtml += title+':'+params.value;
       			return toolTiphtml;
            }
        };
        if(ele.data("percent")=="show" || (ele.data("style")||"").indexOf("percent")>-1){ //百分比显示
        	formatterFn = "{a} <br/>{b}: {c} ({d}%)"; //提示框浮层内容格式器，支持字符串模板和回调函数两种形式。
//        	formatter: "{b}: <br/>{c} ({d}%)"
        }
        
        // 设置
		options = {
            title: {
                text: '', // 主标题名称
                subtext: '', // 副标题名称
                left: 'center' // 标题的位置
            },
            tooltip : { //提示框组件
                trigger: 'item', //（本图中的提示消息）触发类型 数据项图形触发，主要在散点图，饼图等无类目轴的图表中使用。
                formatter: formatterFn
            },
            legend : { //图例组件展现了不同系列的标记(symbol)，颜色和名字。可以通过点击图例控制哪些系列不显示。
            	// orient: 'vertical',         // 标签名称垂直排列
                // x : 'center', // 标签的位置
                y : 'bottom',
                data: legends, // 标签变量名称
                textStyle:{
                    color:"#61d2f7" //可动态
                }
            },
            calculable : true,
            //color: ['#e8451a','#f4b825','#17a390','#2496c1','#5e3287'], //可动态
            series : serieData
        };
		var legendOrient= ele.data("legendOrient");
        if(legendOrient){
        	options.legend.orient = legendOrient;
        }
        var selected= ele.data("selected");
        if(selected || selected==""){ //空表示0
        	if (!selected) {
        		selected = 0;
            }
        	options.series[0].data[selected].selected = true;
        }
        
        showCharts(ele, myChart, options, result);
    });
	
	// 填充数据
	var fillData = function(datas, serieData, title, options) {
		var radius = options.radius;
		var showLobel = options.showLobel;
		var length = options.length;
		var index = options.index;
		var roseType = options.roseType;
		
		legends = title;
		var pieDatas = []; //数据
    	for (var i = 0; i < datas.length; i++) { //处理成name value格式的
    		var serie = {
    			name : legends[i]||"数据",
    			value : datas[i]
    		};
    		pieDatas.push(serie);
        }
    	
		var serie = {
			name:options.title||'数据',
     		type:'pie',
     		radius : radius, 
   			avoidLabelOverlap : false, //如果不需要开启该策略，例如圆环图这个例子中需要强制所有标签放在中心位置，可以将该值设为 false。
       		label : { //饼图图形上的文本标签，可用于说明图形的一些数据信息
				normal : {
					show : showLobel
				}
			},
			labelLine : { //标签的视觉引导线样式
				normal : {
					show : showLobel
				}
			},
			data : pieDatas
			//,roseType: 'radius' //南丁格尔玫瑰图radius半径模式 area面积模式
		};
		
		
		if (length>1) {
			serie.center = [(100/2/length+100/length*index)+'%', '50%'];
        }
//		if(ele.data("ring")){ //显示环
		if(showLobel){ //显示环
			var position = ele.data("label");
			if (position=='center') { //显示在中间
				var serieExtend = {
					label : { //饼图图形上的文本标签，可用于说明图形的一些数据信息
						normal : {
							show : false,
							position : position
						},
						emphasis : {
							show : true,
							textStyle : {
								fontSize : '30',
								fontWeight : 'bold'
							}
						}
					}
				}
				serie = $.extend(true, serie, serieExtend);
            }
    	} else { //不显示标签
    		var serieExtend = {
				label : { //饼图图形上的文本标签，可用于说明图形的一些数据信息
					normal : {
						show : false
					},
					emphasis : {
						show : false
					}
				}
			}
			serie = $.extend(true, serie, serieExtend);
		}
		if (roseType) { ////南丁格尔玫瑰图radius半径模式 area面积模式
			serie.roseType = roseType;
        }
		
		if (ele.data("style")) { //样式
			var style = ele.data("style");
			if (style.indexOf("ringpie")>-1 || style.indexOf("ringpie")>-1) { //双环图
				serie.label.normal.position = "inside";
				serie.label.normal.formatter = "{d}%";
	        } else if (style.indexOf("halfring")>-1 || style.indexOf("halfRing")>-1) { //半环图
	        	serie.roseType = 'area'; //  'area' 所有扇区面积相同，仅通过半径展现数据大小
	        	serie.startAngle = 0; //即顺时针旋转90度
	        	//serie.radius = [41, 110];
//	        	//支持设置成百分比，设置成百分比时第一项是相对于容器宽度，第二项是相对于容器高度
//	        	serie.center = ['50%', '20%'];
//	        	serie.label.normal.show = true,
//	        	serie.label.normal.formatter = '{c}辆';
	        	for (var i = 0; i < datas.length; i++) { //处理成name value格式的
	        		var data = {
	        			name : "",
	        			value : 0,
                        label: {
                            show: false
                        },
                        labelLine: {
                            show: false
                        }
	        		};
	        		serie.data.push(data);
	            }
	        	if (!ele.data("ring")) { //没有则设置默认
	        		serie.radius = [41, 110];
                }
	        	
	        }
			if (style.indexOf("anticlockwise")>-1 ) { //逆时针
				serie.clockwise = false; //饼图的扇区是否是顺时针排布;
	        }
			if (style.indexOf("emphasis")>-1 ) { //强调
				var serieExtend = {
					label : {
						emphasis : {
							show : true, //突出显示
							textStyle : {
								fontSize : '30',
								fontWeight : 'bold'
							}
						}
					}
				}
				serie = $.extend(true, serie, serieExtend);
	        }
//			if (style.indexOf("avoidLabelOverlap")>-1 ) { //防止label覆盖,迁移到公共方法
//				serie.avoidLabelOverlap = true;
//	        }
        }
		
		if (ele.data("itemborder")) { //边框
			var serieExtend = {
				 itemStyle: { //图形样式
                    normal: {
                        borderColor: '#1e2239',
                        borderWidth: ele.data("itemborder")
                    }
                }
			}
			serie = $.extend(true, serie, serieExtend);
        }
		if (ele.data("itemStyle")) { //边框
			var serieExtend = {}
			serieExtend.itemStyle = ele.data("itemStyle");
			serie = $.extend(true, serie, serieExtend);
        }
		serieData.push(serie);
		if (ele.data("style")) {
			//填充数据
			var style = ele.data("style");
			style = style.toLowerCase()
			if (style.indexOf("ringpie")>-1) { //环饼图 环+饼图
				var newObj = JSON.parse(JSON.stringify(serie));
				var radius0 = [0, "40%"];
				if (options.radius0) {
					radius0 = options.radius0;
                }
				
                var serieExtend = {
    				label : { //饼图图形上的文本标签，可用于说明图形的一些数据信息
    					normal : {
    						show : false
    					}
    				},
    				radius : radius0, //饼图的半径
    				silent : true, //禁用
    				itemStyle: { //图形样式
                        normal: {
                            borderColor: '#1e2239',
                            borderWidth: 1.5,
                            opacity: 0.21 //降低可见度
                        }
                    }
        		}
                newObj = $.extend(true, newObj, serieExtend);
				serieData.push(newObj);
	        }
			if (style.indexOf("doublering")>-1) { //双环图
				var newObj = JSON.parse(JSON.stringify(serie)); //深层拷贝
				var doublecolor = ele.data("doublecolor")||[];
				var radius0 = ['45%', '50%'];
				if (options.radius0) {
					radius0 = options.radius0;
                }
                var serieExtend = {
    				label : { //饼图图形上的文本标签，可用于说明图形的一些数据信息
    					normal: {
                            show: false
                        },
                        emphasis: {
                            show: false
                        }
    				},
    				radius : radius0, //饼图的半径
    				animation: false,
    				itemStyle: { //图形样式
                        /*normal: {
                            borderColor: '#1e2239',
                            opacity: 0.8 //降低可见度
                        }*/
                    }
        		}
                newObj = $.extend(true, newObj, serieExtend);
                if (doublecolor) {
                	newObj.data.map((item, index) => {
                		item.itemStyle = {
            				normal: {
            					color: doublecolor[index] // 0% 处的颜色
            				}
                		}
                	});
                }
				serieData.push(newObj);
	        }
        }
    };
}