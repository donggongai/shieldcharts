/**
* numAnimateShow
* @fileOverview 数字动态显示
* @author kevin Lv
* @email donggongai@126.com
* @version 3.0.0
* @date 2019-1-25
*/
;(function($){
	/**
	* @author kevin Lv
	* @constructor numAnimateShow
	* @description 数字动态显示
	* @see The <a href="#">kevin</a>
	* @example
	*	$("#form").numAnimateShow();
	*	$("#form").numAnimateShow({duration : durationDefault, separate : separateDefault});
	* @since version 2.0
	*/
	$.fn.kvcharts = function(operate){
		//debug("选择器["+this.selector+"]调用自定义插件 numAnimateShow，调用元素数 "+this.size()+" 个[jQuery "+this.jquery+"]");
		/**
		* @description 处理函数
		*/
		var eles = $(this); //表单
		eles.each(function(){
			var $this = $(this);
			$this.unbind("refresh").bind("refresh", function() { //刷新
				load($this);
			});
			$this.unbind("stopRefresh").bind("stopRefresh", function() { //停止自动刷新
				$this.removeData("refreshinit");
			});
			var autoload = true; //自动加载
			if ($this.data("autoload") && $this.data("autoload")=="close") {
				autoload = false;
            }
			if (autoload || operate=="refresh" || operate=="load") {
				load($this);
            }
			var refreshTime = $this.data("refresh"); //秒
			var refreshinit = $this.data("refreshinit"); //秒
			if (refreshTime && !refreshinit) {
				$this.data("refreshinit","1"); //可以执行刷新，如果移除说明停止刷新
				refreshTime = Number(refreshTime)||10; //默认10
	            /*setInterval(() => {
	            	load($this);
	            	$this.data("refreshinit","1");
				}, refreshTime*1000);*/
				// 这个模式链式调用了setTimeout()，每次函数执行的时候都会创建一个新的定时器。第二个setTimeout()调用使用了arguments.callee来获取对当前执行的函数的引用，并为其设置另外一个定时器。
				// 这样做的好处是，在前一个定时器代码执行完之前，不会想队列插入新的定时器代码，确保不会有任何缺失的间隔。而且，可以保证在下一次定时器代码执行之前，至少要等等指定的间隔，避免了连续的运行。
	            setTimeout(function () {
	            	if ($this.data("refreshinit")) {
	            		// 处理中
	            		load($this);
	            		setTimeout(arguments.callee, refreshTime*1000);
                    }
            	}, refreshTime*1000);
            }
		});
	};
	// 加载
	var load = function(ele) {
		var url = ele.data("url");
		var type = ele.data("type");
		
	    if (type == "pie") { //饼状图
	    	revokeFn(pieDeal(ele, url));
	    } else if (type == "bar") { // 柱形图
	    	revokeFn(barDeal(ele, url));
	    } else if (type == "line"){ //折线图
	    	revokeFn(lineDeal(ele, url));
		} else if (type == "map"){ //地图
			revokeFn(mapDeal(ele, url));
		} else if (type == "list"){ //列表
			revokeFn(listDeal(ele, url));
		} else if (type == "count"){ //数目处理
			revokeFn(countDeal(ele, url));
		} else if (type == "radar"){ //雷达图
			revokeFn(radarDeal(ele, url));
		} else if (type == "extend"){ //扩展方法
			var dealFn = ele.data("extendfn");
			if (dealFn) {
				if ($.isFunction(ShieldCharts.extendFuns[dealFn].fn)) {
					ShieldCharts.extendFuns[dealFn].fn(ele, url);
	            } else {
	            	ShieldCharts.debug("自定义方法不存在："+dealFn);
	            }
	        } 
		}
    }
	var revokeFn = function(fn) {
		if (typeof mapDeal === 'function') {
			fn;
		}
	}
	
	
	//私有方法
	/**
	* @description 输出选中对象的个数到控制台
	* @param {msg} String 输出内容
	*/
	function debug(msg) {
		if (window.console && window.console.log){
			window.console.log('numAnimateShow.js log: ' + msg);
		}
	}
	$(".loadItem").each(function() {
		var $this = $(this);
		if(!$this.data('inited')){
			$this.data('inited', true);
			$this.kvcharts();
		}
    });
})(jQuery);  
