// 地图 https://github.com/carsonWuu/echartJs/tree/master/ECharts_%E5%9C%B0%E5%9B%BE
var mapDeal = function(ele, url) {
	//河南 http://gallery.echartsjs.com/editor.html?c=xrJcPf9hLW
    //如果需要替换地图  只需要 将 jiangxi 这个变量修改为对应的地区的genJson,城市名称修改为对应的城市
	ShieldCharts.getJSON(url, function(result) {
		if (!result.success) { //失败返回
	        return;
        }
        var dom = ele[0];
        var myChart = echarts.init(dom, dom, ele.data("theme")||ShieldCharts.theme);
        
        var datas = result.data.data; //数据
        var legends = result.data.axisName||[]; //标注
    	var toolTips = result.data.toolTip||legends;//数据过滤掉前面的20 ，最后一个数据添加年
    	var titles = result.data.title||[];//数据过滤掉前面的20 ，最后一个数据添加年
    	var isArray = $.isArray(datas[0]); //说明有多组数据
    	var mapAreaName = result.data.name; //地图区域
    	var showName = result.data.showName||"未定义"; //重点展示区域
    	var adcode = result.data.adcode||"100000"; //区域编码用来获取数据
    	
    	ShieldCharts.getJSON(chartsjsPath +'/map/geodata/'+adcode+'.geoJson', function(result) {
    		var mapJson = {"type": "FeatureCollection"};
            mapJson.features = result.features;
    		echarts.registerMap(mapAreaName, mapJson ); //注册地区
            
            var toolTipData = Array();  // 提示消息的对象  暂时以改动最小改
            var data = Array();  //焦点数组
            var depNameArr = Array();
            if(!datas || datas.size <= 0){ // 如果没有部门数据，直接不展示，避免浏览器崩溃
                return;
            }
            for(var key  in datas){
                var model = datas[key];
                var obj= {},jiaodian = {};
                //手动去除 带市 的数据
                model.departName = model.departName.replace("市","");
                obj.name = showName+model.departName;
                jiaodian.name = showName+model.departName;
                depNameArr.push(model.departName); // 部门数组
                jiaodian.value = 60; //先固定大小
                data.push(jiaodian);				//增加焦点的显示
                obj.value = Array();
                var objArr = {};
                objArr.name = "数据更新总数";
                objArr.value=model.sumCount;
                obj.value.push(objArr); objArr = {};
                objArr.name = "行政处罚";
                objArr.value=model.punishCount;
                obj.value.push(objArr); objArr = {};
                objArr.name = "行政许可";
                objArr.value=model.licensCount;
                obj.value.push(objArr); objArr = {};
                objArr.name = "红名单";
                objArr.value=model.redCount;
                obj.value.push(objArr); objArr = {};
                objArr.name = "黑名单";
                objArr.value=model.blackCount;
                obj.value.push(objArr); objArr = {};
                objArr.name = "信用信息";
                objArr.value=model.archivesCount;
                obj.value.push(objArr); objArr = {};
                //存进来 新对象
                toolTipData.push(obj);
            }
         
            var geoCoordMap = getGeoCoordMap(depNameArr, showName, mapJson)||{};
            // alert(JSON.stringify(geoCoordMap));

            // var geoCoordMap ={"温县公安局":["113.170396","34.944158"],"温县发改委":["113.005815","34.971874"],"温县教育局":["113.070565","35.012286"],"温县科技局":["113.172588","34.907822"],"温县民政局":["113.047739","34.998929"],"温县司法局":["113.084895","34.884922"],"温县财政局":["113.015915","34.954458"],"温县人社局":["113.080642","35.014758"],"温县交运局":["113.114998","35.015564"],"温县商务局":["113.093762","34.911621"],"温县卫计委":["113.183507","34.967937"],"温县环保局":["113.175927","34.881655"],"温县统计局":["113.101164","34.970366"],"温县粮食局":["113.130015","34.901586"],"温县人防办":["113.112093","35.039791"],"温县安监局":["112.923721","34.921630"],"温县国税局":["113.153971","34.995828"],"温县地税局":["113.039199","34.957628"],"温县质量技术监督局":["113.143436","34.880422"],"温县法院":["112.982204","34.889646"],"温县住建局":["113.156871","34.851160"],"温县档案局":["112.976844","34.989484"],"温县移民安置局":["113.072554","34.911925"],"温县国土资源局":["112.963485","34.898631"],"温县供销社":["113.048156","34.871765"],"温县水利局":["112.997077","34.988995"],"温县农林局":["112.993366","34.870664"],"温县审计局":["113.113478","34.939368"],"温县体育局":["112.897899","34.996908"],"温县编办":["113.151578","34.849643"],"温县烟草专卖局":["113.136263","35.037216"],"温县气象局":["112.880222","34.961688"],"温县邮政局":["113.123114","34.951531"],"温县城管局":["112.984635","34.900488"],"温县县政府金融办":["112.919141","34.928484"],"温县人行温县支行":["113.003279","34.903195"],"温县文广新局":["113.001462","34.969037"],"温县工商局":["113.166105","34.952631"],"温县旅游局":["113.128921","34.982911"],"温县食品药品监督管理局":["112.950228","35.004131"],"温县农业机械管理局":["112.948808","34.999067"],"温县县政府法制办":["113.187716","34.938745"],"温县畜牧局":["113.049204","34.982427"],"温县行政服务中心":["112.882155","35.004165"],"温县盐业管理局":["113.141431","35.020441"],"温县检察院":["113.065444","35.005177"],"温县信访局":["112.934742","34.974497"],"温县监察委":["113.108336","34.985094"],"温县工信委":["113.124446","35.013023"],"温县河务局":["112.975158","34.919802"],"温县机关事务管理局":["113.129322","34.995965"],"温县县网络经济发展促进中心":["112.965346","34.864913"],"温县县新闻网络中心":["113.018691","35.014004"],"温县县文明办":["113.144987","34.978889"],"温县公共资源交易中心":["113.064251","35.018919"],"温县银监会焦作银监分局温县办事处":["113.124194","34.966043"],"温县温县发展和改革委员会":["113.130147","34.968985"],"温县政法委":["113.051334","34.997194"],"温县房管中心":["113.051400","34.988000"],"温县民宗委":["113.151500","34.978000"],"温县税务局":["113.161500","34.968000"],"人行温县支行":["113.162500","34.958000"],"银监会焦作银监分局温县办事处":["113.152500","34.948000"],"温县交通局":["113.142500","34.938000"],"温县":["113.042345","34.936756"]};
            var centerMap = geoCoordMap[showName]||{}; // geoCoordMap["温县工商局"]; //视角的中心	//{'温县': [112.798543,34.913124],"温县人社局":[112.637345,34.853843],"温县公安局":[112.633985625,34.9204836250001],"温县工商局":[112.857345,34.993843],"温县国税局":[112.571429472656,34.9697585273438],"温县卫计委":[112.921690703125,34.9114833808594],"温县交运局":[112.671243925781,34.9950051093751],"温县食品药品监督管理局":[112.723260527344,35.0379274726563],"温县质量技术监督局":[112.80545046875,34.8381618476563],"温县地税局":[112.567345,34.953843],"温县住建局":[112.804227324219,34.9989125800782],"温县环保局":[112.738638945313,34.8130568671876],"温县民政局":[112.693082304688,35.0266347480469],"温县司法局":[112.90197390625,34.9284706855469],"温县安监局":[112.654212675781,34.8475051093751],"温县农林局":[112.693082304688,35.0266347480469],"温县文广新局":[112.839298125,34.8146230292969],"温县行政服务中心":[112.927345,34.843843],"温县盐业管理局":[112.738638945313,34.8130568671876],"温县粮食局":[112.650152617188,34.8988430000001],"温县发改委":[112.654212675781,34.8475051093751],"温县水利局":[112.738638945313,34.8130568671876],"温县商务局":[112.580704375,34.9272023750001],"温县人防办":[112.637345,34.893843],"温县畜牧局":[112.624537382813,34.8988430000001],"温县科技局":[112.80545046875,34.8381618476563],"温县财政局":[112.580704375,34.9272023750001],"温县旅游局":[112.723260527344,35.0379274726563],"温县银监会焦作银监分局温县办事处":[112.857345,34.993843],"温县教育局":[112.674823027344,34.84960471875],"温县统计局":[112.571429472656,34.9697585273438],"温县法院":[112.624537382813,34.8988430000001],"温县档案局":[112.804227324219,34.9989125800782],"温县移民安置局":[112.727345,35.043843],"温县国土资源局":[112.637345,34.853843],"温县供销社":[112.90197390625,34.9284706855469],"温县审计局":[112.654212675781,34.8475051093751],"温县体育局":[112.927345,34.843843],"温县编办":[112.693082304688,35.0266347480469],"温县烟草专卖局":[112.873558378906,34.9657204414063],"温县气象局":[112.89271609375,34.9492153144532],"温县邮政局":[112.624537382813,34.8988430000001],"温县城管局":[112.654212675781,34.8475051093751],"温县政府金融办":[112.633985625,34.9204836250001],"温县人行温县支行":[112.921690703125,34.9114833808594],"温县农业机械管理局":[112.637345,34.893843],"温县政府法制办":[112.738638945313,34.8130568671876],"温县检察院":[112.637345,34.893843],"温县信访局":[112.90197390625,34.9284706855469],"温县监察委":[112.567345,34.953843],"温县工信委":[112.637345,34.9038430000001],"温县河务局":[112.857345,34.993843],"温县机关事务管理局":[112.753416777344,35.0365822578125],"温县网络经济发展促进中心":[112.674823027344,34.84960471875],"温县新闻网络中心":[112.90197390625,34.9284706855469],"温县文明办":[112.89271609375,34.9492153144532],"温县公共资源交易中心":[112.738638945313,34.8130568671876],"温县信用办":[112.804227324219,34.9989125800782],"温县残联":[112.839298125,34.8146230292969],"温县爱卫办":[112.839298125,34.8146230292969],"温县民宗委":[112.624537382813,34.8988430000001],"温县电力公司":[112.571429472656,34.9697585273438],"温县电信公司":[112.637345,34.893843],"温县移动公司":[112.921690703125,34.9114833808594],"温县联通公司":[112.89271609375,34.9492153144532]};
            var dataFrom = showName;  //显示的中心点 修改为博爱县 '焦作市'
            var series = [
                {
                    name:  showName,  //运行的线
                    type: 'lines',
                    zlevel: 1,
                    effect: {
                        show: true,
                        period: 6,
                        trailLength: 0.7,
                        color: '#fff',
                        symbolSize: 3
                    },
                    lineStyle: {
                        normal: {
                            color:'#a6c84c',
                            width: 0,
                            curveness: 0.2
                        }
                    },
                    data: data.map(function (dataItem) {
                        return {
                            fromName: dataFrom,
                            toName: dataItem.name,
                            coords: [
                                geoCoordMap[dataItem.name],
                                geoCoordMap[dataFrom],
                            ]
                        }
                    })
                },
                {
                    name: showName,   //运行的线2
                    type: 'lines',
                    zlevel: 2,
                    symbol: ['none', 'arrow'],
                    symbolSize: 10,
                    effect: {
                        show: true,
                        period: 6,
                        trailLength: 0,

                    },
                    lineStyle: {
                        normal: {
                            color:'#a6c84c',
                            width: 1,
                            opacity: 0.6,
                            curveness: 0.2
                        }
                    },
                    data: data.map(function (dataItem) {
                        return {
                            fromName: dataFrom,
                            toName: dataItem.name,
                            coords: [ //第一个数据一对多， 第二个数据 多对一
                                geoCoordMap[dataItem.name],
                                geoCoordMap[dataFrom]
                            ]
                        }
                    })
                },
                {
                    name:  showName,   //显示的点点
                    type: 'effectScatter',
                    coordinateSystem: 'geo',
                    zlevel: 2,
                    rippleEffect: {
                        scale: 5,
                        brushType: 'stroke'
                    },
                    label: {
                        normal: {
                            show: true,
                            position: 'right',
                            formatter: '{b}'
                        }
                    },
                    symbolSize: function (val) {
                        return val[2] / 6;
                    },
                    itemStyle: {
                        normal: {
                            color: '#a6c84c'
                        }
                    },
                    data: data.map(function (dataItem) {
                        return {
                            name: dataItem.name,
                            value: geoCoordMap[dataItem.name].concat([dataItem.value])
                        };
                    })
                }
            ];
            var options = {
                backgroundColor: '', //地图背景颜色 #154e90
                dataRange: {
                    min: 0,
                    max: 100,
                    calculable: true,
                    color: ['#ff3333', 'orange', 'yellow', 'lime', 'aqua'],
                    textStyle: {
                        color: '#fff'
                    },
                    show: true,
                    right:10
                },
                title: {
                    text: '', //主标题 区县接入情况
                    subtext: '', //副标题
                    left: 'center',
                    textStyle: {
                        color: '#fff'
                    }
                },
                tooltip : { //提示框组件。
                    trigger: 'item' ,//触发类型
                    // formatter: '{b0}:\n {c0}',//{a}（系列名称），{b}（区域名称），{c}（合并数值）, {d}（无）
                    formatter: function(params) {
                        if(params.value=="undefined"){} //如果没定义就什么不做
                        else {
                            var toolTiphtml = ''
                            for(var i = 0;i<toolTipData.length;i++){
                                if(params.name==toolTipData[i].name){
                                    toolTiphtml += toolTipData[i].name+':<br>'
                                    for(var j = 0;j<toolTipData[i].value.length;j++){
                                        toolTiphtml+=toolTipData[i].value[j].name+':'+toolTipData[i].value[j].value+"<br>"
                                    }
                                }
                            }
//                  console.log(toolTiphtml)
                            // console.log(convertData(data))
                            return toolTiphtml;
                        }
                    }
                },
                geo: {
                    map: mapAreaName,
                    center: centerMap, //当前视角的中心点，用经纬度表示  这个搭配放大 可开始就定位
                    zoom:1, //当前视角的缩放比例。
                    scaleLimit:{  //滚轮缩放的极限控制，通过min, max最小和最大的缩放值，默认的缩放为1。
                        min:2,
                        max:20
                    },
                    regions: [{   //单独的地图的样式
                        name: showName,
                        itemStyle: {
                            areaColor: '#00bfff', // 地图区域的颜色。
                            color: '#00bfff' 	//图形颜色
                        }
                    }],
                    label: {
                        normal: {
                            show: false
                        },
                        emphasis: {
                            show: false
                        }
                    },
                    roam: true,  // 是否开启鼠标缩放和平移漫游。默认不开启。如果只想要开启缩放或者平移，可以设置成 'scale' 或者 'move'。设置成 true 为都开启
                    /* 	        itemStyle: {
                                    normal: {
                                        areaColor: 'black',
                                        borderColor: '#404a59'
                                    },
                                    emphasis: {
                                        areaColor: '#2a333d'
                                    }
                                },
                     */
                    label: {
                        normal: {
                            show: true,
                            textStyle: {
                                color: '#fff'
                            }
                        },
                        emphasis: {
                            textStyle: {
                                color: '#fff'
                            }
                        }
                    },
                    itemStyle: {
                        normal: {
                            color:'rgba(0, 41, 97, 1)', //图形的颜色
                            borderColor: 'rgba(86, 155, 197, 1)',//'rgba(147, 235, 248, 1)', //图形的描边颜色。支持的颜色格式同 color，不支持回调函数。
                            borderWidth: 2,
                            areaColor: {
                                type: 'radial',
                                x: 0.5,
                                y: 0.5,
                                r: 0.8,
                                colorStops: [{
                                    offset: 0,
                                    color: '#3399ff' // 0% 处的颜色  #3399ff 实现一个好的团  rgba(0, 41, 97, 1)
                                }, {
                                    offset: 1,
                                    color: 'rgba(0, 41, 97, 1)' // 100% 处的颜色
                                }],
                                globalCoord: false // 缺省为 false
                            },
                            shadowColor: 'rgba(0, 41, 97, 1)',  // 阴影颜色 rgba(128, 217, 248, 1)
                            // shadowColor: 'rgba(255, 255, 255, 1)',
                            shadowOffsetX: -2,
                            shadowOffsetY: 2,
                            shadowBlur: 10
                        },
                        emphasis: { //图形div  的 盒子模型
                            areaColor: '#00bfff',//'rgba(70, 144, 196, 1)',//'#389BB7',
                            borderWidth: 0,
                            /*                     itemStyle:{ //高亮下的图形颜色
                                                    color:'red',
                                                    areaColor :'red'
                                                } */
                        }
                    },
                },
                series: series
            };
             
            showCharts(ele, myChart, options, result);
            
    	});
       
    });
	
	// 填充数据
	var fillData = function(datas, serieData, title, options) {
		
    }
	
	// 根据部门名称和区域获取各部门的坐标（随机生成）
    function getGeoCoordMap(depNameArr, areaName, geoJson){
        var polygonArr =Array(); //不规则区域数组
        var returnObject;
        var maxX=0,maxY=0,minX=1000,minY=1000;
        //然后开始执行
        var geoJsonObj = geoJson;
        if (typeof(geoJson) == 'string') { //string类型转json格式
        	geoJsonObj = eval('('+ geoJson +')');
        }
        var featuresArr = geoJsonObj.features;
        var areaArr = Array(); //区域的array的保存
        // 获取位置坐标
        for(var key in featuresArr) {
            var model = featuresArr[key];
            var arr = null;
            if (model.properties.name.indexOf(areaName) > -1 /*|| cityName == areaName*/) { //相同的区域   如果区域相同时生成全部的
                arr = model.geometry.coordinates; //经纬度的数组
                break;
            }
        }
        // 坐标不存在则取第一个
        if (!arr) {
        	arr = featuresArr[0].geometry.coordinates;
        }
        if (arr) {
        	// 取坐标
            for (var key2 in arr) {
                var arr2 = arr[key2];
                for (var key3 in arr2) {
                    var arr3 = arr2[key3];
                    for (var key4 in arr3) {
                        var arr4 = arr3[key4];
                        //生成 不规则点数组
                        var pt = new point();
                        pt.x = arr4[0];
                        pt.y = arr4[1];
                        polygonArr.push(pt);
                        areaArr.push(arr3[key4]);
                        //获取最高维度， 获取最低维度  获取最东经度  获取最东短经度
                        if (arr4[0] > maxX) {
                            maxX = arr4[0];
                        }
                        if (arr4[0] < minX) {
                            minX = arr4[0];
                        }
                        if (arr4[1] > maxY) {
                            maxY = arr4[1];
                        }
                        if (arr4[1] < minY) {
                            minY = arr4[1];
                        }
                    }
                }
            }
            var numberObj = {}; //重新为空
            areaArr = [];//暂时清空  不取边界点
            var flagIndex = 0;
            for (; ;) {//无限循环 随机生成坐标
                var indexX = (Math.random() * (maxX - minX) + minX).toFixed(6); //四舍五入取后六位
                var indexY = (Math.random() * (maxY - minY) + minY).toFixed(6);
                var pt = new point();
                pt.x = indexX;
                pt.y = indexY;
                //判断生成的点是否在区域内
                var flag = PointInPoly(pt, polygonArr);
                if (flag) {
                    areaArr.push([indexX, indexY]);
                    flagIndex++;
                }
                if (flagIndex == depNameArr.length) { //如果等于生成的点 则结束
                    break;
                }
            }
            for (var i = 0; i < depNameArr.length; i++) {
                numberObj[areaName + depNameArr[i]] = areaArr[i]; //动态增加对象
            }
            //额外生成 中心点
            var centerX = ((maxX + minX) / 2).toFixed(6);
            var centerY = ((maxY + minY) / 2).toFixed(6);
            numberObj[areaName] = [centerX, centerY];
            returnObject = numberObj;
        }
        return returnObject;
    }
} 