<%@ page pageEncoding="UTF-8"%>
<html>
	<head>
		<title>图表示例</title>
		<link rel="stylesheet" href="${ctxPath }/charts/css/bootstrap.css" rel="stylesheet">
		<link rel="stylesheet" href="${ctxPath }/charts/css/common.css">
		<link rel="stylesheet" href="${ctxPath }/charts/css/data_text.css">
		<link rel="stylesheet" href="${ctxPath }/charts/css/foundation-datepicker.css">
	</head>
	<body>
	<div class="data_content">
		<div style="font-size: 24px; font-weight: bold; margin-bottom: 25px; color: #fff; text-align: center;">数据分析概况</div>
		<!-- 数量处理 -->
	    <div class="data_info loadItem" data-url="data/countNum.json" data-type="count" >
	    	<div class="info_1 fl" style="width: 15%">
	            <div class="text_1">
	                <div class="fl">
	                    <img src="${ctxPath }/charts/images/info_1.png" alt="" class="fl">
	                    <div class="fl">
	                        <p>数据总量</p>
	                        <p class="_count0" style="color: #ff4e4e;" >12345</p>
	                    </div>
	                </div>
	            </div>
	        </div>
	        <div class="info_1 fl" style="width: 24%">
	            <div class="text_1">
	                <div class="fl" style="width:50%;">
	                    <img src="${ctxPath }/charts/images/info_2.png" alt="" class="fl">
	                    <div class="fl" >
	                        <p>行政许可</p>
	                        <p class="_count1">11</p>
	                    </div>
	                </div>
	                <div class="fl">
	                    <img src="${ctxPath }/charts/images/info_3.png" alt="" class="fl">
	                    <div class="fl">
	                        <p>行政处罚</p>
	                        <p class="_count2">22</p>
	                    </div>
	                </div>
	            </div>
	        </div>
	        <div class="info_2 fl" style="width: 24%">
	            <div class="text_2">
	                <div class="fl">
	                    <img src="${ctxPath }/charts/images/info_4.png" alt="" class="fl">
	                    <div class="fl">
	                        <p>法人</p>
	                        <p class="_count3">33</p>
	                    </div>
	                </div>
	                <div class="fl">
	                    <img src="${ctxPath }/charts/images/info_5.png" alt="" class="fl">
	                    <div class="fl">
	                        <p>自然人</p>
	                        <p class="_count4">44</p>
	                    </div>
	                </div>
	            </div>
	        </div>
	        <div class="info_3 fl" style="width: 23%; border: none;">
	            <div class="text_3" style="width: calc(100% - 20px);border: #31708f solid 1px;">
	                <div class="fl">
	                    <img src="${ctxPath }/charts/images/info_6.png" alt="" class="fl">
	                    <div class="fl">
	                        <p>失信黑名单</p>
	                        <p class="_count5">123</p>
	                    </div>
	                </div>
	                <div class="fl">
	                    <img src="${ctxPath }/charts/images/info_7.png" alt="" class="fl">
	                    <div class="fl">
	                        <p>守信红名单</p>
	                        <p class="_count6">11</p>
	                    </div>
	                </div>
	            </div>
	        </div>
	        <div class="info_3 fl" style="width: 14%">
	            <div class="text_3">
	                <div class="fl">
	                    <img src="${ctxPath }/charts/images/info_5.png" alt="" class="fl">
	                    <div class="fl">
	                        <p>信用信息</p>
	                        <p style="color: #25f3e6" class="_count7">789</p>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
	    <!-- 图表开始 -->
	    <div class="data_main">
	        <div class="main_left fl">
	            <div class="left_2">
	                <div class="main_title"><img src="${ctxPath }/charts/images/title_1.png" alt="">法人类型分析</div>
	                <!-- 饼状图  data-rose南丁格尔玫瑰图 data-label="close"关闭标签   -->
	                <div id="creditWarning" class="loadItem" data-url="data/companyNum.json" data-type="pie" data-color='["#C1232B","#2A7D87","#FFE211","#E87C25","#C7D639"]' style="height: 100%; width: 100%;"></div>
	            </div>
	            <div class="left_2">
	                <div class="main_title"><img src="${ctxPath }/charts/images/title_2.png" alt="">法人成立日期统计</div>
	                <!-- 柱形图,data-vx表示横向显示    -->
					<div id="creditUseRanking" class="loadItem" data-url="data/chengliriqi.json" data-type="bar" data-vx="show" data-legend-style="hide" style="height: 100%"></div>
	            </div>
	        </div>
	        <div class="main_center fl">
	            <div class="center_text">
	                <div class="main_title"><img src="${ctxPath }/charts/images/title_3.png" alt="">部门接入情况</div>
	                <!-- 焦作市地图 -->
					<div id="jiaozuoMap" class="loadItem" data-url="data/mapdata_boai.json" data-type="map" style="width: 100%; height: 600px;"></div>
	            </div>
	        </div>
	        <div class="main_right fr">
	            <div class="right_1"> 
	                <div class="main_title"><img src="${ctxPath }/charts/images/title_4.png" alt="">法人数量走势</div>
	                <!--折线图,data-color覆盖颜色data-style="area " 模式设置多个模式用空格连接area面积模式 smooth平滑模式,data-legend-style=hide隐藏图例-->
					<div id="creditProgress" class="loadItem" data-url="data/mulData.json" data-type="line" data-symbol="circle,10" data-style="area " data-color='["#ffff43", "#17a390"]' 
						data-grid='{"top":"15%"}' data-legend-style="hide" style="height: 100%"></div>
				</div>
	            <div class="right_2">
	                <div class="main_title"><img src="${ctxPath }/charts/images/title_5.png" alt="">信用事件情况</div>
	                <!-- 饼状图  #61d2f7,data-percent显示百分比，data-ring内环，data-legend控制legend显示，data-label="close"关闭标签-->
					<div id="creditEvent" class="loadItem" data-url="data/creditEvent.json" data-type="pie" data-legend='{"orient" : "vertical","x":"left","top":20}' 
						data-label="center" data-percent="show" data-ring="70%" data-color='["#ff910b","#a2c32e","#fff000","#0988d7"]' style="height: 100%"></div>
	            </div>
	    	</div>
	    </div>
	    <div class="data_bottom">
	        <div class="bottom_1 fl" style="width: 74%">
	            <div class="main_title"><img src="${ctxPath }/charts/images/title_6.png" alt="">信用信息数据量</div>
	            <!-- 柱形图  data-color="#3398DB"可定义颜色，data-randomcolor="true"表示随机data-bar-width="50%"设置每个bar宽度   creditInfo.json data-tooltip-show="cross"悬浮框效果cross|shadow -->
	            <!-- data-mix="line" data-mix-axis 多图混合时用 -->
				<div id="creditInfo" class="loadItem" data-url="data/mulTypeData.json" data-type="bar" data-delay="10" data-randomcolor="true" data-tooltip-show="cross" 
					data-mix="line" data-mix-axis='{"name": "增长率","min": 0,"max": 100,"interval": 20,"axisLabel": {"formatter": "{value} %"}}' data-legend-style="hide" style="height: 100%"></div>
	        </div>
	
	        <div class="bottom_4 fr">
	            <div class="main_title"><img src="${ctxPath }/charts/images/title_5.png" alt="">部门数据总量TOP5</div>
	            <!-- 列表,data-width控制列宽度 -->
	            <div class="main_table loadItem" data-url="data/list.json" data-type="list" data-width="15%,,,15%"data-limit="5"></div>
	        </div>
	    </div>
	</div>
	<script type="text/javascript" src="${ctxPath }/charts/js/jquery-2.2.1.min.js"></script>
	<script type="text/javascript" src="${ctxPath }/charts/js/echarts.min.js"></script>
	<script type="text/javascript" src="${ctxPath }/charts/js/point_util.js"></script>
	<!-- 依赖原数据插件 -->
	<script type="text/javascript" src="${ctxPath }/webjars/shieldjs/depends/jquery.metadata.js"></script>
	<script type="text/javascript" src="${ctxPath }/webjars/shieldjs/core/shield.util.js"></script>
	<!-- 处理 -->
	<script type="text/javascript" src="${ctxPath }/charts/js/charts.deal.pie.js"></script>
	<script type="text/javascript" src="${ctxPath }/charts/js/charts.deal.bar.js"></script>
	<script type="text/javascript" src="${ctxPath }/charts/js/charts.deal.line.js"></script>
	<script type="text/javascript" src="${ctxPath }/charts/js/charts.deal.map.js"></script>
	<script type="text/javascript" src="${ctxPath }/charts/js/charts.deal.list.js"></script>
	<script type="text/javascript" src="${ctxPath }/charts/js/charts.deal.count.js"></script>
	<script type="text/javascript" src="${ctxPath }/charts/js/charts.base.js"></script>
	<%-- <script type="text/javascript" src="${ctxPath }/charts/js/charts.deal.extend.js"></script> --%>
	<script type="text/javascript" src="${ctxPath }/charts/js/charts.tool.js"></script>
	</body>
</html>
