package com.ld.echarts.model;


import java.util.LinkedList;

import com.ld.echarts.attr.Grid;
import com.ld.echarts.attr.Legend;
import com.ld.echarts.attr.Series;
import com.ld.echarts.attr.Title;
import com.ld.echarts.attr.Tooltip;
import com.ld.echarts.attr.XAxis;
import com.ld.echarts.attr.YAxis;

import lombok.Data;
/**
 * 
 * 折线图
 * @ClassName LineChartModel
 * @author <a href="mailto:903127441@qq.com" target="_blank">于鹏</a>
 * @date 2020年10月9日 下午1:50:28
 *
 */
@Data
public class LineChartModel {
	private Title title;
	private Tooltip tooltip;
	private Legend legend;
	private LinkedList<Series> series;
	private Grid grid;
	private XAxis xAxis;
	private YAxis yAxis;
	
	
}
