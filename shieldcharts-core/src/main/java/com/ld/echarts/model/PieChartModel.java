package com.ld.echarts.model;


import com.ld.echarts.attr.Legend;
import com.ld.echarts.attr.Series;
import com.ld.echarts.attr.Title;
import com.ld.echarts.attr.Tooltip;

import lombok.Data;
/**
 * 
 * 饼状图
 * @ClassName PieChartModel
 * @author <a href="mailto:903127441@qq.com" target="_blank">于鹏</a>
 * @date 2020年9月24日 上午11:46:33
 *
 */
@Data
public class PieChartModel {
	
	private Title title;
	private Tooltip tooltip;
	private Legend legend;
	private Series series;
	
}
