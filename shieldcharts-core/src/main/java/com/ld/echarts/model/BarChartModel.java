package com.ld.echarts.model;

import java.util.LinkedList;

import com.ld.echarts.attr.Legend;
import com.ld.echarts.attr.Series;
import com.ld.echarts.attr.Title;
import com.ld.echarts.attr.Tooltip;
import com.ld.echarts.attr.XAxis;
import com.ld.echarts.attr.YAxis;

import lombok.Data;
/**
 * 
 * 柱状图
 * @ClassName ColumnChartModel
 * @author <a href="mailto:903127441@qq.com" target="_blank">于鹏</a>
 * @date 2020年9月23日 上午8:30:58
 *
 */
@Data
public class BarChartModel {
	
	/**
	 * 标题对象
	 */
	private Title title;
	/**
	 * 横坐标对象
	 */
	private XAxis xAxis;
	/**
	 * 横坐标对象
	 */
	private YAxis yAxis;
	/**
	 * 图例
	 */
	private Legend legend;
	/**
	 * 
	 */
	private LinkedList<Series> series;
	
	private Tooltip tooltip;
	
	public BarChartModel() {}


	public BarChartModel(Title title, XAxis xAxis, YAxis yAxis, Legend legend, LinkedList<Series> series) {
		super();
		this.title = title;
		this.xAxis = xAxis;
		this.yAxis = yAxis;
		this.legend = legend;
		this.series = series;
	}

	
	
	
	
	
}
