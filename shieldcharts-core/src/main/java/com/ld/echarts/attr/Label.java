package com.ld.echarts.attr;

import lombok.Data;

/**
 * 
 * 图形上的文本标签，可用于说明图形的一些数据信息。
 * 比如值，名称等，label选项在 ECharts 2.x 中放置于itemStyle下，在 ECharts 3 中为了让整个配置项结构更扁平合理，
 * label 被拿出来跟 itemStyle 平级，并且跟 itemStyle 一样拥有 emphasis 状态。
 * @ClassName Label
 * @author <a href="mailto:903127441@qq.com" target="_blank">于鹏</a>
 * @date 2020年9月24日 上午9:25:36
 *
 */
@Data
public class Label {

	
	private boolean show;//是否显示
	private String position;//标签的位置。'outside'饼图扇区外侧，通过视觉引导线连到相应的扇区。'inside'饼图扇区内部。'inner' 同 'inside'。'center'在饼图中心位置。
	
	private String fontSize;//文字的字体大小。
	 /**
	 可选：
		'normal'
		'bold'
		'bolder'
		'lighter'
		100 | 200 | 300 | 400...
	  */
	private String  fontWeight;//文字字体的粗细。
	
	private String  color ;//文字的颜色。
	//标签内容格式器，支持字符串模板和回调函数两种形式，字符串模板与回调函数返回的字符串均支持用 \n 换行。
//	字符串模板 模板变量有：
//	{a}：系列名。
//	{b}：数据名。
//	{c}：数据值。
//	{d}：百分比。
//	{@xxx}：数据中名为'xxx'的维度的值，如{@product}表示名为'product'` 的维度的值。
//	{@[n]}：数据中维度n的值，如{@[3]}` 表示维度 3 的值，从 0 开始计数。
	private String  formatter;//详情参照，https://echarts.apache.org/zh/option.html#series-pie.label.formatter
	
	public Label() {}
	
	
}
