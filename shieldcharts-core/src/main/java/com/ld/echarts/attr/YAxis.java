package com.ld.echarts.attr;

import java.util.LinkedList;

import lombok.Data;

/**
 * 
 * Y坐标轴信息
 * @ClassName YAxis
 * @author <a href="mailto:903127441@qq.com" target="_blank">于鹏</a>
 * @date 2020年10月9日 下午1:52:23
 *
 */
@Data
public class YAxis {
	private LinkedList data;

	
	/**
	坐标轴类型。 可选：
	'value' 数值轴，适用于连续数据。
	'category' 类目轴，适用于离散的类目数据。为该类型时类目数据可自动从 series.data 或 dataset.source 中取，或者可通过 yAxis.data 设置类目数据。
	'time' 时间轴，适用于连续的时序数据，与数值轴相比时间轴带有时间的格式化，在刻度计算上也有所不同，例如会根据跨度的范围来决定使用月，星期，日还是小时范围的刻度。
	'log' 对数轴。适用于对数数据。
	 */
	private String type;
	private String name;//坐标轴名称。
	
	public YAxis() {
		
	}
	
	public YAxis(LinkedList data) {
		super();
		this.data = data;
	}
	
}
