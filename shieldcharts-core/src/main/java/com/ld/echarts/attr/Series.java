package com.ld.echarts.attr;

import java.util.LinkedList;
import java.util.List;

import lombok.Data;

@Data
public class Series {
	private String name;
	private String type;//统计图类型，具体见   https://echarts.apache.org/zh/option.html#series
		
	private LinkedList data;//数据

	
	/**
	 * 目前统计图较少，属性可能存在问题，后续会直接封装属性便于操作
	 */
	/**
	 * 专用于饼图的属性
	 */
	private Object radius;//饼图的半径。可以为如下类型：number：直接指定外半径值。string：例如，'20%'，表示外半径为可视区尺寸（容器高宽中较小一项）的 20% 长度。Array.<number|string>：数组的第一项是内半径，第二项是外半径。每一项遵从上述 number string 的描述。
	private boolean avoidLabelOverlap;//是否启用防止标签重叠策略，默认开启，在标签拥挤重叠的情况下会挪动各个标签的位置，防止标签间的重叠。
	private Emphasis emphasis;//高亮的扇区和标签样式。
	private LabelLine labelLine;//标签的视觉引导线样式
	private Label label;
	
	
	public Series() {
	}
	
	public Series(String name, String type, LinkedList data) {
		super();
		this.name = name;
		this.type = type;
		this.data = data;
	}

}
