package com.ld.echarts.attr;

import lombok.Data;

/**
 * 
 * 标签的视觉引导线样式
 * @ClassName LabelLine
 * @author <a href="mailto:903127441@qq.com" target="_blank">于鹏</a>
 * @date 2020年9月24日 上午9:22:02
 *
 */
@Data
public class LabelLine {
	private boolean show;//是否显示视觉引导线。
	private Integer length;//视觉引导线第一段的长度。
	private Integer length2;//视觉引导项第二段的长度。
	private Integer smooth;//是否平滑视觉引导线，默认不平滑，可以设置成 true 平滑显示，也可以设置为 0 到 1 的值，表示平滑程度。
}
