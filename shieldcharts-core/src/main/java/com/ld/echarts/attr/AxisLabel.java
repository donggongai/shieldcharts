package com.ld.echarts.attr;

import lombok.Data;

@Data
public class AxisLabel {

	private boolean show = true;
	private Integer interval;// 坐标轴刻度标签的显示间隔，在类目轴中有效。
	private Integer rotate;// 刻度标签旋转的角度，在类目轴的类目标签显示不下的时候可以通过旋转防止标签之间重叠。
	private Integer margin;// 刻度标签与轴线之间的距离。
	private String formatter;// 刻度标签的内容格式器
	private boolean showMinLabel;// 是否显示最小 tick 的 label。可取值 true, false, null。默认自动判定（即如果标签重叠，不会显示最小 tick 的label）。
	private boolean showMaxLabel;// 是否显示最大 tick 的 label。可取值 true, false, null。默认自动判定（即如果标签重叠，不会显示最大 tick 的label）。
	private String color;// 刻度标签文字的颜色
	private String fontStyle;// 文字字体的风格。 可选：'normal','italic','oblique'

	public AxisLabel(){}
	
}
