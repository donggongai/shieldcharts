package com.ld.echarts.attr;

import lombok.Data;

@Data
public class Title {
	private String text;//主标题文本，支持使用 \n 换行
	private String link;//主标题文本超链接。
	private String subtext;//副标题文本超链接。
	private String textAlign;//整体（包括 text 和 subtext）的水平对齐。  可选值：'auto'、'left'、'right'、'center'。
	private String  textVerticalAlign;//整体（包括 text 和 subtext）的垂直对齐。 可选值：'auto'、'top'、'bottom'、'middle'。
	private String  padding ;//标题内边距，单位px，默认各方向内边距为5，接受数组分别设定上右下左边距。
	
	
	public Title() {
	}
	
	public Title(String text) {
		super();
		this.text = text;
	}

	public Title(String text, String link, String subtext, String textAlign, String textVerticalAlign, String padding) {
		super();
		this.text = text;
		this.link = link;
		this.subtext = subtext;
		this.textAlign = textAlign;
		this.textVerticalAlign = textVerticalAlign;
		this.padding = padding;
	}
	
	
	
}
