package com.ld.echarts.attr;

import lombok.Data;

@Data
public class TextStyle {
	private String  color;//主标题文字的颜色。
	private String  fontStyle;//主标题文字字体的风格。可选：'normal','italic','oblique'
	private String  fontWeight;//主标题文字字体的粗细。 可选：'normal','bold','bolder','lighter',100 | 200 | 300 | 400..
	private String  fontFamily;//主标题文字的字体系列。还可以是 'serif' , 'monospace', 'Arial', 'Courier New', 'Microsoft YaHei', ...
	private String  fontSize;//主标题文字的字体大小。
	private Integer  lineHeight;//行高
	private String  width;//文字块的宽度。一般不用指定，不指定则自动是文字的宽度。在想做表格项或者使用图片（参见 backgroundColor）时，可能会使用它。
	private String  height;//文字块的高度。一般不用指定，不指定则自动是文字的高度。在使用图片（参见 backgroundColor）时，可能会使用它。
	private String  textBorderColor;//文字本身的描边颜色。 
	private Integer  textBorderWidth;//文字本身的描边宽度。
	private String  textShadowColor;//文字本身的阴影颜色。
}
