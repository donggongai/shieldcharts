package com.ld.echarts.attr;

import java.util.LinkedList;

import lombok.Data;

@Data
public class Legend {
	
	
	private String orient;//图例列表的布局朝向。'horizontal','vertical'
	private Integer left;//图例组件离容器左侧的距离。left 的值可以是像 20 这样的具体像素值，可以是像 '20%' 这样相对于容器高宽的百分比，也可以是 'left', 'center', 'right'。
	private LinkedList data;

	public Legend() {
	}
	
	public Legend(LinkedList data) {
		super();
		this.data = data;
	}
	
	
}
