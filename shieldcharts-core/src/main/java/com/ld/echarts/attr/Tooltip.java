package com.ld.echarts.attr;

import lombok.Data;
/**
 * 
 * 提示框组件。
 * 提示框组件可以设置在多种地方：
		可以设置在全局，即 tooltip
		可以设置在坐标系中，即 grid.tooltip、polar.tooltip、single.tooltip
		可以设置在系列中，即 series.tooltip
		可以设置在系列的每个数据项中，即 series.data.tooltip
 * @ClassName Tooltip
 * @author <a href="mailto:903127441@qq.com" target="_blank">于鹏</a>
 * @date 2020年9月24日 上午10:45:39
 *
 */
@Data
public class Tooltip {

	private boolean show=true;//是否显示提示框组件，包括提示框浮层和 axisPointer。
	/**
	触发类型。
		'item'	数据项图形触发，主要在散点图，饼图等无类目轴的图表中使用。
		'axis'	坐标轴触发，主要在柱状图，折线图等会使用类目轴的图表中使用。	在 ECharts 2.x 中只支持类目轴上使用 axis trigger，在 ECharts 3 中支持在直角坐标系和极坐标系上的所有类型的轴。并且可以通过 axisPointer.axis 指定坐标轴。
		'none'	什么都不触发。
	 */
	private String  trigger;
		
	/**
	模板变量有 {a}, {b}，{c}，{d}，{e}，分别表示系列名，数据名，数据值等。 在 trigger 为 'axis' 的时候，会有多个系列的数据，此时可以通过 {a0}, {a1}, {a2} 这种后面加索引的方式表示系列的索引。 不同图表类型下的 {a}，{b}，{c}，{d} 含义不一样。 其中变量{a}, {b}, {c}, {d}在不同图表类型下代表数据含义为：
			折线（区域）图、柱状（条形）图、K线图 : {a}（系列名称），{b}（类目值），{c}（数值）, {d}（无）
			散点图（气泡）图 : {a}（系列名称），{b}（数据名称），{c}（数值数组）, {d}（无）
			地图 : {a}（系列名称），{b}（区域名称），{c}（合并数值）, {d}（无）
			饼图、仪表盘、漏斗图: {a}（系列名称），{b}（数据项名称），{c}（数值）, {d}（百分比）
	示例：
	formatter: '{b0}: {c0}<br />{b1}: {c1}'
	 */
	private String  formatter;		
	
	public Tooltip(){
	}
	
}
