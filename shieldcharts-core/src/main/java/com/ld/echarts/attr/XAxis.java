package com.ld.echarts.attr;

import java.util.LinkedList;
import java.util.List;

import lombok.Data;

/**
 * 
 * X坐标轴信息
 * @ClassName XAxis
 * @author <a href="mailto:903127441@qq.com" target="_blank">于鹏</a>
 * @date 2020年10月9日 下午1:52:07
 *
 */
@Data
public class XAxis {
	private boolean show=true;//是否显示 x 轴。
	private LinkedList data;//横坐标数据
	
	/**
	坐标轴类型。 可选：
	'value' 数值轴，适用于连续数据。
	'category' 类目轴，适用于离散的类目数据。为该类型时类目数据可自动从 series.data 或 dataset.source 中取，或者可通过 yAxis.data 设置类目数据。
	'time' 时间轴，适用于连续的时序数据，与数值轴相比时间轴带有时间的格式化，在刻度计算上也有所不同，例如会根据跨度的范围来决定使用月，星期，日还是小时范围的刻度。
	'log' 对数轴。适用于对数数据。
	 */
	private String type;
	private boolean boundaryGap=true;//坐标轴两边留白策略，类目轴和非类目轴的设置和表现不一样。

	
	
	public XAxis() {
	}
	
	public XAxis(LinkedList data) {
		super();
		this.data = data;
	}
	
}
