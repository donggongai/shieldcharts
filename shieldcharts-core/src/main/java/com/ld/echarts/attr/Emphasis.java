package com.ld.echarts.attr;

import lombok.Data;

/**
 * 
 * 高亮的扇区和标签样式。
 * @ClassName Emphasis
 * @author <a href="mailto:903127441@qq.com" target="_blank">于鹏</a>
 * @date 2020年9月24日 上午9:21:49
 *
 */
@Data
public class Emphasis {
	private Label label;
	
}
