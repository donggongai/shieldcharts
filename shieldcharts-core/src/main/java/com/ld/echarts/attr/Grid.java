package com.ld.echarts.attr;

import lombok.Data;

/**
 * grid 直角坐标系内绘图网格，单个 grid 内最多可以放置上下两个 X 轴，左右两个 Y 轴。可以在网格上绘制折线图，柱状图，散点图（气泡图）。 在
 * ECharts 2.x 里单个 echarts 实例中最多只能存在一个 grid 组件，在 ECharts 3 中可以存在任意个 grid 组件。
 * 
 * @ClassName Grid
 * @author <a href="mailto:903127441@qq.com" target="_blank">于鹏</a>
 * @date 2020年10月9日 上午8:59:51
 *
 */
@Data
public class Grid {

	private boolean show = true;
	private String left;// grid 组件离容器左侧的距离。
	private String right;// grid 组件离容器右侧的距离。
	private String top;// grid 组件离容器上侧的距离。
	private String bottom;// grid 组件离容器下侧的距离。
	private String height;// grid 组件的高度。默认自适应 'auto'。
	private String width;// grid 组件的宽度。默认自适应 'auto'。

	private boolean containLabel;

	public Grid() {
	}

	public Grid(String left, String right, String bottom) {
		super();
		this.left = left;
		this.right = right;
		this.bottom = bottom;
	}

}
