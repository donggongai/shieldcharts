package com.ld.echarts.init;

import java.util.LinkedHashMap;
import java.util.LinkedList;

import com.ld.echarts.attr.Emphasis;
import com.ld.echarts.attr.Grid;
import com.ld.echarts.attr.Label;
import com.ld.echarts.attr.LabelLine;
import com.ld.echarts.attr.Legend;
import com.ld.echarts.attr.Series;
import com.ld.echarts.attr.Title;
import com.ld.echarts.attr.Tooltip;
import com.ld.echarts.attr.XAxis;
import com.ld.echarts.attr.YAxis;
import com.ld.echarts.data.PieDataModel;
import com.ld.echarts.model.LineChartModel;
import com.ld.echarts.model.PieChartModel;

/**
 * 
 * 折线图初始化类
 * 
 * @ClassName InitLine
 * @author <a href="mailto:903127441@qq.com" target="_blank">于鹏</a>
 * @date 2020年10月9日 上午9:21:30
 *
 */
public class InitLine {

	/**
	 * 
	 * 简单折线图生成
	 * @Title getSimple
	 * @author 于鹏
	 * @date 2020年10月12日 上午8:37:38
	 * @param nameList
	 * @param dataList
	 * @return LineChartModel
	 */
	public static LineChartModel getSimple(LinkedList<String> nameList, LinkedList<String> dataList) {
		LineChartModel chartModel = new LineChartModel();
		// 创建tooltip
		Tooltip tooltip = new Tooltip();
		tooltip.setTrigger("axis");
		Grid grid = new Grid("3%", "4%", "3%");
		grid.setContainLabel(true);
		XAxis xAxis = new XAxis();
		xAxis.setType("category");
		xAxis.setBoundaryGap(false);
		xAxis.setData(nameList);
		YAxis yAxis = new YAxis();
		yAxis.setType("value");
		Series series = new Series();
		series.setData(dataList);
		series.setType("line");
		chartModel.setGrid(grid);
		chartModel.setTooltip(tooltip);
		chartModel.setXAxis(xAxis);
		chartModel.setYAxis(yAxis);
		chartModel.setSeries(new LinkedList<Series>() {
			{
				add(series);
			}
		});
		return chartModel;
	}

	/**
	 * 多条折线图初始化 https://echarts.apache.org/examples/zh/editor.html?c=line-stack
	 */
	public static LineChartModel getManyLine(LinkedList<String> XNameList,LinkedHashMap<String,LinkedList> dataList) {
return null;
	}

}
