package com.ld.echarts.init;

import java.util.LinkedList;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.ld.echarts.attr.Series;
import com.ld.echarts.attr.Title;
import com.ld.echarts.attr.Tooltip;
import com.ld.echarts.attr.XAxis;
import com.ld.echarts.attr.YAxis;
import com.ld.echarts.data.PieDataModel;
import com.ld.echarts.model.BarChartModel;

/**
 * 
 * 柱状图初始化类
 * @ClassName InitBar
 * @author <a href="mailto:903127441@qq.com" target="_blank">于鹏</a>
 * @date 2020年10月9日 上午9:23:04
 *
 */
public class InitBar {
	
	/**
	 * 
	 * 初始化简单柱状图
	 * @Title initSimpleBar
	 * @author 于鹏
	 * @date 2020年9月29日 上午8:15:45
	 * @param nameList  数据标题
	 * @param dataList	具体数据
	 * @return BarChartModel
	 */
	public static BarChartModel initSimpleBar(LinkedList<String> nameList,LinkedList<String> dataList) {
		BarChartModel columnChartModel = new BarChartModel();// 柱状图对象
		columnChartModel.setTitle(new Title());// 赋值titile
		columnChartModel.setYAxis(new YAxis());
		columnChartModel.setXAxis(new XAxis(nameList));
		columnChartModel.setSeries(new LinkedList<Series>() {
			{
				add(new Series(null, "bar", dataList));
			}
		});
		Tooltip tooltip=new Tooltip();
		tooltip.setTrigger("axis");
		tooltip.setShow(true);
		columnChartModel.setTooltip(tooltip);
		
		return columnChartModel;
	}
	
}
