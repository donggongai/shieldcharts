package com.ld.echarts.init;

import java.util.LinkedList;

import com.ld.echarts.attr.Emphasis;
import com.ld.echarts.attr.Label;
import com.ld.echarts.attr.LabelLine;
import com.ld.echarts.attr.Legend;
import com.ld.echarts.attr.Series;
import com.ld.echarts.attr.Title;
import com.ld.echarts.attr.Tooltip;
import com.ld.echarts.data.PieDataModel;
import com.ld.echarts.model.PieChartModel;

/**
 * 
 * 饼状图初始化类
 * @ClassName InitPie
 * @author <a href="mailto:903127441@qq.com" target="_blank">于鹏</a>
 * @date 2020年10月9日 上午9:22:21
 *
 */
public class InitPie {
	
	
	private static  PieChartModel  initSimplePie(LinkedList<String> nameList,LinkedList<PieDataModel> dataList,String title) {
		PieChartModel initSimplePie = InitPie.initSimplePie(nameList, dataList);
		initSimplePie.setTitle(new Title(title));
		return initSimplePie;
	}
	/**
	 * 
	 * 简单饼状图初始化
	 * 具体见样式形状见：https://echarts.apache.org/examples/zh/editor.html?c=pie-doughnut
	 * @Title initSimplePie
	 * @author 于鹏
	 * @date 2020年9月28日 上午8:08:38
	 * @param nameList
	 * @param dataList
	 * @return PieChartModel
	 */
	public static  PieChartModel initSimplePie(LinkedList<String> nameList,LinkedList<PieDataModel> dataList) {
		PieChartModel pieChartModel = new PieChartModel();// 柱状图对象
		Tooltip tooltip = new Tooltip();
		tooltip.setFormatter("{a} <br/>{b}: {c} ({d}%)");
		tooltip.setTrigger("item");
		tooltip.setShow(true);
		pieChartModel.setTooltip(tooltip);
		Legend legend = new Legend();
		legend.setLeft(10);
		legend.setOrient("vertical");
		legend.setData(nameList);
		pieChartModel.setLegend(legend);
		
		Series series=new Series();
		series.setName("统计");
		series.setType("pie");
		series.setRadius(new LinkedList<String>() {{
			add("50%");
			add("70%");
		}});
		series.setAvoidLabelOverlap(false);
		Label label=new Label();
		label.setShow(false);
		label.setPosition("center");
		series.setLabel(label);
		Emphasis emphasis=new Emphasis();
		Label labelE=new Label();
		labelE.setShow(true);
		labelE.setFontSize("30");
		labelE.setFontWeight("bold");
		emphasis.setLabel(labelE);
		series.setEmphasis(emphasis);
		LabelLine labelLine =new LabelLine();
		labelLine.setShow(false);
		series.setLabelLine(labelLine);
		series.setData(dataList);
		pieChartModel.setSeries(series);
		return pieChartModel;
	}
	
	
}
