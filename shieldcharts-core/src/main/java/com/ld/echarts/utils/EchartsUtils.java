package com.ld.echarts.utils;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Random;

import com.alibaba.fastjson.JSONObject;
import com.ld.echarts.data.PieDataModel;
import com.ld.echarts.init.InitBar;
import com.ld.echarts.init.InitLine;
import com.ld.echarts.init.InitPie;
import com.ld.echarts.model.BarChartModel;
import com.ld.echarts.model.LineChartModel;
import com.ld.echarts.model.PieChartModel;

/**
 * 
 * 傻瓜式Echarts图表生成工具类
 * 不用学习Echarts，不用看文档，只要你传进数据来，在下带你一把梭哈
 * @ClassName EchartsUtils
 * @author <a href="mailto:903127441@qq.com" target="_blank">于鹏</a>
 * @date 2020年9月29日 上午8:20:53
 *
 */
public class EchartsUtils {

	public static void main(String[] args) {
		LinkedHashMap<String, String> map = new LinkedHashMap<>();
		Random r = new Random(1);
		for (int i = 0; i < 5; i++) {
			int ran1 = r.nextInt(100);
			map.put("名字" + i, ran1 + "");
		}
		Object tess = EchartsUtils.getSimleBar(map);
		Object json = EchartsUtils.toJson(tess);
		System.out.println("option="+json);

	}

	/**
	 * 
	 * 获得折线图对象
	 * 
	 * @Title getSimleLine
	 * @author 于鹏
	 * @date 2020年10月12日 上午8:21:07
	 * @param data
	 * @return Object
	 */
	public static Object getSimleLine(LinkedHashMap<String, String> data) {
		LinkedList<String> nameList = new LinkedList<>();// 数据标题
		LinkedList<String> dataList = new LinkedList<>();
		for (String key : data.keySet()) {
			String value = data.get(key);
			dataList.add(value);
			nameList.add(key);
		}
		LineChartModel simple = InitLine.getSimple(nameList, dataList);
		return simple;

	}


	/**
	 * 
	 * 此方法为饼状图对象，支持二次扩展哟
	 * 
	 * @Title getSimlePie
	 * @author 于鹏
	 * @date 2020年10月9日 上午9:12:15
	 * @param data
	 * @return Object
	 */
	public static Object getSimlePie(LinkedHashMap<String, String> data) {
		LinkedList<String> nameList = new LinkedList<>();// 数据标题
		LinkedList<PieDataModel> dataList = new LinkedList<>();
		for (String key : data.keySet()) {
			String value = data.get(key);
			dataList.add(new PieDataModel(key, value));
			nameList.add(key);
		}
		PieChartModel initSimplePie = InitPie.initSimplePie(nameList, dataList);
		return initSimplePie;
	}


	/**
	 * 
	 * 此方法为柱状图对象，支持二次拓展呦
	 * 
	 * @Title getSimleBar
	 * @author 于鹏
	 * @date 2020年10月9日 上午9:13:27
	 * @param data
	 * @return Object
	 */
	public static Object getSimleBar(LinkedHashMap<String, String> data) {
		LinkedList<String> nameList = new LinkedList<>();// 数据标题
		LinkedList<String> dataList = new LinkedList<>();
		for (String key : data.keySet()) {
			String value = data.get(key);
			dataList.add(value);
			nameList.add(key);
		}
		BarChartModel initSimpleBar = InitBar.initSimpleBar(nameList, dataList);
		return initSimpleBar;
	}

	public static Object toJson(Object obj) {
		Object json = JSONObject.toJSON(obj);
		return json;
	}

}
