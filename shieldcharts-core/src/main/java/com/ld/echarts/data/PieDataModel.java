package com.ld.echarts.data;

import lombok.Data;

@Data
public class PieDataModel implements DataInterface {
	private String name;
	private String value;
	
	public PieDataModel() {
	}
	
	public PieDataModel(String name, String value) {
		super();
		this.name = name;
		this.value = value;
	}
	
}
